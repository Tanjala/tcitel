<?php

function getIPs($withV6 = false)
{
    preg_match_all('/inet'.($withV6 ? '6?' : '').' addr: ?([^ ]+)/', `ifconfig`, $ips);
    return $ips[1];
}

//$PGSQL_HOST = "vsim.czapwbxncvgj.eu-central-1.rds.amazonaws.com";
$PGSQL_USER = "vsim_user";
$PGSQL_PASS = "vsim_pass";
$PGSQL_DB	= "vsim_db";

$ips = getIPs();

$BILLING_IP = !$ips || in_array('172.31.19.10', $ips) ? '172.31.22.127' : '127.0.0.1';
// ako postoji tunnel preko procescom PBX-a za rad van office-a
$PGSQL_HOST = !$ips || in_array('172.31.19.10', $ips) ? '172.31.28.21' : preg_grep('/192.168.88./', $ips) ? "vsim.czapwbxncvgj.eu-central-1.rds.amazonaws.com" : '127.0.0.1';

$session_name = "Tcitel";
