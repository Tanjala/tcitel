Ext.define
(
	'Tcitel.controller.common.Common',
    {
		extend: 'Ext.app.Controller',
        statics:
        {
            almostMidnight: function()
            {
                var d = new Date();
                d.setHours(23);
                d.setMinutes(59);
                d.setSeconds(59);
                return Ext.Date.format(d, 'Y-m-d H:i:s');
            },
            midnight: function()
            {
                var d = new Date();
                d.setHours(0);
                d.setMinutes(0);
                d.setSeconds(0);
                return Ext.Date.format(d, 'Y-m-d H:i:s');
            },
            monthStart: function()
            {
                var d = new Date();
                d.setDate(1);
                d.setHours(0);
                d.setMinutes(0);
                d.setSeconds(0);
                return Ext.Date.format(d, 'Y-m-d H:i:s');
            },
             monthBefore: function()
            {
                var d = new Date();
                d.setMonth(-1);
                d.setHours(0);
                d.setMinutes(0);
                d.setSeconds(0);
                return Ext.Date.format(d, 'Y-m-d H:i:s');
            },
            monthBeforeDate: function()
            {
                var d = new Date();
                d.setMonth(d.getMonth() - 1);
                return Ext.Date.format(d, 'Y-m-d');
            },
             monthStartDate: function()
            {
                var d = new Date();
                d.setDate(1);
                return Ext.Date.format(d, 'Y-m-d');
            }
        }
	}
);
