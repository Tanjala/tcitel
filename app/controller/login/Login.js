Ext.define
(
	'Tcitel.controller.login.Login',
	{
		extend: 'Ext.app.Controller',
        requires:
        [
            'Tcitel.view.login.Login',
            'Tcitel.model.login.User',
            'Tcitel.view.main.Main'
        ],
    	init: function()
	    {
	         this.listen
	         (
	            {
	                global:
	                {
	                    beforeviewportrender: this.checkLogIn,
                        aftervalidateloggedin: this.setupApplication
	                }
	            }
	        );
	    },
	    checkLogIn: function()
	    {
            Ext.Ajax.request
            (
                {
                    url: 'php/login/checklogin.php',
                    success: function( response, options )
                    {
                        var result = Ext.decode( response.responseText );
                        if(result.success)
                        {
                            Tcitel.LoggedInUser = Ext.create( 'Tcitel.model.login.User', result.data );
                            Ext.globalEvents.fireEvent('aftervalidateloggedin');
                        }
                        else
                        {
                            Ext.widget('login').show();
                        }
                    },
                    failure: function( response, options )
                    {
                        Ext.widget('login').show();
                    }
                }
            )
	    },
        setupApplication: function()
        {
            Ext.widget('main').show();
            Ext.ComponentQuery.query('[brand_access]').forEach
            (
                function(element, index, array)
                {
                    //element.setVisible(Tasks.LoggedInUser.inRole(element.privilege));
                    //if (!Tasks.LoggedInUser.inRole(element.privilege)) element.hide();
                    if (Tcitel.LoggedInUser.data.brand != element.brand_access) element.destroy();
                }
            );
        }
 	}
 );
