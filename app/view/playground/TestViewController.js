Ext.define('Tcitel.view.playground.TestViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.playground',
    routes : {
    	'playground' : 'onPlayground'
	},

	onPlayground: function()
	{
		this.resume();
		//this.redirectTo('home');
	}

});
