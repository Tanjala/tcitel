Ext.define
(
    'Tcitel.view.rates.RatesTabs',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'rates-tabs',
        //controller: 'routes',
        plain: true,
        items:
        [
            {
                title: 'Rates Search',
                tabConfig:
                {
                    tooltip: 'Search'
                },
                xtype: 'ratessearch'
            },
            {
                title: 'Add Rate',
                tabConfig:
                {
                    tooltip: 'Create'
                },
                xtype: 'ratesadd'
            }
        ]
    }
);