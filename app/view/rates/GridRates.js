
Ext.define
(
    'Tcitel.view.rates.GridRates',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'gridrates',
        bind: {store:'{rate}'},
        header:{
                items:
                [
                  {
                    xtype:'button',
                    text: 'Reset',
                    handler: 'reset'
                  }
                ] 
            },   
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{rate}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        columns:
        {
            defaults:

            {
                flex: 1
            },
            items:
            [
                 {
                    dataIndex: 'iso2',
                    xtype:'templatecolumn',
                    tpl:'<img src="resources/images/flags4030/{iso2}.png" width="30px">',
                    width: 70,
                    flex: false
                   
                },
                 {
                    dataIndex :  'destination',
                    id: 'hiddenheader1',
                    width: 280,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    items:
                    [
                        {
                            text      :  'Destination',
                            dataIndex :  'destination',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'combo',
                            reference: 'destination_country',
                            flex : 1,
                            padding: 3,
                            minChars: 0,
                            name: 'destination',
                            bind: {store:'{country}'},
                            displayField: 'display',
                            valueField: 'value',
                            allowBlank: true,
                            value: '',
                            listeners:
                            {
                                select: 'groupSelect'
                            }
                        }
                    ]
                },
           
                 {
                    dataIndex :  'prefix',
                    id: 'hiddenheader2',
                    readOnly: false,
                    width: 220,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    items:
                    [
                        {
                            text      :  'Prefix',
                            dataIndex :  'prefix',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'textfield',
                            name: 'prefix',
                          //fieldLabel: 'Number',
                            bind: '{search.prefix}',
                            reference: 'prefix1',
                            readOnly: false,
                            minChars: 0,
                            margin:'3 2 0 2',
                            listeners:
                            {
                                specialkey: 'prefixSelect'
                            }
                        }
                      
                    ]
                },
            
                 {
                    text: 'MIN',
                    dataIndex: 'perminutecost'
                   
                },
                 {
                    text: 'SMS',
                    dataIndex: 'persmscost'
                   
                },
                {
                    dataIndex :  'brand',
                    brand_access: 'Virtual SIM',
                    id: 'hiddenheader3',
                    width: 250,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    items:
                    [
                        {
                            text      :  'Brand',
                            dataIndex :  'brand',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'combo',
                            reference: 'brandastic',
                            flex : 1,
                            padding: 3,
                            minChars: 0,
                            name: 'brand',
                            bind: {store:'{brand}'},
                            displayField: 'display',
                            valueField: 'value',
                            allowBlank: true,
                            value: '',
                            listeners:
                            {
                                select: 'gridcomboSelect'
                            }
                        }
                    ]
                },
             
                  {
                    text: 'Route',
                    width: 90,
                    flex: false,
                    dataIndex: 'route'
                   
                }
           
            ]
        }
    }
);
