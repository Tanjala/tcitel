

Ext.define
(
    'Tcitel.view.rates.Rates',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'rates',
        title:'Rates',
        itemId: 'rates',
        controller: 'rates',
        viewModel:
        {
            type: 'rates'
        },
        requires:
        [
            //'Tcitel.view.rates.RatesSearch',
            'Tcitel.view.rates.GridRates',
            //'Tcitel.view.rates.AddRates',
            //'Tcitel.view.rates.RatesTabs',
            'Tcitel.view.rates.RatesController',
            'Tcitel.view.rates.RatesViewModel'//,
            //'Tcitel.view.routes.EditRoute'
        ],
        layout:
        {
            type: 'hbox',
            align: 'stretch'
        },
        defaults:
        {
            xtype: 'panel',
            defaults:
            {
                border: true,
                margin: 3
            }
        },
        items:
        [
           /* {
                width: 300,
                items:
                [
                    {
                        xtype: 'rates-tabs',
                        title:'Rates'
                    }
                ]
            },*/
            {
                flex: 1,
                layout:
                {
                    type: 'fit'
                },
                items:
                [
                    {
                        //title : 'Data',
                        xtype: 'gridrates'
                    }
                ]
            }
        ]
    }
);



