Ext.define
(
    'Tcitel.view.rates.AddRates',
    {
        extend: 'Ext.form.Panel',
        xtype: 'ratesadd',
        monitorValid: true,
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 90,
                    width:240,
                    allowBlank: false
                },
                items:
                [
                    {
                        name: 'name',
                        fieldLabel: 'Route Name'
                    },
                     {
                        name: 'host',
                        fieldLabel: 'Host'
                    },
                    {
                        xtype:'checkboxfield',
                        name: 'active',
                        fieldLabel: 'Activation',
                        labelWidth: 95,
                        inputValue: 't',
                        uncheckedValue:'f',
                        boxLabel  : 'Active'
                    },
                    {             
                        xtype: 'checkboxgroup',
                        fieldLabel: 'Call Direction',
                        //allowBlank: false,
                        items: 
                        [
                           {
                                xtype: 'checkboxfield',
                                boxLabel  : 'Inbound',
                                name: 'inbound',
                                inputValue: 't',
                                uncheckedValue:'f',
                                margin:'0 6 0 0'

                            },
                            {
                                xtype: 'checkboxfield',
                                name:'outbound',
                                inputValue: 't',
                                uncheckedValue:'f',
                                boxLabel  : 'Outbound'

                            }
                        ]
                        
                    
                    },
                    {
                        name: 'ip',
                        xtype:'textarea',
                        emptyText: 'line break separated',
                        fieldLabel: 'IP',
                        //maskRe: /[.0-9\r//]/,
                        allowBlank: true
                    },
                   
                    {
                        xtype: 'button',
                        text: 'Clear',
                        margin: '10 5 0 95',
                        width: 70,
                        handler: 'clear'
                    },
                    {
                        xtype: 'button',
                        text: 'Add',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'add',
                        formBind: true
                    }
                ]
            }
        ]
    }
);
