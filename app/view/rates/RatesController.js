Ext.define('Tcitel.view.rates.RatesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.rates',

     comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

         search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('rate'), filter);
        },

        clear: function(button, e)
        {
            button.up('form').getForm().reset();
        },

         reset: function()
        {
            this.getViewModel().set('search', {});
           
            var combo=this.lookupReference('destination_country'),
                combobrand=this.lookupReference('brandastic');
            
            combo.reset();
            combobrand.reset();
            this.groupSelect(combo);
            this.gridcomboSelect(combobrand);

        },
          edit: function(button, e)
        {
            var record = button.up('container').getWidgetRecord();
            var routeedit = Ext.widget('routeedit');
            routeedit.loadRecord(record);
            this.getView().add(routeedit);
            routeedit.show();
        }
        ,
        add: function(button, e)
        {
            var form = button.up('form'),
            values = form.getValues(),
            store = this.getStore('route'),
            route = Ext.create
            (
                'Tcitel.model.routes.Routes',
                values
            );
            //delete route.id;
           // delete route.data.id;
            route.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        var response = Ext.decode(operation._response.responseText);
                        if (success)
                        {
                           if (response.message)
                           {
                                Ext.Msg.alert('Success with error!', response.message);
                           }
                           else
                           {
                                Ext.Msg.alert('Success!', 'Route is added.');
                           }
                           form.reset();
                           store.reload();
                        }
                        else
                        {
                            //var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );

        },
         groupSelect: function(combo, record, eOpts)
        {
            var filter = 
            [
              {property: 'destination', value: combo.value},
              {property: 'brand', value: ''}

            ];
            this.lookupReference('brandastic').reset();
            this.lookupReference('prefix1').reset();
            this.filterStore(this.getStore('rate'), filter);


        },

        gridcomboSelect: function(combo, record, eOpts)
        {
            var filter = 
            [
                {property: 'brand', value: combo.value},
                {property: 'destination', value: this.lookupReference('destination_country').getValue()},
                {property: 'prefix', value: this.lookupReference('prefix1').getValue()}
            ];
            
           this.filterStore(this.getStore('rate'), filter);
        },

         prefixSelect: function(field, e)
        {
             if (e.getKey() == e.ENTER)
            {
                this.search();
                this.lookupReference('brandastic').reset();
                this.lookupReference('destination_country').reset();
            };
           /* var filter = [{property: 'prefix', value: search.prefix}];
            this.filterStore(this.getStore('rate'), filter);*/
        },
        

        save: function(button, e, eOpts)
        {
            var form = button.up('form'),
            store = this.getStore('route'),
            route = form.getRecord();
            form.updateRecord();
            form.setLoading(true);
            route.save
            (
                {
                    params :
                    {
                        route: route.data.route
                    },
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                            form.setLoading(false);
                            form.close();
                            Ext.Msg.alert('Success!', 'Route data is saved.');
                            store.reload();
                        }
                        else
                        {
                            form.setLoading(false);
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );
        },
        
        modify: function(button, e, eOpts)
        {
            var form = button.up('form'),
            route = form.getRecord(),
            store = this.getStore('route')
            route.data.ip = ' ' + route.data.ip + ' ';
            form.updateRecord(route);
            route.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        var response = Ext.decode(operation._response.responseText);

                         if (success)
                        {
                           if (response.message)
                           {
                                Ext.Msg.alert('Success with error!', response.message);
                           }
                           else
                           {
                                Ext.Msg.alert('Success!', 'Route is changed.');
                           }
                           form.close();
                           store.reload();
                        }
                        else
                        {
                            Ext.Msg.alert('Failure!', response.message);
                            store.reload();
                        }
                    }
                }
            );
        }


});