Ext.define
(
    'Tcitel.view.rates.RatesViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.rates',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.rates.Rates'
        ],
        data:
        {
             coyntrycode:
            {
                
                destination: ''

            },
            
            search:
            {
                
                destination: '',
                prefix: '',
                brand:''   
                
            }
        },
       stores:
        {
            rate:
            {
                model: 'Tcitel.model.rates.Rates',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/rates/readrates.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
               
                remoteFilter: true,
                //remoteSort: true,
                autoLoad: true
            },
             brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_rates',
                        valueField: 'brand',
                        displayField: 'brand',
                        queryField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },


           /*  prefix:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_rates',
                        valueField: 'prefix',
                        displayField: 'prefix',
                        queryField: 'prefix'//,
                        //all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },*/

            country:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/rates/combocountry.php',
                    extraParams:
                    {
                        table: 'vs_rates',
                        valueField: 'destination',
                        displayField: 'destination',
                        queryField: 'destination'//,
                        //all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            }

            
          
        }
    }
);
