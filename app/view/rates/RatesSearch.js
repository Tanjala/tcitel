
Ext.define
(
    'Tcitel.view.rates.RatesSearch',
    {
        extend: 'Ext.form.Panel',
        xtype: 'ratessearch',
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 90,
                    width:240,
                    listeners:
                    {
                        specialkey: 'specialKey'
                    }
                },
                items:
                [
                    {
                        name: 'destination',
                        fieldLabel: 'Country',
                        bind: '{search.destination}'
                    },
                     {
                        name: 'prefix',
                        fieldLabel: 'Prefix',
                        bind: '{search.prefix}'
                    },
                   /* {
                        xtype: 'combo',
                        name: 'direction',
                        hiddenName: 'direction',
                        fieldLabel: 'Call Direction',
                        bind: {store:'{direction}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'combo',
                        name: 'active',
                        hiddenName: 'active',
                        fieldLabel: 'Active',
                        bind: {store:'{active}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },*/
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 5 0 95',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'search'
                    }
                ]
            }
        ]
    }
);







