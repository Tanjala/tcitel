Ext.define
(
    'Tcitel.view.pages.Pages',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'pages',
        requires:
        [
            'Tcitel.view.pages.PagesController',
            //'Tcitel.view.spofnumbers.Numbers',
            'Tcitel.view.menucardtab.MenuCardTab',
            'Tcitel.view.billing.Billing',/*,
            'Tcitel.view.playground.TestView'*/
            'Tcitel.view.cdrlog.CdrLog',
            'Tcitel.view.messagestat.hourly.Hourly',
            'Tcitel.view.messagestat.daily.Daily',
            
            //'Tcitel.view.dailystat.DailyStat',
            //'Tcitel.view.orders.Orders',
            //'Tcitel.view.autorenew.Autorenew',
            'Tcitel.view.stat.Stat',
            'Tcitel.view.operator.Operator',
            //'Tcitel.view.messagelog.MessageLog',
            'Tcitel.view.rates.Rates',
            'Tcitel.view.routes.Routes',
            //'Tcitel.view.gt_routes.GT_routes',
            'Tcitel.view.spofnumbers.MainNumbers'

        ],

        controller: 'pages',

        initComponent: function()
        {
            me =  this;
            this.callParent();
        },
        items:
        [
            {
                xtype: 'billing'
            },
            {
                xtype: 'paymentmain'
            },
            {
                xtype: 'cdrlog'
            },
            {
                xtype: 'stat'
                //title: 'Stat'
            },
            
             {
                border: false,
                title: 'Messaging <i class="fa fa-bar-chart"></i>',
                xtype: 'menucardtab',
                items:
                [
                    {
                        border: false,
                        xtype: 'messagehourlystat',
                        title: 'Hourly <i class="fa fa-bar-chart"></i>',
                        itemId:'messagehourly',
                        header:false
                    },
                    {

                        xtype: 'messagedailystat',
                        border: false,
                        itemId:'messagedaily',
                        title: 'Daily <i class="fa fa-bar-chart"></i>',
                        header: false
                    }
                ]
            },
           /* {
                xtype: 'messagelog',
                brand_access: 'Virtual SIM'
            },*/
            {
                xtype: 'spofnumbersmain'
            },
            {
                xtype: 'routes'//,
                 //brand_access: 'Virtual SIM'
                //title:'Routes'
            },
            {
                 xtype: 'rates'
                
                //title:'Rates'
            },
            /* {
                 xtype: 'gt_routes',
                 brand_access: 'Virtual SIM'
                
                //title:'Rates'
            },*/
            {   // Fill all available space
                tabConfig :
                {
                    xtype : 'tbfill'
                }
            },
            {
                xtype: 'operator',
                tabConfig:
                {
                    tooltip: 'Operators'
                }
            },
            {
                title: 'Logout',
                listeners:
                {
                    beforeactivate: 'logOut',
                    added: 'setTitle'
                }
            }
        ]
    }
);