Ext.define
(
    'Tcitel.view.pages.PagesController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.pages',

        logOut: function( button, e, eOpts )
        {
            var me = this;
            Ext.Msg.confirm
            (
                'Confirm', 'Logout?',
                function( button )
                {
                    if( button=='yes' )
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: 'php/login/logout.php',
                                success: function(response, options)
                                {
                                    window.location.reload(true);
                                },
                                failure: function( response, options )
                                {
                                    Ext.Msg.alert( 'Attention', 'Sorry, an error occurred during your request. Please try again.');
                                }
                            }
                        );
                    }
                }
            );
            return false;
        },

        setTitle: function( tabPanel, eOpts )
        {
            tabPanel.setTitle('Logout <i>' + Tcitel.LoggedInUser.data.firstname + ' ' + Tcitel.LoggedInUser.data.lastname + '</i>');
        }
    }
);
