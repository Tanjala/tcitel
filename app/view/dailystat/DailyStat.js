Ext.define
(
    'Tcitel.view.dailystat.DailyStat',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'dailystat',
        //title: 'Hourly Stat',
        border: false,
        itemId: 'DailyStat',
        controller: 'dailystat',
       
        viewModel:
        {
            type: 'dailystat'
        },
        requires:
        [

            'Tcitel.view.dailystat.West',
            'Tcitel.view.dailystat.Center',
            'Tcitel.view.dailystat.DailyStatController',
            'Tcitel.view.dailystat.DailyStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'dailystat'
        },
        items:
        [
            {
               // border: false,
                xtype: 'dailystatwest',
                region: 'west'
            },
            {
               // border: false,
                xtype: 'dailystatcenter',
                region: 'center'
            }
        ]
    }
);
