Ext.define
(
    'Tcitel.view.dailystat.DailyStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.dailystat',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.dailystat.DailyStat'
        ],
        data:
        {
            search:
            {
                startday: Ext.Date.format(Ext.Date.getFirstDateOfMonth(new Date()), 'Y-m-d'),
                endday: Ext.Date.format(new Date(), 'Y-m-d')
            }
        },
        stores:
        {
            dailystat:
            {
                storeId: 'dailystat',
                model: 'Tcitel.model.dailystat.DailyStat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/dailystat/stat.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            }
        }
    }
);
