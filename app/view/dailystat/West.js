Ext.define
(
    'Tcitel.view.dailystat.West',
    {
        extend: 'Ext.form.Panel',
        xtype: 'dailystatwest',
        layout: 'hbox',
        border: false,
        items:
        [
            {
                xtype: 'fieldset',
                margin: 20,
                padding: 20,
                title: 'Filter',
                defaults:
                {
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    width: 180,
                    editable: false,
                    labelWidth: 50,
                    listeners:
                    {
                        change: 'change'
                    }
                },
                items:
                [
                    {
                        value: Ext.Date.format(Ext.Date.getFirstDateOfMonth(new Date()), 'Y-m-d'),
                        name: 'startday',
                        fieldLabel: 'Start'
                    },
                    {
                        value: Ext.Date.format(new Date(), 'Y-m-d'),
                        name: 'endday',
                        fieldLabel: 'End'
                    },
                    {
                        xtype: 'button',
                        text: 'Apply',
                        handler: 'search',
                        margin: '10 0 0 110',
                        width: 70
                    }
                ]
            }
        ]
    }
);
