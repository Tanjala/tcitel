Ext.define
(
	'Tcitel.view.billing.Devices',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'devices',
		title: 'Devices',
		bind: {store:'{devices}'},
		plugins:
		[{
            ptype: 'cellediting',
            clicksToEdit: 1
        }],
		columns:
		{
            defaults:
            {
            	editor:
            	{
            		readOnly:true
            	},
                flex: 1
            },
			items:
			[
			    {
			    	xtype: 'rownumberer',
			    	width: 40,
			    	flex: false
			    },
			    {
			        text: 'User ID',
			        dataIndex: 'user_id'
			    },
			    {
			        text: 'Platform',
			        dataIndex: 'platform'
			    },
			    {
			        text: 'Platform Data',
			        dataIndex: 'platform_data',
			        editor: 
			        {
			        	xtype:'textarea'
			        }
			    },
			    {
			        text: 'Brand',
			        dataIndex: 'app_name',
			        brand_access: 'Virtual SIM'
			    },
			     {
			        text: 'App Version',
			        dataIndex: 'app_version'
			    },
			     {
			        text: 'Device ID',
			        dataIndex: 'duid'
			    },
			     {
                    text: 'Trial Log',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',

                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 60
                        },
                        items:
                        [

                            {

                                html:'details',
                                margin: '0 5 5 5',
                                handler: 'triallog'
                            }
                        ]
                    }
                },
			    {
			        text: 'Caller ID',
			        dataIndex: 'caller_id'
			    },
			    {
			        text: 'HC Prefix',
			        dataIndex: 'nc_prefix'
			    },
			    {
			        text: 'Created',
			        dataIndex: 'created'
			    },
			    {
			        text: 'Registered',
			        dataIndex: 'registered'
			    }
			]
		}
	}
);