Ext.define
(
	'Tcitel.view.billing.ChargeLog',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'billinglog',
		title: 'Charge Log',
		bind: {store:'{chargelog}'},
		//plugins: 'gridfilters',
		/*selModel:
        {
            mode: 'MULTI'
        },
        allowDeselect: true,*/
		plugins:
		[{
            ptype: 'cellediting',
            clicksToEdit: 2
        }/*,
        {
          ptype:'gridfilters'
        }*/],
		columns:
		{
            defaults:
            {
            	editor:
            	{
            		xtype:'textfield',
            		readOnly:true
            	},
                flex: 1
            },
			items:
			[
			    {
			        text: 'Created',
			        dataIndex: 'created_at'
		
			    },
			    {
			        text: 'Expires',
			        dataIndex: 'expires_at'
			    },
			    {
			        text: 'ID',
			        dataIndex: 'transaction_id'
			    },
			   /* {
			        text: 'Auth ID',
			        dataIndex: 'auth_id'
			    },*/
			    /*{
			        text: 'Profile ID',
			        dataIndex: 'billing_profile_id'
			    },*/
			    {
                    dataIndex :  'name',
                    id: 'hiddenheader',
                    width: 200,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    items:
                    [
                        {
                            text      :  'Billing Type',
                            dataIndex :  'name',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'combo',
                            reference: 'billing_type',
                            flex : 1,
                            padding: 3,
                            name: 'chargelog',
                            bind: {store:'{billing_profile}'},
                            displayField: 'display',
                            valueField: 'value',
                            forceSelection: true,
                            editable: false,
                            allowBlank: true,
                            value: '',
                            listeners:
                            {
                                select: 'groupSelect'
                            }
                        }
                    ]
                },
			    {
			        text: 'Committed',
			        dataIndex: 'committed'
			    },
			    {
			        text: 'Rate',
			        dataIndex: 'rate'
			    },
			    {
			        text: 'Multiply',
			        dataIndex: 'multiply'
			    },
			    {
			        text: 'Amount',
			        dataIndex: 'amount'
			    },
			    {
			        text: 'Result',
			        dataIndex: 'result_name',
			          editor:
	            	{
	            		xtype:'textarea',
	            		readOnly:true
	            	}
			    },
			     {
			        text: 'Brand',
			        dataIndex: 'brand',
			        brand_access: 'Virtual SIM'
			    },
			    {
			        text: 'Meta Data',
			        dataIndex: 'meta_data',
			        editor:
	            	{
	            		xtype:'textarea',
	            		readOnly:true
	            	}
			    }
			]
		}
	}
);