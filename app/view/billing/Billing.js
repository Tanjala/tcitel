

Ext.define
(
    'Tcitel.view.billing.Billing',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'billing',
        title: 'Billing',
        itemId: 'billing',
        controller: 'billing',
        
        viewModel:
        {
            type: 'billing'
        },
        requires:
        [
            'Tcitel.view.billing.CustomerBilling',
            'Tcitel.view.billing.BillingController',
            'Tcitel.view.billing.BillingViewModel',
             'Tcitel.view.billing.HiddenChart'
        ],

        items:
        [
            {
                xtype: 'billingcustomer'
            },
            {   // Fill all available space
                tabConfig :
                {
                    xtype : 'tbfill'
                }
            },
            {
                xtype: 'hiddenChart',
                brand_access: '100'
            }
        ]
    }
);