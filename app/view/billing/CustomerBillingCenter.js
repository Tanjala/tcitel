Ext.define
(
    'Tcitel.view.billing.CustomerBillingCenter',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'billingcenter',
        requires:
        [
            'Tcitel.view.billing.PhoneNumbers',
            'Tcitel.view.billing.ChargeLog',
            'Tcitel.view.billing.OrderLog',
            'Tcitel.view.billing.Devices',
            //'Tcitel.view.billing.CdrLog',
            'Tcitel.view.billing.TrialLogDetail'
        ],

    	items:
        [
            {
                xtype: 'billinglog'
            },
            {
                xtype: 'phonenumbers'
            },
            {
                xtype: 'devices'
            }
            /*,
            {
                xtype: 'billing_cdrlog'
            }*/
            /*,
            {
                xtype: 'orderlog'
            }*/
        ]
    }
);