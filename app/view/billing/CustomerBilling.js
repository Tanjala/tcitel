
Ext.define
(
    'Tcitel.view.billing.CustomerBilling',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'billingcustomer',
        title: 'Customer Billing',
        requires:
        [
            'Tcitel.view.billing.CustomerBillingNorth',
            'Tcitel.view.billing.CustomerBillingCenter'//,

            /*'Tcitel.view.billing.BillingController',
            'Tcitel.view.billing.BillingViewModel'*/
        ],
        layout: 'border',
        items:
        [
            {
                xtype: 'billingnorth',
                region: 'north'
            },
            {
                xtype: 'billingcenter',
                region: 'center'
            }
        ]
    }
);