Ext.define
(
    'Tcitel.view.billing.CustomerBillingNorth',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'billingnorth',
        title: 'Customer Billing',
        header:
        {
            hidden:'true'
        },
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: '20 0 20 20',
            padding: 20,
            defaults:
            {
                xtype: 'textfield',
                labelWidth: 80,
                readOnly: true
            }
        },
        items:
        [
            {
                title: 'Customer Search',
                padding: '20 20 15 20',
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 55,
                    readOnly: false,
                    listeners:
                    {
                        specialkey: 'specialKey'
                    }
                },
                items:
                [
                    {
                        name: 'number',
                        fieldLabel: 'Number',
                        bind: '{search.number}',
                        listeners:
                         {
                            change: 'numberChange'
                         }
                    },
                    {
                        name: 'email',
                        fieldLabel: 'Email',
                       // vtype: 'email',
                        bind: '{search.email}',
                        listeners:
                         {
                            change: 'numberChange'
                         }
                    },
                   
                    {
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'brand',
                        hiddenName: 'brand',
                        reference: 'fooBrand',
                        fieldLabel: 'Brand',
                        brand_access: 'Virtual SIM',
                        bind: {store:'{brand_number}'},
                        allowBlank:false,
                        disabled:true,
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        listeners:
                        {
                            change: 'comboSelect'
                        }
                    },
                    {
                        name: 'user_id',
                        fieldLabel: 'User ID',
                        bind: '{search.user_id}'
                    },
                    {
                        name: 'billing_id',
                        fieldLabel: 'Billing ID',
                        bind: '{search.billing_id}'
                    },
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 30 0 60',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'seach'
                    }
                ]
            },
            {
                xtype: 'form',
                layout: 'hbox',
                margin: 0,
                border: false,
                defaults:
                {
                    xtype: 'fieldset',
                    margin: '0 20 0 0',
                    padding: 20,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 80,
                        readOnly: true
                    }
                },
                items:
                [
                    {
                        title: 'Customer Data',
                      
                        items:
                        [
                            {
                                name: 'user_id',
                                allowBlank: false,
                                preventMark: true,
                                fieldLabel: 'User ID',
                                bind: '{customer.user_id}'
                            },
                            {
                                name: 'email',
                                fieldLabel: 'Email',
                                bind: '{customer.email}'
                            },
                            {
                                name: 'user_type',
                                fieldLabel: 'User Type',
                                bind: '{customer.user_type}'
                            },
                            {
                                name: 'brand',
                                fieldLabel: 'Brand',
                                brand_access: 'Virtual SIM',
                                bind: '{customer.brand}'
                            },
                            {
                                xtype: 'datetimefield',
                                format: 'Y-m-d',
                                name: 'created',
                                fieldLabel: 'Created',
                                bind: '{customer.created}'
                            }
                        ]
                    },
                    {
                        title: 'Billing Details',
                        items:
                        [
                            {
                                name: 'billing_id',
                                allowBlank: false,
                                preventMark: true,
                                fieldLabel: 'Billing ID',
                                bind: '{customer.billing_id}'
                            },
                            {
                                xtype: 'textarea',
                                grow: true,
                                height: 53,
                                width: 255,
                                name: 'balances',
                                fieldLabel: 'Balances',
                                bind: '{customer.balances}'
                            },
                            {
                                xtype: 'textarea',
                                grow: true,
                                height: 53,
                                width: 255,
                                name: 'reservations',
                                fieldLabel: 'Reservations',
                                bind: '{customer.reservations}'
                            }
                        ]
                    },
                    {
                    
                        title: 'Add / Remove Credit',
                        margin: '0 0 20 0',
                        padding: 20,
                        defaults:
                        {
                            allowBlank: false,
                            readOnly: false,
                            labelWidth: 65,
                            listeners:
                            {
                                specialKey: 'specialKeyAdd'
                            }
                        },
                        items:
                        [
                            {
                                xtype: 'numberfield',
                                hideTrigger: true,
                                keyNavEnabled: false,
                                mouseWheelEnabled: false,
                                allowExponential: false,
                                decimalPrecision: 10,
                                name: 'ammount',
                                fieldLabel: 'Amount',
                                bind: '{admin.ammount}'
                            },
                            {
                                xtype: 'textfield',
                                name: 'password',
                                inputType: 'password',
                                fieldLabel: 'Password',
                                bind: '{admin.password}'
                            },
                            {
                                xtype: 'textarea',
                                name: 'info',
                                inputType: 'info',
                                fieldLabel: 'Info',
                                bind: '{admin.info}'
                            },
                            {
                                xtype: 'button',
                                formBind: true,
                                width: 70,
                                margin: '10 0 0 170',
                                text: 'Proceed',
                                handler: 'addCredit'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'form',
                border: false,
                margin: 0,
                defaults:
                {
                    xtype: 'fieldset',
                   
                    defaults:
                    {
                        xtype: 'textfield'
                    }
                },
                items:
                [

                    {
                    
                        title: 'Deactivation/Activation',
                        brand_access: 'Virtual SIM',
                        width: 200,
                        margin: '0 0 20 0',
                        padding: 20,
                        defaults:
                        {
                            
                            readOnly: false,
                            labelWidth: 65,
                            width: 145
                            
                        },
                        items:
                        [
                            {
                                xtype: 'textfield',
                                name: 'user_id',
                                allowBlank: false,
                                preventMark: true,
                                fieldLabel: 'User ID',
                                bind: '{customer.user_id}',
                                hidden: true
                            },
                            {
                                xtype: 'checkbox',
                                name: 'active',
                                allowBlank: true,
                                disabled:true,
                                inputValue:'t',
                                fieldLabel: 'Active',
                                bind: '{customer.active}'
                            },
                            {
                                xtype: 'textfield',
                                name: 'password',
                                inputType: 'password',
                                fieldLabel: 'Password',
                                allowBlank: false,
                                bind: '{admin1.password}'
                            },
                          
                            {
                                xtype: 'button',
                                formBind: true,
                                width: 75,
                                margin: '10 0 0 70',
                                text: 'Proceed',
                                handler: 'deactivation'
                            }
                        ]
                    }
                ]
            }
            /*,
             {
                title: 'Balance Search',
                defaults:
                {
                   // allowBlank: false,
                    readOnly: false,
                    labelWidth: 65,
                    width:150,
                    listeners:
                    {
                        specialKey: 'specialKeyAdd'
                    }
                },
                items:
                [
                    {
                        xtype: 'numberfield',
                        hideTrigger: true,
                        keyNavEnabled: false,
                        mouseWheelEnabled: false,
                        allowExponential: false,
                        name: 'balances',
                        fieldLabel: 'Balance >'//,
                       // bind: '{admin.balances}'
                    },,
                    {
                        xtype: 'textfield',
                        name: 'brand',
                        inputType: 'password',
                        fieldLabel: 'Brand',
                       // bind: '{admin.brand}'
                    },
                    {
                        xtype: 'button',
                        formBind: true,
                        width: 70,
                        margin: '10 0 0 70',
                        text: 'Search',
                        handler: 'edit'
                    }
                ]
            }*/
        ]
    
    }
);
