Ext.define
(
	'Tcitel.view.billing.PhoneNumbers',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'phonenumbers',
		title: 'Phone Numbers',
		bind: {store:'{numbers}'},
		plugins:
		{
            ptype: 'cellediting',
            clicksToEdit: 1
        },
		columns:
		{
            defaults:
            {
            	editor:
            	{
            		xtype:'textfield',
            		readOnly:true
            	},
                flex: 1
            },
			items:
			[
			     {
			    	xtype: 'rownumberer',
			    	width: 40,
			    	flex: false
			    },
			    {
			        text: 'Number',
			        dataIndex: 'number'
			    },
			    {
			        text: 'Type',
			        dataIndex: 'type'
			    },
			    {
			        text: 'Status',
			        dataIndex: 'status'
			    },
			    {
                    text: 'Autorenew',
                    dataIndex: 'auto_renew',
                    xtype: 'checkcolumn',
                    disabled: true,
                    disabledCls : 'x-item-enabled'
                },
			    {
			        text: 'Created',
			        dataIndex: 'created'
			    },
			    {
			        text: 'Expiration Date',
			        dataIndex: 'expiration_date'
			    }
			]
		}
	}
);