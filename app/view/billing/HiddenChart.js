Ext.define
(
    'Tcitel.view.billing.HiddenChart',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'hiddenChart',
        margin: 5,
       frame:true,
       // style: 'backgroundColor: white',
        layout:
        {
            type: 'fit',
            align: 'stretch'
        },
       // border: false,
        initComponent: function()
        {
            var me = this;
            me.items =
            [
                {
                    xtype: 'cartesian',
                    legend:
                    {
                        docked: 'right'
                    },
                    insetPadding:
                    {
                        top: 100,
                        left: 40,
                        right: 40,
                        bottom: 40
                    },
                    sprites:
                    [
                        {
                            type: 'text',
                            text: 'Hourly Payment Transactions',
                            fontSize: 16,
                            width: 160,
                            height: 10,
                            x: 40, // the sprite x position
                            y: 20  // the sprite y position
                         }
                    ],
                    axes:
                    [
                        {
                            type: 'numeric',
                            position: 'left',
                            grid: true,
                            //minimum: 0,
                            title:'Payment'
                        },
                        {
                            type: 'category',
                            position: 'bottom',
                            grid: true,
                            fields: ['sat'],
                            title:'Hour'
                            
                        }
                    ],
                    series:
                    [
                        {
                            type: 'bar',
                            axis: 'left',
                            xField: 'sat',
                            stacked: true,
                            style:
                            {
                                opacity: 0.80
                            },
                            highlight:
                            {
                                fillStyle: 'yellow'
                            },
                            tooltip:
                            {
                                style: 'background: #fff',
                                renderer: function(storeItem, item)
                                {
                                    var network = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                                    this.setHtml(network + ' for ' + storeItem.get('sat') + ' hour'+': ' + storeItem.get(item.field));
                                }
                            }
                        }
                    ]
                }
            ];
            this.callParent();
        }
    }
);