Ext.define
(
    'Tcitel.view.billing.TrialLogDetail',
    {
        extend: 'Ext.grid.Panel',
         xtype: 'trialLogGrid',
         alias: 'widget.trialLogGrid',
        title: 'Trial Log Detail',
        floating: true,
        bind: {store:'{trialLogdetail}'},
        draggable: true,
        closable: true,
        modal: true,
        autoShow: false,
        frame: true,
       
        height: 300,
        viewConfig:
        {
            enableTextSelection: true
        },
         plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },

        
        initComponent: function()
        {
             var me = this;

            me.width = 1100;
            me.columns = [

            {
                text     : 'Device ID',
                flex     : 1,
                dataIndex: 'duid'
            },
            {
                text     : 'User ID',
                flex     : 1,
                //sortable : false//,
                dataIndex: 'user_id'
            },
            {
                text: 'Number',
                dataIndex: 'number',
                 flex     : 1
               // sortable : true,
            },
            {
                text: 'Email',
                dataIndex: 'email',
                 flex     : false,
                  width    : 200
               
            },
            {
                
                text: 'Start Date',
                dataIndex: 'start',
                 flex     : 1,
                 sortable : true

            },
            {
                text: 'End Date',
                dataIndex: 'end',
                flex     : 1,
                sortable : true
            },
            {

                text: 'Brand',
                dataIndex: 'brand',
                flex     : 1
                
            }
        ];

        me.callParent();
        }
    }
);
