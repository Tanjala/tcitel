Ext.define
(
	'Tcitel.view.billing.OrderLog',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'orderlog',
		title: 'Order Log',
		bind: {store:'{orderlog}'},
		columns:
		{
            defaults:
            {
                flex: 1
            },
			items:
			[
			    {
			        text: 'Time',
			        dataIndex: 'timestamp'
			    },
			    {
			        text: 'User ID',
			        dataIndex: 'user_id'
			    },
			    {
			        text: 'Billing ID',
			        dataIndex: 'billing_id'
			    },
			    {
			        text: 'Number',
			        dataIndex: 'number'
			    },
			    {
			        text: 'Type',
			        dataIndex: 'type'
			    },
			    {
			        text: 'Credit',
			        dataIndex: 'credit'
			    },
			    {
			        text: 'Operator',
			        dataIndex: 'operator'
			    }
			]
		}
	}
);