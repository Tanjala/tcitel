Ext.define
(
    'Tcitel.view.billing.BillingViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.billing',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.billing.User',
            'Tcitel.model.billing.Number',
            'Tcitel.model.billing.OrderLog',
            'Tcitel.model.billing.ChargeLog',
            'Tcitel.model.billing.Payment',
            'Tcitel.model.billing.Devices',
            'Tcitel.model.billing.CdrLog'
        ],
        data:
        {
            admin: {},
            admin1:{},
            customer: {},
            total:{},
            charglog:
            {
                name:''
            },
            /* cdrlog:
            {
                
            },*/
            search:
            {
                number: '',
                brand:'',
                email: '',
                user_id: '',
                billing_id: '',
                name:''
               
            },
            searchpay:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                type: '',
                brand:''
            },
            total1:
            {
                GooglePlay:'',
                iTunes:'',
                PayPal:'',
                PayPalWeb:'',
                gui:'',
                totalpayment:''
            }
        },
        stores:
        {
            user:
            {
                model: 'Tcitel.model.billing.User',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/user.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                listeners:
                {
                    load: 'userLoad',
                    beforeload: 'clearStores'
                }
            },
            numbers:
            {
                model: 'Tcitel.model.billing.Number',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/numbers.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true
            },
            devices:
            {
                model: 'Tcitel.model.billing.Devices',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/devices.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true
            },
            orderlog:
            {
                model: 'Tcitel.model.billing.OrderLog',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/orderlog.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true
            },
            chargelog:
            {
                model: 'Tcitel.model.billing.ChargeLog',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/chargelog.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: false
            },

            cdrlog:
            {
                model: 'Tcitel.model.billing.CdrLog',

                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/cdrlog.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: false
                 /*listeners:
                {
                    beforeload: 'cdrLogBeforeLoad'
                }*/
            },
    
    
            payment:
            {
                model: 'Tcitel.model.billing.Payment',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/payment.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad:true
            },
            total:
            {
                model: 'Tcitel.model.billing.Total',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/total.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                listeners:
                {
                    load: 'paymentLoad'
                },
                remoteFilter: true,
                autoLoad: true
            },

            trialLogdetail:
              {
                model: 'Tcitel.model.billing.Transaction',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/triallogdetail.php',
                   
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                /*listeners:
                {
                    load: 'paymentLoad'
                },*/
                remoteFilter: true,
                autoLoad: false
            },

           
            type:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'GooglePlay', value:'6'},
                    {display: 'iTunes', value:'7'},
                    {display: 'PayPal', value:'8'},
                    {display: 'PayPalWeb', value:'9'},
                    {display: 'Bazaar', value:'19'}
                ]
            },
            brand_number:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/brandNumber.php',
                   
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                listeners:
                {
                    load: 'comboLoad'
                },
                autoLoad: false
            },
            brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand'//,
                        //all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            brandPay:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             billing_profile:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'b_billing_profile',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
         
    }
);