Ext.define
(
    'Tcitel.view.billing.BillingController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.billing',

         comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
            //console.log(combo.value);
        },

      /*  comboSelectPay: function(combo, record, eOpts)
        {
            this.getViewModel().get('searchpay')[combo.name] = combo.value;
        },*/
        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },
      /*  search: function(button, e)
        {
           
            var search = this.getViewModel().get('searchpay'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('payment'), filter);
            this.total();
        },*/
        seach: function(button, e)
        {
            
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('user'), filter);
        },
        specialKey: function(field, e)
        {
            // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
            // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
            if (e.getKey() == e.ENTER)
            {
                this.seach();
            }
        },
        reset: function()
        {
            this.getViewModel().set('search', {});
             this.getViewModel().set('admin', {});
            
            var combo=this.lookupReference('billing_type');
            
            combo.reset();
            this.groupSelect(combo);
            this.clearStores();

        },
   
        userLoad: function(store, records, successful, eOpts)
        {
            if(successful && records.length)
            {
                var customer = store.first();
                //debugger;
                this.getViewModel().set('customer', customer);
                this.getStore('numbers').load({params:{'user_id': customer.data.user_id}});
                this.getStore('chargelog').load({params:{'billing_id': customer.data.billing_id}});
                //this.getStore('cdrlog').load({params:{'user_id': customer.data.user_id}});
                this.getStore('cdrlog').load();
                this.getStore('devices').load({params:{'user_id': customer.data.user_id}});
                //this.getStore('orderlog').load({params:{'ids[]': customer.data.ids}});

            }
        },
         comboLoad:  function()
         {
             var combo = this.lookupReference('fooBrand');
             console.log(combo);
             combo.select(combo.getStore().getAt(0));
            
         },

        paymentLoad: function(store, records, successful, eOpts)
        {
             if(successful && records.length)
             {
                 var payment = store.first();
             //debugger;
                 this.getViewModel().set('total', payment);
                
                 //this.getStore('orderlog').load({params:{'ids[]': customer.data.ids}});
             }
        },
        clearStores: function(store, operation, eOpts)
        {
            this.getViewModel().set('customer', {});
            this.getStore('user').loadData([],false);
            this.getStore('numbers').loadData([],false);
            this.getStore('orderlog').loadData([],false);
            this.getStore('chargelog').loadData([],false);
            this.getStore('cdrlog').loadData([],false);
            this.getStore('devices').loadData([],false);
        },

        cdrLogBeforeLoad: function(store, operation, eOpts)
        {
            var customer = this.getViewModel().get('customer');
            var filter = new Ext.util.Filter({
                property: 'user_id',
                value   : customer.data.user_id
            });

            store.setFilters([filter]);
        },

        addCredit:function(component, e)
        {
            var me = this,
            form = component.up('form'),
            customer = this.getViewModel().get('customer'),
            admin = this.getViewModel().get('admin');
            //console.log(form);
            if (!form.getForm().isValid() || admin.ammount == 0) return;
            Ext.Msg.confirm
            (
                'Confirm', 'Commit transaction?',
                function(button)
                {
                    if(button=='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: 'php/billing/addcredit.php',
                                params:
                                {
                                    billing_id: customer.data.billing_id,
                                    brand: customer.data.brand,
                                    ammount: admin.ammount,
                                    password: admin.password,
                                    info:admin.info
                                },
                               success: function( response, options )
                                {
                                    var result = Ext.decode( response.responseText );
                                    if( result.success )
                                    {
                                        Ext.Msg.alert( 'Success', 'Transaction completed!');
                                        me.getViewModel().set('admin', {});
                                        me.seach();
                                    }
                                    else
                                    {
                                        Ext.Msg.alert( 'Failure', 'Transaction error.');
                                    }
                                },
                                failure: function( response, options )
                                {
                                    Ext.Msg.alert( 'Failure', 'Transaction error.');
                                }
                            }
                        );
                    }
                }
            );
        },

         deactivation:function(component, e)
        {
            var me = this,
            form = component.up('form'),
            customer = this.getViewModel().get('customer'),
            admin1 = this.getViewModel().get('admin1');
            //console.log(form);
            if (!form.getForm().isValid()) return;
            Ext.Msg.confirm
            (
                'Confirm', customer.data.active == 't' ? 'Confirm deactivation?' : 'Confirm activation?',
                function(button)
                {
                    if(button=='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: 'php/billing/deactivation.php',
                                params:
                                {
                                    active:customer.data.active,
                                    user_id: customer.data.user_id,
                                    brand: customer.data.brand,
                                    password: admin1.password,
                                    info:admin1.info
                                },
                               success: function( response, options )
                                {
                                    var result = Ext.decode( response.responseText );

                                    if( result.success )
                                    {
                                        Ext.Msg.alert( 'Success', customer.data.active == 't' ? 'Deactivation completed!' : 'Activation completed!');
                                        me.getViewModel().set('admin1', {});
                                        me.seach();
                                    }
                                    else
                                    {
                                        Ext.Msg.alert( 'Failure', customer.data.active == 't' ? 'Deactivation error!' : 'Activation error!');
                                    }
                                },
                                failure: function( response, options )
                                {
                                    Ext.Msg.alert( 'Failure',customer.data.active == 't' ? 'Deactivation error!' : 'Activation error!');
                                }
                            }
                        );
                    }
                }
            );
        },
      
        groupSelect: function(combo, record, eOpts)
        {
            var filter = [{property: 'name', value: combo.value}];
            //console.log(filter);
            this.filterStore(this.getStore(combo.name), filter);
        },
           edit: function(button, e)
        {
            var record = button.up('form');
            //.getWidgetRecord();
            //console.log(record);
            var partnersedit = Ext.widget('balancesearch');
            partnersedit.loadRecord(record);
            this.getView().add(partnersedit);
            partnersedit.show();
        },

         numberChange: function(field, newValue, oldValue, eOpts)
        
        {
            //console.log(field.name);
            if (Tcitel.LoggedInUser.data.brand != 'Virtual SIM') return ;

            var combo= field.up('billingnorth').down('combo[name=brand]'),
                fieldNumber= field.up('billingnorth').down('field[name=number]'),
                fieldEmail= field.up('billingnorth').down('field[name=email]');


            if(field.name == 'number' && newValue != '' )
                    {
                        fieldEmail.reset();
                    }
            if(field.name == 'email' && newValue != '')
                    {
                        fieldNumber.reset();
                    }
           
            if (newValue == '' )
                {
                    combo.setValue('');
                    combo.disable();
                }
            else
                {   
                    combo.enable();
                    //combo.setValue('');
                    combo.getStore('brand_number').load({params: {number:newValue, email:newValue}});

                }
        },
         emailChange: function( field , isValid , eOpts ) 
         {
            if (Tcitel.LoggedInUser.data.brand != 'Virtual SIM') return ;

            var combo= field.up('billingnorth').down('combo[name=brand]'),
                fieldNumber= field.up('billingnorth').down('field[name=number]');
            
             console.log(isValid);
             console.log(field.value);
             
             if(field.value!='')
             {
                 fieldNumber.reset();
             }
             if (field.value == '' )
                {
                    combo.setValue('');
                    combo.disable();
                }
            else 
                {   
                    combo.enable();
                    //combo.setValue('');
                    if(isValid)
                    {
                        combo.getStore('brand_number').load({params: {email:field.value}});
                    }

                }
         },

       
         triallog: function(button)
        {
            var record = button.up('container').getWidgetRecord();
            var transactionGrid = Ext.widget('trialLogGrid');
            this.getView().add(transactionGrid);
            this.getStore('trialLogdetail').load({params: {duid:record.data.duid}});
            transactionGrid.show();
        },

     
        specialKeyAdd: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                if (field.name != 'password')
                {
                    field.nextSibling().focus(false,true);
                }
                else
                {
                    this.addCredit( field, e) ;
                }
            }
        }
    }
);
