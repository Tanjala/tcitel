Ext.define('Tcitel.view.messagestat.hourly.HourlyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.hourlystat',
   
    comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },
          search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [
            {property: 'day', value: search.day},
            {property: 'direction', value: search.direction},
             {property: 'brand', value: search.brand}];
            
            this.filterStore(this.getStore('hourly'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    day: Ext.Date.format(new Date(), 'Y-m-d'),
                    direction:'',
                    brand: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },
        change: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('search')['day'] = field.getRawValue();
           
        }
});
