Ext.define
(
    'Tcitel.view.messagestat.hourly.Hourly',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'messagehourlystat',
        itemId:'messagehourly',
        border: false,
        //itemId: 'messagehourlystat',
        controller: 'hourlystat',
       
        viewModel:
        {
            type: 'hourlystat'
        },
        requires:
        [
              'Tcitel.view.messagestat.hourly.CenterChart',
              'Tcitel.view.messagestat.hourly.HourlyController', 
              'Tcitel.view.messagestat.hourly.HourlyViewModel',
              'Tcitel.view.messagestat.hourly.Search'//,
            
        ],
        layout: 'border',
        bodyStyle: 'background:white',

        defaults:
        {
            xtype: 'label'
        },

        items:
        [
            {
                xtype: 'panel',
                region: 'west',
                border:false,
                items:
                [
                   {
                     xtype: 'hourlystatwest',
                     region: 'north'
                   },
                   {
                     region: 'center'
                   }
                ]
            },

            {
                xtype: 'hourlystatcenter',
                region: 'center'
            }
        ]
    }
);
