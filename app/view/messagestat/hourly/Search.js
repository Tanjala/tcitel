Ext.define
(
    'Tcitel.view.messagestat.hourly.Search',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'hourlystatwest',
        title: 'Hourly <i class="fa fa-bar-chart"></i>',
        layout:
	        {
	            type: 'vbox'
	        },
	    margin: 30 ,
	    frame: true,
        width: 225,
        height:220,
        defaults:
                {
                   //margin: '20 10 0 15',
                    labelWidth: 55,
                    width: 180
                },

         items:
        [

            
               {
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        margin: '20 10 0 15',
                        editable: false,
                        value: new Date(),
                        name: 'day',
                        fieldLabel: 'Date',
                        listeners:
                        {
                            change: 'change'
                        }
                },
              {
                        xtype: 'combo',
                        margin: '13 10 0 15',
                        name: 'direction',
                        hiddenName: 'direction',
                        fieldLabel: 'Direction',
                        bind: {store:'{direction}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
             {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        margin: '13 10 0 15',
                        bind: {store:'{brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                 },
                {
                    text: 'Apply',
                    handler: 'search',
                    formBind: true,
                    xtype:'button',
                    width: 55,
                    margin: '13 0 0 135'

                }
         ]
     
     }
 );       