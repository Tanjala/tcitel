Ext.define
(
    'Tcitel.view.messagestat.daily.Daily',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'messagedailystat',
        itemId:'messagedaily',
        border: false,
        controller: 'messagedailystat',
       
        viewModel:
        {
            type: 'messagedailystat'
        },
        requires:
        [
              'Tcitel.view.messagestat.daily.CenterChart',
              'Tcitel.view.messagestat.daily.DailyController',
              'Tcitel.view.messagestat.daily.DailyViewModel',
              'Tcitel.view.messagestat.daily.Search'//,
            
        ],
        layout: 'border',
        bodyStyle: 'background:white',

        defaults:
        {
            xtype: 'label'
        },

        items:
        [
            {
                xtype: 'panel',
                region: 'west',
                border:false,
                items:
                [
                   {
                     xtype: 'messagedailystatwest',
                     region: 'north'
                   },
                   {
                     region: 'center'
                   }
                ]
            },

            {
                xtype: 'messagedailystatcenter',
                region: 'center'
            }
        ]
    }
);
