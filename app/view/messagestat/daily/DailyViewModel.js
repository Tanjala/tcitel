Ext.define
(
    'Tcitel.view.messagestat.daily.DailyViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.messagedailystat',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.messagestat.Daily'
        ],
        data:
        {
            search:
            {
                startday: Tcitel.controller.common.Common.monthBeforeDate(),
                endday: Ext.Date.format(new Date(), 'Y-m-d'),
                direction:'',
                brand: ''
            }   
                
        },
       stores:
        {
            daily:
            {
                model: 'Tcitel.model.messagestat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/messagestat/daily/read.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
            brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             direction:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'APP2APP', value:'APP2APP'},
                    {display: 'APP2SMPP', value:'APP2SMPP'},
                    //{display: 'APP2WEB', value:'APP2WEB'},
                    {display: 'SMPP2APP', value:'SMPP2APP'}//,
                    //{display: 'WEB2APP', value:'WEB2APP'}//,
                    //{display: 'APP', value:'<'}
                ]
            }
        }
    }
);
