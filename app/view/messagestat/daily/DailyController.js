Ext.define('Tcitel.view.messagestat.daily.DailyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.messagedailystat',
   
    comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },
         search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [
            {property: 'startday', value: search.startday},
            {property: 'endday', value: search.endday},
            {property: 'direction', value: search.direction},
             {property: 'brand', value: search.brand}];
            
            this.filterStore(this.getStore('daily'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    startday:Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    direction:'',
                    brand: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },

        change: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('search')[field.name] = field.getRawValue();
        }
       /* search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('daily'), filter);
        }*/
});
