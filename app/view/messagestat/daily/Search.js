Ext.define
(
    'Tcitel.view.messagestat.daily.Search',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'messagedailystatwest',
        title: 'Daily <i class="fa fa-bar-chart"></i>',
        layout:
	        {
	            type: 'vbox'
	        },
	    margin: 30 ,
	    frame: true,
        width: 225,
        height:250,
        defaults:
                {
                   //margin: '20 10 0 15',
                    labelWidth: 55,
                    width: 180
                },

         items:
        [

            
             {
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    margin: '20 10 0 15',
                   // width: 180,
                    editable: false,
                    //labelWidth: 50,
                    listeners:
                    {
                        change: 'change'
                    },
                        value: Tcitel.controller.common.Common.monthBeforeDate(),
                        name: 'startday',
                        fieldLabel: 'Start'
             },
             {          
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    margin: '13 10 0 15',
                    //width: 180,
                    editable: false,
                    //labelWidth: 50,
                    listeners:
                    {
                        change: 'change'
                    },
                        value: Ext.Date.format(new Date(), 'Y-m-d'),
                        name: 'endday',
                        fieldLabel: 'End'
              },
              {
                        xtype: 'combo',
                        margin: '13 10 0 15',
                        name: 'direction',
                        hiddenName: 'direction',
                        fieldLabel: 'Direction',
                        bind: {store:'{direction}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
             {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        margin: '13 10 0 15',
                        bind: {store:'{brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                 },
                {
                    text: 'Apply',
                    handler: 'search',
                    formBind: true,
                    xtype:'button',
                    width: 55,
                    margin: '13 0 0 135'

                }
         ]
     
     }
 );       