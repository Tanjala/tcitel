Ext.define
(
    'Tcitel.view.messagestat.daily.CenterChart',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'messagedailystatcenter',
        margin: 30,
        frame:true,
        //style: 'backgroundColor: white',
        layout:
        {
            type: 'fit',
            align: 'stretch'
        },
        initComponent: function()
        {
            var me = this;
            me.items =
            [
                {
                    xtype: 'cartesian',
                    legend:
                    {
                        docked: 'right'
                    },
                    bind : {store: '{daily}'},
                    insetPadding:
                    {
                        top: 40,
                        left: 40,
                        right: 40,
                        bottom: 20
                    },
                    axes:
                    [
                        {
                            type: 'numeric',
                            position: 'left',
                            grid: true,
                            minimum: 0,
                            title:'Amount'
                        },
                        {
                            type: 'category',
                            position: 'bottom',
                            grid: true,
                            fields: ['day'],
                            title:'Days',
                            label:
                            {
                                rotate: { degrees: -60 }
                            }
                        }
                    ],
                    series:
                    [
                        {
                            type: 'bar',
                            axis: 'left',
                            title: [ 'Delivered', 'Undelivered' ],
                            xField: 'day',
                            yField: [ 'DELIVERED', 'UNDELIVERED' ],
                            colors : ['#A7BD2F', '#00A0B0'],
                            stacked: true,
                            style:
                            {
                                opacity: 0.80
                            },
                            highlight:
                            {
                                fillStyle: 'yellow'
                            },
                            tooltip:
                            {
                                style: 'background: #fff',
                                renderer: function(storeItem, item)
                                {
                                    var network = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                                    this.setHtml(network + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.field));
                                }
                            }
                        }
                    ]
                }
            ];
            this.callParent();
        }
    }
);