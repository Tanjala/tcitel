Ext.define
(
    'Tcitel.view.stat.app2tel.daily.App2telDailyStatController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.a2t_daily',

        comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

        search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [{property: 'startday', value: search.startday},
                      {property: 'endday', value: search.endday},
                      {property: 'brand', value: search.brand},
                      {property: 'route', value: search.route}];
            this.filterStore(this.getStore('dailystat'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    startday: Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand: '',
                    route: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },

        change: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('search')[field.name] = field.getRawValue();
        }
    }
);
