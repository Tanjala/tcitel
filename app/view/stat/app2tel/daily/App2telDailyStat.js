Ext.define
(
    'Tcitel.view.stat.app2tel.daily.App2telDailyStat',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2t_daily',
        title: 'Daily Stat App2Tel',
        border: false,
        itemId: 'a2t_daily',
        controller: 'a2t_daily',
       
        viewModel:
        {
            type: 'a2t_daily'
        },
        requires:
        [

            'Tcitel.view.stat.app2tel.daily.West',
            'Tcitel.view.stat.app2tel.daily.Center',
            'Tcitel.view.stat.app2tel.daily.App2telDailyStatController',
            'Tcitel.view.stat.app2tel.daily.App2telDailyStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'a2t_daily'
        },
        items:
        [
            {
               // border: false,
                xtype: 'a2t_daily_west',
                region: 'west'
            },
            {
               // border: false,
                xtype: 'a2t_daily_center',
                region: 'center'
            }
        ]
    }
);
