Ext.define
(
    'Tcitel.view.stat.app2tel.daily.App2telDailyStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.a2t_daily',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.stat.Stat'
        ],
        data:
        {
            search:
            {
                startday: Tcitel.controller.common.Common.monthBeforeDate(),
                endday: Ext.Date.format(new Date(), 'Y-m-d'),
                brand: '',
                route: ''
            }
        },
        stores:
        {
            dailystat:
            {
                storeId: 'dailystat',
                model: 'Tcitel.model.stat.Stat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/dailystat/stat.php',
                    extraParams:
                    {
                        call_type: 'app2tel'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
              brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
              route:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_routes',
                        valueField: 'route',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
