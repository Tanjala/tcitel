Ext.define
(
    'Tcitel.view.stat.app2tel.daily.West',
    {
        extend: 'Ext.form.Panel',
        xtype: 'a2t_daily_west',
        layout: 'hbox',
        border: false,
        items:
        [
            {
                xtype: 'fieldset',
                margin: 20,
                padding: 20,
                title: 'Filter',
                defaults:
                {
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    width: 180,
                    editable: false,
                    labelWidth: 50,
                    listeners:
                    {
                        change: 'change'
                    }
                },
                items:
                [
                    {
                        value: Tcitel.controller.common.Common.monthBeforeDate(),
                        name: 'startday',
                        fieldLabel: 'Start'
                    },
                    {
                        value: Ext.Date.format(new Date(), 'Y-m-d'),
                        name: 'endday',
                        fieldLabel: 'End'
                    },
                    {
                        xtype: 'combo',
                        labelWidth: 50,
                        width: 180,
                        //pading: '10 0 10 0',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'combo',
                        labelWidth: 50,
                        width: 180,
                        //pading: '10 0 10 0',
                        name: 'route',
                        hiddenName: 'route',
                        fieldLabel: 'Route',
                        bind: {store:'{route}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Apply',
                        handler: 'search',
                        margin: '10 0 0 110',
                        width: 70
                    }
                ]
            }
        ]
    }
);
