Ext.define
(
    'Tcitel.view.stat.app2tel.hourly.App2telHourly',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2t_hourly',
        title: 'Hourly Stat App2Tell',
        border: false,
        itemId: 'a2t_hourly',
        controller: 'a2t_hourly',
        header:
        {
            //title: 'Hourly Stat',
            border: false
        },
        viewModel:
        {
            type: 'a2t_hourly'
        },
        requires:
        [

            'Tcitel.view.stat.app2tel.hourly.West',
            'Tcitel.view.stat.app2tel.hourly.Center',
            'Tcitel.view.stat.app2tel.hourly.App2telStatController',
            'Tcitel.view.stat.app2tel.hourly.App2telStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'a2t_hourly'
        },
        items:
        [
            {
                border: false,
                xtype: 'a2t_hourly_west',
                region: 'west'
            },
            {
                border: false,
                xtype: 'a2t_hourly_center',
                region: 'center'
            }
        ]
    }
);
