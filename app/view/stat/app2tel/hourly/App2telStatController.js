Ext.define
(
    'Tcitel.view.stat.app2tel.hourly.App2telStatController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.a2t_hourly',

        comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

        search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [{property: 'day', value: search.day},
                  {property: 'brand', value: search.brand},
                  {property: 'route', value: search.route}];
            this.filterStore(this.getStore('cdrstat'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    day: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand: '',
                    route: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },

        change: function(field, newValue, oldValue, eOpts)
        {
           this.getViewModel().get('search')['day'] = field.getRawValue();
        }
    }
);
