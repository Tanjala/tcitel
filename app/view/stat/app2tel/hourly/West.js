Ext.define
(
    'Tcitel.view.stat.app2tel.hourly.West',
    {
        extend: 'Ext.form.Panel',
        xtype: 'a2t_hourly_west',
        layout: 'hbox',
        items:
        [
            {
                xtype: 'fieldset',
                margin: 20,
                padding: 20,
                title: 'Filter',
                items:
                [
                    {
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        width: 180,
                        editable: false,
                        value: new Date(),
                        labelWidth: 50,
                        name: 'day',
                        fieldLabel: 'Date',
                        listeners:
                        {
                            change: 'change'
                        }
                    },
                    {
                        xtype: 'combo',
                        labelWidth: 50,
                        width: 180,
                        //pading: '10 0 10 0',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                     {
                        xtype: 'combo',
                        labelWidth: 50,
                        width: 180,
                        //pading: '10 0 10 0',
                        name: 'route',
                        hiddenName: 'route',
                        fieldLabel: 'Route',
                        bind: {store:'{route}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Apply',
                        handler: 'search',
                        margin: '10 0 0 110',
                        width: 70
                    }
                ]
            }
        ]
    }
);
