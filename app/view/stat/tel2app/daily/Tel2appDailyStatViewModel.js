Ext.define
(
    'Tcitel.view.stat.tel2app.daily.Tel2appDailyStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.t2a_daily',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.stat.Stat'
        ],
        data:
        {
            search:
            {
                startday: Tcitel.controller.common.Common.monthBeforeDate(),
                endday: Ext.Date.format(new Date(), 'Y-m-d'),
                brand: '',
                route: ''
            }
        },
        stores:
        {
            dailystat:
            {
                storeId: 'dailystat',
                model: 'Tcitel.model.stat.Stat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/dailystat/stat.php',
                    extraParams:
                    {
                        call_type: 'tel2app'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
              brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
              route:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_routes',
                        valueField: 'route',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
