Ext.define
(
    'Tcitel.view.stat.tel2app.daily.Tel2appDailyStat',
    {
        extend: 'Ext.panel.Panel',
        xtype: 't2a_daily',
        title: 'Hourly Stat Tel2App',
        border: false,
        itemId: 't2a_daily',
        controller: 't2a_daily',
       
        viewModel:
        {
            type: 't2a_daily'
        },
        requires:
        [

            'Tcitel.view.stat.tel2app.daily.West',
            'Tcitel.view.stat.tel2app.daily.Center',
            'Tcitel.view.stat.tel2app.daily.Tel2appDailyStatController',
            'Tcitel.view.stat.tel2app.daily.Tel2appDailyStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 't2a_daily'
        },
        items:
        [
            {
               // border: false,
                xtype: 't2a_daily_west',
                region: 'west'
            },
            {
               // border: false,
                xtype: 't2a_daily_center',
                region: 'center'
            }
        ]
    }
);
