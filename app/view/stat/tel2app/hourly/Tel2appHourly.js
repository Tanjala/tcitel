Ext.define
(
    'Tcitel.view.stat.tel2app.hourly.Tel2appHourly',
    {
        extend: 'Ext.panel.Panel',
        xtype: 't2a_hourly',
        title: 'Hourly Stat Tel2App',
        border: false,
        itemId: 't2a_hourly',
        controller: 't2a_hourly',
        header:
        {
            //title: 'Hourly Stat',
            border: false
        },
        viewModel:
        {
            type: 't2a_hourly'
        },
        requires:
        [

            'Tcitel.view.stat.tel2app.hourly.West',
            'Tcitel.view.stat.tel2app.hourly.Center',
            'Tcitel.view.stat.tel2app.hourly.Tel2appStatController',
            'Tcitel.view.stat.tel2app.hourly.Tel2appStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 't2a_hourly'
        },
        items:
        [
            {
                border: false,
                xtype: 't2a_hourly_west',
                region: 'west'
            },
            {
                border: false,
                xtype: 't2a_hourly_center',
                region: 'center'
            }
        ]
    }
);
