Ext.define
(
    'Tcitel.view.stat.tel2app.hourly.Tel2appStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.t2a_hourly',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.stat.Stat'
        ],
        data:
        {
            search:
            {
                day: Ext.Date.format(new Date(), 'Y-m-d'),
                brand: '',
                route: ''
            }
        },
        stores:
        {
            cdrstat:
            {
                storeId: 'cdrstat',
                model: 'Tcitel.model.stat.Stat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/cdrstat/stat.php',
                    extraParams:
                    {
                        call_type: 'tel2app'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
              brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
              route:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_routes',
                        valueField: 'route',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
