Ext.define
(
    'Tcitel.view.stat.app2app.hourly.App2appHourly',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2a_hourly',
        title: 'Hourly Stat App2App',
        border: false,
        itemId: 'a2a_hourly',
        controller: 'a2a_hourly',
       /* header:
        {
            //title: 'Hourly Stat',
            border: false
        },*/
        viewModel:
        {
            type: 'a2a_hourly'
        },
        requires:
        [

            'Tcitel.view.stat.app2app.hourly.West',
            'Tcitel.view.stat.app2app.hourly.Center',
            'Tcitel.view.stat.app2app.hourly.App2appStatController',
            'Tcitel.view.stat.app2app.hourly.App2appStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'a2a_hourly'
        },
        items:
        [
            {
               // border: false,
                xtype: 'a2a_hourly_west',
                region: 'west'
            },
            {
                //border: false,
                xtype: 'a2a_hourly_center',
                region: 'center'
            }
        ]
    }
);
