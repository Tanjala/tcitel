Ext.define
(
    'Tcitel.view.stat.app2app.hourly.App2appStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.a2a_hourly',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.stat.Stat'
        ],
        data:
        {
            search:
            {
                day: Ext.Date.format(new Date(), 'Y-m-d'),
                brand: ''
            }
        },
        stores:
        {
            cdrstat:
            {
                storeId: 'cdrstat',
                model: 'Tcitel.model.stat.Stat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/cdrstat/stat.php',
                    extraParams:
                    {
                        call_type: 'app2app'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
              brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
