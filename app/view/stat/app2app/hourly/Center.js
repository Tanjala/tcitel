Ext.define
(
    'Tcitel.view.stat.app2app.hourly.Center',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2a_hourly_center',
        //title: '&nbsp;',
      
        layout: 'fit',
        autoRender:true,
        border: false,

        items:
        [
            {
                xtype: 'cartesian',
                border: false,
                //store: Ext.getStore('cdrstat'),
                bind: {store: 'cdrstat'},
               

                insetPadding: 50,

                interactions: ['panzoom'],

                legend:
                {
                    docked: 'right',
                    border: false
                },

                //define x and y axis.
                axes:
                [
                    {
                        type: 'numeric',
                        position: 'left',
                        grid: true,
                        minimum: -1,
                        title: 'Amount'

                    },
                    {
                        type: 'category',
                        //visibleRange: [0, 1],
                        position: 'bottom',
                        grid: true,
                        title:'Hours'
                    }
                ],

                //define the actual series
                series:
                [
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'ANSWERED',
                        title: 'ANSWERED',
                        marker:
                        {
                            type: 'cross',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style:
                         {
                            
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'BUSY',
                        title: 'BUSY',
                        marker:
                        {
                            type: 'triangle',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                           lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'NO ANSWER',
                        title: 'NO ANSWER',
                        marker:
                        {
                            type: 'square',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style:
                        {
                         
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'FAILED',
                        title: 'FAILED',
                        marker:
                        {
                            type: 'arrow',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                            lineWidth: 1.5
                        }
                    },
                     {
                        type: 'line',
                        xField: 'hour',
                        yField: 'CONGESTION',
                        title: 'CONGESTION',
                        marker:
                        {
                            type: 'circle',
                            fx:
                            {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                         
                            lineWidth: 1.5
                        }
                    },
                     {
                        type: 'line',
                        xField: 'hour',
                        yField: 'billmin',
                        title: 'BILLMIN',
                        marker:
                        {
                            type: 'circle',
                            fx:
                            {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ' hour: ' + storeItem.get(item.series.getYField())+' min');
                            }
                        },
                        style: 
                        {
                         
                            lineWidth: 1.5
                        }
                    }
                ]
            }
        ]


    }
);