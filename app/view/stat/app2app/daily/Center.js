Ext.define
(
    'Tcitel.view.stat.app2app.daily.Center',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2a_daily_center',
        //title: '&nbsp;',

        layout: 'fit',

        border: false,

        items:
        [
            {
                xtype: 'cartesian',
                border: false,
                bind: {store: 'dailystat'},

                insetPadding: 50,

                interactions: ['panzoom'],

                legend:
                {
                    docked: 'right',
                    border: false
                },

                axes:
                [
                    {
                        type: 'numeric',
                        position: 'left',
                        grid: true,
                        minimum: -1,
                        title: 'Amount'
                    },
                    {
                        type: 'category',
                        //visibleRange: [0, 1],
                        position: 'bottom',
                        grid: true,
                        title:'Days',
                        label: 
                        {
                            rotate: 
                            {
                                degrees: -45
                            }
                        }
                    }
                ],

                //define the actual series
                series:
                [
                    {
                        type: 'line',
                        xField: 'day',
                        yField: 'ANSWERED',
                        title: 'ANSWERED',
                        marker:
                        {
                            type: 'cross',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style:
                         {
                          
                            lineWidth: 1.5
                          }
                    },
                    {
                        type: 'line',
                        xField: 'day',
                        yField: 'BUSY',
                        title: 'BUSY',
                        marker:
                        {
                            type: 'triangle',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style:
                        {
                     
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'day',
                        yField: 'NO ANSWER',
                        title: 'NO ANSWER',
                        marker:
                        {
                            type: 'square',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                          
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'day',
                        yField: 'FAILED',
                        title: 'FAILED',
                        marker:
                        {
                            type: 'arrow',
                            fx:
                            {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                         
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'day',
                        yField: 'CONGESTION',
                        title: 'CONGESTION',
                        marker:
                        {
                            type: 'circle',
                            fx:
                            {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: 
                        {
                         
                            lineWidth: 1.5
                        }
                    },
                     {
                        type: 'line',
                        xField: 'day',
                        yField: 'billmin',
                        title: 'BILLMIN',
                        marker:
                        {
                            type: 'circle',
                            fx:
                            {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('day') + ': ' + storeItem.get(item.series.getYField()) +' min');
                            }
                        },
                        style: 
                        {
                         
                            lineWidth: 1.5
                        }
                    }
                ]
            }
        ]


    }
);