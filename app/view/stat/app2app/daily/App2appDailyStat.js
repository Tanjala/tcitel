Ext.define
(
    'Tcitel.view.stat.app2app.daily.App2appDailyStat',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'a2a_daily',
        title: 'Daily Stat App2App',
        border: false,
        itemId: 'a2a_daily',
        controller: 'a2a_daily',
       
        viewModel:
        {
            type: 'a2a_daily'
        },
        requires:
        [

            'Tcitel.view.stat.app2app.daily.West',
            'Tcitel.view.stat.app2app.daily.Center',
            'Tcitel.view.stat.app2app.daily.App2appDailyStatController',
            'Tcitel.view.stat.app2app.daily.App2appDailyStatViewModel'
        ],
        layout: 'border',
         bodyStyle: 'background:white',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'a2a_daily'
        },
        items:
        [
            {
               // border: false,
                xtype: 'a2a_daily_west',
                region: 'west'
            },
            {
               // border: false,
                xtype: 'a2a_daily_center',
                region: 'center'
            }
        ]
    }
);
