Ext.define
(
    'Tcitel.view.stat.Stat',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'stat',
        title:'Cdr <i class="fa fa-line-chart"></i>',
        requires:
        [
           'Tcitel.view.menucardtab.MenuCardTab',
           'Tcitel.view.stat.app2app.hourly.App2appHourly',
           'Tcitel.view.stat.app2tel.hourly.App2telHourly',
           'Tcitel.view.stat.tel2app.hourly.Tel2appHourly',
           'Tcitel.view.stat.app2app.daily.App2appDailyStat',
           'Tcitel.view.stat.app2tel.daily.App2telDailyStat',
            'Tcitel.view.stat.tel2app.daily.Tel2appDailyStat'//,
           /* 'Tcitel.view.stat.StatController',
            'Tcitel.view.stat.StatViewModel',
                       */
        ],

        controller: 'pages',

        initComponent: function()
        {
            me =  this;
            this.callParent();
        },
        items:
        [

            {
                border: false,
                title: 'App2App',
                xtype: 'menucardtab',
                items:
                [
                    {
                        border: false,
                       xtype: 'a2a_hourly',
                       title: 'Hourly Stat'
                    },
                    {

                        itemId: 'DailyStat',
                        xtype: 'a2a_daily',
                        title: 'Daily Stat'
                    }
                ]
            },
            {
                border: false,
                title: 'App2Tel',
                xtype: 'menucardtab',
                items:
                [
                    {
                        border: false,
                       xtype: 'a2t_hourly',
                       title: 'Hourly Stat'
                    },
                    {

                        itemId: 'DailyStat',
                        xtype: 'a2t_daily',
                        title: 'Daily Stat'
                    }
                ]
            },
            {
                border: false,
                title: 'Tel2App',
                xtype: 'menucardtab',
                items:
                [
                    {
                        border: false,
                        xtype: 't2a_hourly',
                        title: 'Hourly Stat'
                    },
                    {

                        itemId: 'DailyStat',
                        xtype: 't2a_daily',
                        title: 'Daily Stat'
                    }
                ]
            },
            {   // Fill all available space
                tabConfig :
                {
                    xtype : 'tbfill'
                }
            }
        ]
    }
);