Ext.define
(
    'Tcitel.view.paymentstat.ChartPaymentType',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'chart_payment_type',
        margin: 5,
       frame:true,
       // style: 'backgroundColor: white',
        layout:
        {
            type: 'fit',
            align: 'stretch'
        },
       // border: false,
        initComponent: function()
        {
            var me = this;
            me.items =
            [
                {
                    xtype: 'cartesian',
                    legend:
                    {
                        docked: 'right'
                    },
                   bind : {store: '{paymentstat}'},
                    insetPadding:
                    {
                        top: 100,
                        left: 40,
                        right: 40,
                        bottom: 40
                    },
                    sprites:
                    [
                        {
                            type: 'text',
                            text: 'Hourly Payment Transactions',
                            fontSize: 16,
                            width: 160,
                            height: 10,
                            x: 40, // the sprite x position
                            y: 20  // the sprite y position
                         }
                    ],
                    axes:
                    [
                        {
                            type: 'numeric',
                            position: 'left',
                            grid: true,
                            //minimum: 0,
                            title:'Payment'
                        },
                        {
                            type: 'category',
                            position: 'bottom',
                            grid: true,
                            fields: ['sat'],
                            title:'Hour'
                            
                        }
                    ],
                    series:
                    [
                        {
                            type: 'bar',
                            axis: 'left',
                            title: [ 'GooglePlay', 'iTunes', 'PayPal', 'PayPalWeb','TopUp', 'AIK Banka','Bazaar', 'Gui' ],
                            xField: 'sat',
                            yField: [ 'GooglePlay', 'iTunes', 'PayPal', 'PayPalWeb','TopUp', 'AIK Banka', 'Bazaar', 'gui' ],
                            colors : ['#A7BD2F', '#00A0B0', '#b62020', '#ff9900', '#ead61c','#d7c797','#b00299',  '#666547' ],
                            stacked: true,
                            style:
                            {
                                opacity: 0.80
                            },
                            highlight:
                            {
                                fillStyle: 'yellow'
                            },
                            tooltip:
                            {
                                style: 'background: #fff',
                                renderer: function(storeItem, item)
                                {
                                    var network = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                                    this.setHtml(network + ' for ' + storeItem.get('sat') + ' hour'+': ' + storeItem.get(item.field));
                                }
                            }
                        }
                    ]
                }
            ];
            this.callParent();
        }
    }
);