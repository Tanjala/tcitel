Ext.define
(
    'Tcitel.view.paymentstat.Stat',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'stat_payment',
        title: 'Statistic',
        requires:
        [
            'Tcitel.view.paymentstat.Charts',
             'Tcitel.view.paymentstat.Search',
             //'Tcitel.view.paymentstat.Grid',
             //'Tcitel.view.paymentstat.Donut',
             'Tcitel.view.paymentstat.PaymentTab'
          
        ],
    

     layout:
        {
            type: 'hbox',
            align: 'stretch'
        },

        defaults:
        {
            xtype: 'panel',
            bodyStyle: 'background:white',
            border:false
        },
        items:
        [
            {
                width: 250,

                layout: 'border',
 
                items:
                [
                    {
                        title:'Search',
                        region: 'north',
                         xtype: 'search_payment'
                      
                    }/*,
                    {
                        xtype: 'pie-donut',
                        region:'center',
                        border:false
                    }*/
                ]
            },
            {
                flex: 1,
                  border: false,
                  margin: 0,
                 layout:
                {
                    type: 'fit'
                },
               
                items:
                [
                    {
                        xtype: 'paymentstat_tab'
                    }
                  
                ]
            }
        ]


     
    }
);