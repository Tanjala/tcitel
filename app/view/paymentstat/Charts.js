Ext.define
(
    'Tcitel.view.paymentstat.Charts',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartPayment',
        title: ' Hourly <i class="fa fa-bar-chart"></i>',
         //border: false,
        requires:
        [
           'Tcitel.view.paymentstat.ChartPaymentType'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
           
            {
                xtype: 'chart_payment_type',
                //width:'50%',
                 region: 'center'
            }
        ]
    }
);