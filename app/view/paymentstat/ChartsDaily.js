Ext.define
(
    'Tcitel.view.paymentstat.ChartsDaily',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartPaymentDaily',
        title: ' Daily <i class="fa fa-bar-chart"></i>',
         //border: false,
        requires:
        [
           'Tcitel.view.paymentstat.ChartPaymentTypeDaily'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
           
            {
                xtype: 'chart_payment_type_daily',
                //width:'50%',
                 region: 'center'
            }
        ]
    }
);