Ext.define
(
    'Tcitel.view.paymentstat.PaymentTab',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'paymentstat_tab',
        itemId:'paymentstat_tab',
        defaults: 
        {
            listeners: 
            {
                activate:'activateTab'       
             }
        },
        border:false,
        requires:
        [
           'Tcitel.view.paymentstat.Charts',
           'Tcitel.view.paymentstat.ChartsDaily'
           
        ],
    	items:
        [
            {
                xtype: 'chartPayment'
            },
            {
                xtype: 'chartPaymentDaily'
            }
            /*,
            {
                xtype: 'chartNumbersExpired'
            },
            {
                xtype: 'chartNumbersVirtual'
            },
            {
                xtype: 'chartNumbersReal'
            },
            {
                xtype: 'dataStat'
            }*/
        ]
    }
);