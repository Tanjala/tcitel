Ext.define
(
	'Tcitel.view.spofnumbers.NumbersController',
	{
	    extend: 'Ext.app.ViewController',
	    alias: 'controller.spofnumbers',

	    comboSelect: function(combo, record, eOpts)
	    {
            var name = combo.name;
	    	this.getViewModel().get('search')[name] = combo.value;
	    },
        comboSelectType: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('searchType')[name] = combo.value;
        },
         comboSelectStat: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('searchStat')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            //console.log(field);
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },
        searchTab: function(button, e)
        {
          var tabPanel = Ext.ComponentQuery.query('spofnumberstabstat')[0];
          var activeTab = tabPanel.getActiveTab();
          if (activeTab.xtype == 'chartNumbers')
           {
              this.searchStat();
           }
           else if(activeTab.xtype == 'dataStat')
           {
            this.searchStatGrid();
           }
           else if( activeTab.xtype == 'chartNumbersReal')
           {
              this.searchStatReal();
           }
           else if(activeTab.xtype == 'chartNumbersExpired')
            {
                 this.searchStatExpired();
            }
              else if(activeTab.xtype == 'chartNumbersVirtual')
            {
                this.searchStatVirtual();
            }

        },
         resetTab: function(button, e)
        {
          var combo=this.lookupReference('expired'),
              comboType=this.lookupReference('type'),
              tabPanel = Ext.ComponentQuery.query('spofnumberstabstat')[0]
              activeTab = tabPanel.getActiveTab();

          if (activeTab.xtype == 'chartNumbers')
           {
               button.up('form').reset();
               this.resetStat();
           }
           else if(activeTab.xtype == 'dataStat')
           {
              button.up('form').reset();
              this.resetStatGrid();
              combo.reset();
              comboType.reset();
              this.gridcomboSelect(combo);
               
           }
           else if(activeTab.xtype == 'chartNumbersReal')
           {
              button.up('form').reset();
              this.resetStatReal(); 
           }
           else if(activeTab.xtype == 'chartNumbersExpired')
            {
                  button.up('form').reset();
                  this.resetStatExpired();
            }
              else if(activeTab.xtype == 'chartNumbersVirtual')
            {
                  button.up('form').reset();
                  this.resetStatVirtual(); 
            }
        },


	    search: function(button, e)
		{
	    	var search = this.getViewModel().get('search'),
		    filter = [{property: 'number', value: search.number},
                      {property: 'provider', value: search.provider},
                      {property: 'type', value: search.type},
                      {property: 'quarantine', value: search.quarantine},
                      {property: 'condition', value: search.condition},
                      {property: 'brand', value: search.brand},
                      {property: 'reserved', value: search.reserved}];
		    this.filterStore(this.getStore('numbers'), filter);
		},

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    number: '',
                    provider: '',
                    type: '',
                    quarantine: Tcitel.controller.common.Common.almostMidnight(),
                    condition: '',
                    reserved: '',
                    brand:''
                }
            );
            button.up('fieldset').down('combo[name=type]').reset();
            button.up('fieldset').down('combo[name=condition]').reset();
            button.up('fieldset').down('combo[name=brand]').reset();
            this.search();
        },
        searchType: function(button, e)
        {
           
            var search = this.getViewModel().get('searchType'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('numbersType'), filter);
            //this.total();
        },
        searchStatVirtual: function(button, e)
        {
            var search = this.getViewModel().get('searchStat'),
                filter = [];
                search.expired = '';
                search.type = 'virtual';
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
         
            this.filterStore(this.getStore('virtual'), filter);
            
        },
        searchStatReal: function(button, e)
        {
            var search = this.getViewModel().get('searchStat'),
                filter = [];
                search.expired = '';
                search.type = 'real';
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
         
            this.filterStore(this.getStore('real'), filter);
            
        },
        searchStat: function(button, e)
        {
            var search = this.getViewModel().get('searchStat'),
                filter = [];
                search.expired = 'FALSE';
                search.type = '';
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
         
            this.filterStore(this.getStore('spofstat'), filter);
            
        },
         searchStatExpired: function(button, e)
        {
            var search = this.getViewModel().get('searchStat'),
                filter = [];
                search.expired = 'TRUE';
                search.type = '';       
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
          
            this.filterStore(this.getStore('spofstatExpired'), filter);
            
        },
        searchStatGrid: function(button, e)
        {
           var expired=this.lookupReference('expired').getValue(),
               type=this.lookupReference('type').getValue();
            var search = this.getViewModel().get('searchStat'),
            filter = [];
            search.expired = expired;
            search.type = type;
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('statGrid'), filter);
        },
       

       reset1: function(button, e)
        {
            this.getViewModel().set
            (
                'searchType',
                {
                    number: '',
                    user_id: '',
                    type: '',
                    expiration_date: Tcitel.controller.common.Common.almostMidnight(),
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end:Tcitel.controller.common.Common.almostMidnight(),
                    brand:''
                }
            );
            button.up('fieldset').down('combo[name=type]').reset();
            button.up('fieldset').down('combo[name=brand]').reset();
            this.searchType();
        },
        resetStat: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStat',
                {
                    startday: Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand:'',
                    expired:'FALSE',
                    type:''
                }
            );
          
            this.searchStat();

        },
        resetStatReal: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStat',
                {
                    startday:Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand:'',
                    expired:'',
                    type:'real'
                }
            );
          
            this.searchStatReal();

        },
         resetStatVirtual: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStat',
                {
                    startday: Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand:'',
                    expired:'',
                    type:'virtual'
                }
            );
          
            this.searchStatVirtual();

        },
         resetStatExpired: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStat',
                {
                    startday: Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand:'',
                    expired:'TRUE',
                    type:''
                }
            );
           
            this.searchStatExpired();

        },
         resetStatGrid: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStat',
                {
                    startday: Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brand:'',
                    expired:'',
                    type:''
                }
            );
           
            this.searchStatGrid();

        },
          gridcomboSelect: function(combo, record, eOpts)
        {
            var search = this.getViewModel().get('searchStat');
            var filter = 
            [
                {property: 'expired', value: combo.value},
                {property: 'startday', value:search.startday},
                 {property: 'endday', value:search.endday},
                 {property: 'brandStat', value:search.brandStat},
                {property: 'type', value: this.lookupReference('type').getValue()}//,
            ];
           this.filterStore(this.getStore('statGrid'), filter);
        },
         typecomboSelect: function(combo, record, eOpts)
        {
            var search = this.getViewModel().get('searchStat');
            var filter = 
            [
                {property: 'type', value: combo.value},
                {property: 'startday', value:search.startday},
                 {property: 'endday', value:search.endday},
                 {property: 'brandStat', value:search.brandStat},
                {property: 'expired', value: this.lookupReference('expired').getValue()}//,
               
            ];
           this.filterStore(this.getStore('statGrid'), filter);
        },
        activateTab: function(tab, eOpts)
        {
           // console.log(tab.xtype);
            if (tab.xtype == 'chartNumbers')
            {
                this.searchStat();
               
            }
            else if (tab.xtype == 'dataStat')
            {
                this.searchStatGrid();
            }
             else if (tab.xtype == 'chartNumbersReal')
            {

                this.searchStatReal();
                
            }
            else if(tab.xtype == 'chartNumbersExpired')
            {
                 this.searchStatExpired();
            }
              else if(tab.xtype == 'chartNumbersVirtual')
              {
                this.searchStatVirtual();
              }
        },


        send: function(button, e, eOpts)
        {
            var me = this,
            form = button.up('form').getForm();
            if(form.isValid())
            {
                form.submit
                (
                    {
                        url: 'php/spofnumbers/send.php',
                        waitMsg: 'Uploading & processing...',
                        success: function(fp, o)
                        {
                            Ext.Msg.alert('Success', 'File "' + o.result.file + '" uploaded.<br><strong>' + o.result.inserted + '</strong> records inserted, <strong>' + o.result.errors + '</strong> errors.');
                            form.isValid();
                            me.getStore('numbers').reload();
                            me.getStore('errors').reload();
                        },
                        failure: function(uploadDataForm, o)
                        {
                            Ext.Msg.alert('Failure', 'File "'+ o.result.file+'" upload ERROR.');
                            form.isValid();
                        }
                    }
                );
            }
        },
          change: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('searchStat')[field.name] = field.getRawValue();
        },

        clearOldData: function(button, e, eOpts)
        {
            var me = this;
            Ext.Msg.confirm
            (
                'Confirm', 'Clear the table?',
                function(button)
                {
                    if(button=='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: 'php/spofnumbers/cleardata.php',
                                success: function( response, options )
                                {
                                    var result = Ext.decode( response.responseText );
                                    if( result.success )
                                    {
                                        Ext.Msg.alert( 'Success', 'Data deleted!');
                                        me.getStore('errors').reload();
                                    }
                                    else
                                    {
                                        Ext.Msg.alert( 'Failure', 'Please try again.');
                                    }
                                },
                                failure: function( response, options )
                                {
                                    Ext.Msg.alert( 'Failure', 'Please try again.');
                                }
                            }
                        );
                    }
                }
            );
        }
	}
);
