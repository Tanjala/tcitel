Ext.define
(
	'Tcitel.view.spofnumbers.NumbersTypeGrid',
	{
    	extend: 'Ext.grid.Panel',
        xtype: 'numbersgrid',
        //title: 'Numbers Type',
        bind: {store:'{numbersType}'},
        columnLines: true,
        border: true,
        header:
        {
            hidden:'true'
        },
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{numbersType}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false
            },
            items:
            [
                {
                    text: 'Expiration Date',
                    dataIndex: 'expiration_date',
                    sortable: true
                },
                {
                    text: 'Number',
                    dataIndex: 'number',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'User ID',
                    dataIndex: 'user_id',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                 {
                    text: 'Created Date',
                    dataIndex: 'created',
                    sortable: true
                },
               
                {
                    text: 'Type',
                    dataIndex: 'type'
                },
                {
                    text: 'Brand',
                    brand_access: 'Virtual SIM',
                    dataIndex: 'brand'
                },
                {
                    text: 'Provider',
                    dataIndex: 'provider'
                },
                {
                    text: 'Price',
                    dataIndex: 'price_input'
                },
                {
                    text: 'Our Price',
                    dataIndex: 'price_output'
                },
                {
                    text: 'Auto Renewal',
                    dataIndex: 'auto_renew',
                    renderer: function (value)
                    {
                        return value == 't' ? 'TRUE' : 'FALSE';
                    }
                }/*,
                {
                    text: 'Special Offer Cities ID',
                    dataIndex: 'special_offer_cities_id'
                }*/
        	]
        }
	}
);
