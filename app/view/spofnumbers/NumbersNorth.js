
Ext.define
(
    'Tcitel.view.spofnumbers.NumbersNorth',
    {
        extend: 'Ext.form.Panel',
        xtype: 'numberssearch',
        header:
        {
            hidden:'true'
        },
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20
        },
        items:
        [
        {
            padding: 19,
            title: 'Number Type Search',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 90,
                        width:270
                    }
                },
                items:
                [
                {
                    margin: '0 8 0 0',
                    items:
                    [
                     {
                        name: 'user_id',
                        fieldLabel: 'User ID',
                         bind: '{searchType.user_id}'
                    },
                     {
                        name: 'number',
                        fieldLabel: 'Number',
                         bind: '{searchType.number}'
                    },
                    
                    {
                        xtype: 'combo',
                        name: 'type',
                        hiddenName: 'type',
                        fieldLabel: 'Number Type',
                        bind: {store:'{numtype}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        margin: '5 5 0 0',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelectType'
                        }
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'expiration_date',
                        margin: '5 5 0 0',
                        editable: false,
                        fieldLabel: 'Expire Date',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{searchType.expiration_date}'
                    }
                   
                 
                    ]
                },
                {  
                   //margin: '0 20 0 0',
                   //layout:'vbox',
                   items:
                   [
                    
                     {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        brand_access: 'Virtual SIM',
                        bind: {store:'{brandli}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        //margin: '5 5 0 0',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelectType'
                        }
                    },
                     {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start Created',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                         bind: '{searchType.log_start}'
                    },
                     {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End Created',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{searchType.log_end}'
                    },
                     
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '5 0 0 95',
                        width: 70,
                        handler: 'reset1'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '5 0 0 35',
                        width: 70,
                        handler: 'searchType'
                    }
                   
                   ]
                }/*,
                {
                    margin: '0 20 0 0',
                   layout:'vbox',
                   items:
                   [
                    {
                        xtype: 'button',
                        text: 'Messaging',
                        width: 100,
                        handler: 'messaging'
                    },
                     {
                        xtype: 'button',
                        text: 'Calls',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'voice'
                    },
                    {
                        xtype: 'button',
                        text: 'DIDWW',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'DIDWW'
                    }
                   
                   ]

                }*/
                ]
            }
            ]
        }/*,
        {
            padding:10,
            title: 'Total',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        readOnly:true,
                        labelWidth: 120

                    }
                },*/
             /*   items:
                [*/
               /* {
                    margin: '0 20 0 10',
                    
                    items:
                    [
                     {
                        name: 'GooglePlay',
                        fieldLabel: 'GooglePlay Total',
                        bind: '{total.GooglePlay}'
                    },
                    {
                        name: 'iTunes',
                        fieldLabel: 'iTunes Total',
                        bind: '{total.iTunes}'
                    },
                    {
                        name: 'PayPal',
                        fieldLabel: 'PayPal Total',
                        bind: '{total.PayPal}'
                    },
                    {
                        name: 'PayPalWeb',
                        fieldLabel: 'PayPalWeb Total',
                        bind: '{total.PayPalWeb}'
                    },
                     {
                        name: 'Bazaar',
                        fieldLabel: 'Bazaar Total',
                        bind: '{total.Bazaar}'
                    },
                     {
                            name: 'totalpayment',
                            margin: '15 20 0 0',
                            fieldLabel: 'TOTAL PAYMENT',
                            labelCls: 'biggertext',
                            bind: '{total.totalpayment}'
                        }
                    ]
                },*/
               /* {  
                   margin: '0 20 0 0',
                   layout:'vbox',

                   items:
                   [
                    
                    {
                        name: 'gui',
                        fieldLabel: 'Gui Total',
                        bind: '{total.gui}'
                    },
                    {
                        name: 'registration',
                        fieldLabel: 'Registration Total',
                        bind: '{total.registration}'
                    },
                    {
                        name: 'messaging',
                        fieldLabel: 'Messaging Total',
                        bind: '{total.messaging}'
                    },
                     {
                        name: 'voice',
                        fieldLabel: 'Calls Total',
                        bind: '{total.voice}'
                    },
                     {
                        name: 'DIDWW',
                        fieldLabel: 'DIDWW Total',
                        bind: '{total.DIDWW}'
                    }

                   ]
               }*/
              
               // ]
            /*}
            ] 
        }*/
        ]
    }
);
       