Ext.define
(
	'Tcitel.view.spofnumbers.NumbersGrid',
	{
    	extend: 'Ext.grid.Panel',
        xtype: 'spofnumbersgrid',
        title: 'Numbers',
        bind: {store:'{numbers}'},
        columnLines: true,
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{numbers}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false
            },
            items:
            [
                {
                    text: 'Number',
                    dataIndex: 'number',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Special Offer Regions ID',
                    dataIndex: 'special_offer_regions_id'
                },
                {
                    text: 'Reserved',
                    dataIndex: 'reserved'
                },
                {
                    text: 'Type',
                    dataIndex: 'type'
                },
                {
                    text: 'Brand',
                    brand_access: 'Virtual SIM',
                    dataIndex: 'brand'
                },
                {
                    text: 'Provider',
                    dataIndex: 'provider'
                },
                {
                    text: 'Price ID',
                    dataIndex: 'price_id'
                },
                {
                    text: 'Quarantine',
                    dataIndex: 'quarantine'
                },
                {
                    text: 'Special Offer Cities ID',
                    dataIndex: 'special_offer_cities_id'
                }
        	]
        }
	}
);
