Ext.define
(
    'Tcitel.view.spofnumbers.NumbersViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.spofnumbers',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.spofnumbers.Number',
            'Tcitel.model.spofnumbers.NumberType'
        ],
        data:
        {
            search:
            {
                number: '',
                provider: '',
                type: '',
                quarantine: Tcitel.controller.common.Common.almostMidnight(),
                condition: '',
                reserved: '',
                brand:''
            },

             searchType:
            {
                number: '',
                user_id: '',
                type: '',
                expiration_date: Tcitel.controller.common.Common.almostMidnight(),
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end:Tcitel.controller.common.Common.almostMidnight(),
                brand:''
            },
            searchStat:
            {
                startday: Tcitel.controller.common.Common.monthBeforeDate(),
                // Ext.Date.format(Ext.Date.getFirstDateOfMonth(new Date()), 'Y-m-d'),
                endday: Ext.Date.format(new Date(), 'Y-m-d'),
                brand:'',
                expired:'',
                type:''
            }
            
        },
        stores:
        {
            typeenumall:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_special_offer_numbers',
                        valueField: 'type',
                        displayField: 'type',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            brandli:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             numtype:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_numbers',
                        valueField: 'type',
                        displayField: 'type',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             brandone:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        
                         table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            typeenum:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        
                         table: 'vs_special_offer_numbers',
                        valueField: 'type',
                        displayField: 'type'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            numbers:
            {
                model: 'Tcitel.model.spofnumbers.Number',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/numbers.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
              numbersType:
            {
                model: 'Tcitel.model.spofnumbers.NumberType',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/numbersType.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
               statGrid:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/read_grid.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
             donut:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/donut_stat.php',
                    /*extraParams:
                    {
                        
                        expired: 'FALSE'
                    },*/
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
             spofstat:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/read_numbers_chart.php',
                    extraParams:
                    {
                        
                        expired: 'FALSE'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
              real:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/read_real_chart.php',
                    extraParams:
                    {
                        
                        type: 'real'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
              virtual:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/read_virtual_chart.php',
                    extraParams:
                    {
                        
                        type: 'virtual'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
             spofstatExpired:
            {
                model: 'Tcitel.model.spofstat.Daily',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/read_numbers_chart.php',
                    extraParams:
                    {
                        
                        expired: 'TRUE'
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
            errors:
            {
                model: 'Tcitel.model.spofnumbers.Number',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/spofnumbers/errors.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
            conditionstore:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'YES', value:'>'},
                    {display: 'NO', value:'<'}
                ]
            },
              expiredGrid:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'EXPIRED', value:'t'},
                    {display: 'ACTIVE', value:'f'}
                ]
            }
        }
    }
);
