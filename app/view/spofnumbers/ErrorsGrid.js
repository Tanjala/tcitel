Ext.define
(
	'Tcitel.view.spofnumbers.ErrorsGrid',
	{
    	extend: 'Ext.grid.Panel',
        xtype: 'spoferrorsgrid',
        title: 'Errors',
        bind: {store:'{errors}'},
        columnLines: true,
        border: true,
        dockedItems:
        [
            {
                xtype: 'toolbar',
                dock: 'top',
                items:
                [
                    {
                        xtype: 'button',
                        text: 'Clear Old Data',
                        handler: 'clearOldData'
                    }/*,
                    {
                        xtype: 'button',
                        text: 'Export Data'
                    }*/
                ]
            },
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{errors}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false
            },
            items:
        	[
                {
                    text: 'Number',
                    dataIndex: 'number',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Special Offer Regions ID',
                    dataIndex: 'special_offer_regions_id'
                },
                {
                    text: 'Reserved',
                    dataIndex: 'reserved'
                },
                {
                    text: 'Type',
                    dataIndex: 'type'
                },
                  {
                    text: 'Brand',
                    dataIndex: 'brand'
                },
                {
                    text: 'Provider',
                    dataIndex: 'provider'
                },
                {
                    text: 'Price ID',
                    dataIndex: 'price_id'
                },
                {
                    text: 'Quarantine',
                    dataIndex: 'quarantine'
                },
                {
                    text: 'Special Offer Cities ID',
                    dataIndex: 'special_offer_cities_id'
                }
        	]
        }
	}
);
