Ext.define
(
    'Tcitel.view.spofnumbers.NumbersTab',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'spofnumberstab',
        requires:
        [
            'Tcitel.view.spofnumbers.NumbersGrid',
            'Tcitel.view.spofnumbers.ErrorsGrid'
        ],
    	items:
        [
            {
                xtype: 'spofnumbersgrid'
            },
            {
                xtype: 'spoferrorsgrid',
                brand_access: 'Virtual SIM'
            }
        ]
    }
);
