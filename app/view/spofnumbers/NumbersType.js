Ext.define
(
    'Tcitel.view.spofnumbers.NumbersType',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'numbers',
        title: 'Numbers',
        itemId: 'numbers',
        controller: 'spofnumbers',
      
        layout: 'border',
        header:
        {
            style: 'background-color: lightslategrey;',
            defaults:
            {
                xtype: 'label'
            },
            items:
            [
                {

                    text: 'SMS Numbers',
                    style:
                    {
                        color: 'white'
                    }
                }
            ]
        },
        viewModel:
        {
            type: 'spofnumbers'
        },
        items:
        [
            {
                xtype: 'numberssearch',
                region: 'north'
            },
            {
                xtype: 'numbersgrid',
                region: 'center'
            }
        ]
    }
);
