Ext.define
(
    'Tcitel.view.spofnumbers.Numbers',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'spofnumbers',
        title: 'Special Offer Numbers',
        itemId: 'spofnumbers',
        controller: 'spofnumbers',
      
        layout: 'border',
       
        viewModel:
        {
            type: 'spofnumbers'
        },
        items:
        [
            {
                xtype: 'spofnumberssearch',
                region: 'north'
            },
            {
                xtype: 'spofnumberstab',
                region: 'center'
            }
        ]
    }
);
