Ext.define
(
    'Tcitel.view.spofnumbers.NumbersSearch',
    {
        extend: 'Ext.form.Panel',
        xtype: 'spofnumberssearch',
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: '20 0 20 20',
            padding: '11 20 15 20'
        },
        items:
        [
            {
                title: 'Numbers Filter',
                items:
                [
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults:
                        {
                            border: false,
                            defaults:
                            {
                                xtype: 'textfield',
                                labelWidth: 60,
                                enableKeyEvents:true,
                                listeners:
                                        {
                                            specialKey: 'specialKey'
                                        }
                            }
                        },
                        items:
                        [
                            {
                                margin: '9 20 0 0',
                                items:
                                [
                                    {
                                        name: 'number',
                                        maskRe: /[0-9]/,
                                        fieldLabel: 'Number',
                                        bind: '{search.number}'
                                    },
                                    {
                                        name: 'provider',
                                        maskRe: /[0-9]/,
                                        fieldLabel: 'Provider',
                                        bind: '{search.provider}'
                                    },
                                    {
                                        xtype: 'combo',
                                        name: 'type',
                                        hiddenName: 'type',
                                        fieldLabel: 'Type',
                                        bind: {store:'{typeenumall}'},
                                        displayField: 'display',
                                        valueField: 'value',
                                        queryMode: 'local',
                                        forceSelection: true,
                                        editable: false,
                                        value: '',
                                        listeners:
                                        {
                                            select: 'comboSelect'
                                        }
                                    },
                                     {
                                        xtype: 'combo',
                                        name: 'brand',
                                        hiddenName: 'brand',
                                        fieldLabel: 'Brand',
                                        bind: {store:'{brandli}'},
                                        displayField: 'display',
                                        valueField: 'value',
                                        brand_access: 'Virtual SIM',
                                        queryMode: 'local',
                                        forceSelection: true,
                                        editable: false,
                                        value: '',
                                        listeners:
                                        {
                                            select: 'comboSelect'
                                        }
                                    },
                                     {
                                        name: 'reserved',
                                        maskRe: /[0-9]/,
                                        fieldLabel: 'Reserved',
                                        bind: '{search.reserved}'
                                    }
                                ]
                            },
                            {
                                items:
                                [
                                    {
                                        xtype: 'fieldset',
                                        padding: '7 15 7 15',
                                        title: 'Quarantine',
                                        defaults:
                                        {
                                            xtype: 'textfield',
                                            labelWidth: 65,
                                            listeners:
                                            {
                                                specialKey: 'specialKey'
                                            }
                                        },
                                        items:
                                        [
                                            {
                                                xtype: 'datetimefield',
                                                name: 'quarantine',
                                                editable: false,
                                                fieldLabel: 'Time',
                                                format: 'Y-m-d',
                                                value: Tcitel.controller.common.Common.almostMidnight(),
                                                bind: '{search.quarantine}'
                                            },
                                            {
                                                xtype: 'combo',
                                                name: 'condition',
                                                hiddenName: 'condition',
                                                fieldLabel: 'Condition',
                                                bind: {store:'{conditionstore}'},
                                                displayField: 'display',
                                                valueField: 'value',
                                                queryMode: 'local',
                                                forceSelection: true,
                                                editable: false,
                                                value: '',
                                                listeners:
                                                {
                                                    select: 'comboSelect'
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Reset',
                                        margin: '10 135 0 0',
                                        width: 70,
                                        handler: 'reset'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Search',
                                        margin: '10 0 0 0',
                                        width: 70,
                                        handler: 'search'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                title: 'Numbers Upload',
                brand_access: 'Virtual SIM',
                padding: '18 20 15 20',
                items:
                [
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults:
                        {
                            border: false,
                            defaults:
                            {
                                xtype: 'textfield',
                                width: 350,
                                allowBlank: false,
                                labelWidth: 140
                            }
                        },
                        items:
                        [
                            {
                                margin: '0 20 0 0',
                                items:
                                [
                                    {
                                        xtype: 'restfileupload',
                                        accept: ['csv', 'txt'],
                                        emptyText: 'extensions: csv, txt',
                                        name: 'number',
                                        fieldLabel: 'Numbers',
                                        allowBlank: false,
                                        buttonText: 'Select File'
                                    },
                                    {
                                        fieldLabel: 'Spec Offer Regions ID',
                                        name: 'special_offer_regions_id',
                                        maskRe: /[0-9]/
                                    },
                                    {
                                        fieldLabel: 'Reserved',
                                        name: 'reserved',
                                        maskRe: /[0-9]/
                                    },
                                    {
                                        xtype: 'combo',
                                        name: 'type',
                                        hiddenName: 'type',
                                        fieldLabel: 'Type',
                                        bind: {store:'{typeenum}'},
                                        displayField: 'display',
                                        valueField: 'value',
                                        queryMode: 'local',
                                        forceSelection: true,
                                        editable: false,
                                        value: 'trial'
                                    },
                                     {
                                          xtype: 'datetimefield',
                                          name: 'quarantine',
                                          editable: false,
                                          fieldLabel: 'Quarantine',
                                          format: 'Y-m-d',
                                          value: Tcitel.controller.common.Common.almostMidnight()//,
                                          //bind: '{search.quarantine}'
                                    }
                                ]
                            },
                            {
                                defaults:
                                {
                                    xtype: 'textfield',
                                    width: 350,
                                    allowBlank: false,
                                    labelWidth: 130
                                },
                                items:
                                [
                                      {
                                        xtype: 'combo',
                                        name: 'brand',
                                        hiddenName: 'brand',
                                        fieldLabel: 'Brand',
                                        bind: {store:'{brandone}'},
                                        brand_access: 'Virtual SIM',
                                        displayField: 'display',
                                        valueField: 'value',
                                        queryMode: 'local',
                                        forceSelection: true,
                                        editable: false,
                                        value: 'Virtual SIM'
                                    },
                                    {
                                        fieldLabel: 'Provider',
                                        name: 'provider',
                                        maskRe: /[0-9]/
                                    },
                                    {
                                        fieldLabel: 'Price ID',
                                        name: 'price_id',
                                        maskRe: /[0-9]/
                                    },
                                    {
                                        fieldLabel: 'Spec Offer Cities ID',
                                        name: 'special_offer_cities_id',
                                        maskRe: /[0-9]/
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Send',
                                        width: 80,
                                        margin: '6 0 0 205',
                                        formBind: true,
                                        handler: 'send'
                                    }
                                ]
                            }
                        ]
                    }
               ]
            }
        ]
    }
);
