Ext.define
(
    'Tcitel.view.spofnumbers.MainNumbers',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'spofnumbersmain',
        title: 'Numbers',
        itemId: 'spofnumbersmain',
        controller: 'spofnumbers',
        
        viewModel:
        {
            type: 'spofnumbers'
        },
        requires:
        [
            'Tcitel.view.spofnumbers.NumbersController',
            'Tcitel.view.spofnumbers.NumbersSearch',
            'Tcitel.view.spofnumbers.NumbersTab',
            'Tcitel.view.spofnumbers.NumbersViewModel',
            'Tcitel.view.spofnumbers.NumbersType',
            'Tcitel.view.spofnumbers.NumbersNorth',
            'Tcitel.view.spofnumbers.NumbersTypeGrid',
            'Tcitel.view.spofnumbersstat.Stat'
        ],

        items:
        [
            
            {
                xtype: 'numbers',
                brand_access: 'Virtual SIM'
            },
            
            {
                xtype: 'stat_numbers',
                brand_access: 'Virtual SIM'
            },
            {
                xtype: 'spofnumbers'
            }
        ]
    }
);