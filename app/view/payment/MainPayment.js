Ext.define
(
    'Tcitel.view.payment.MainPayment',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'paymentmain',
        title: 'Payment',
        itemId: 'paymentmain',
        controller: 'payment',
        
        viewModel:
        {
            type: 'payment'
        },
        requires:
        [
            'Tcitel.view.payment.PaymentCenter',
            'Tcitel.view.payment.Payment',
            'Tcitel.view.payment.PaymentController',
            'Tcitel.view.payment.PaymentViewModel',
            'Tcitel.view.payment.TransactionDetail',
            'Tcitel.view.payment.PaymentNorth',
            'Tcitel.view.payment.Order',
            'Tcitel.view.payment.OrderNorth',
            'Tcitel.view.paymentstat.Stat'
        ],

        items:
        [
            {
                xtype: 'payment'
            },
             {
                xtype: 'stat_payment',
                brand_access: 'Virtual SIM'
            },
            ,
            {
                xtype: 'order',
                brand_access: 'Virtual SIM'
            }
        ]
    }
);