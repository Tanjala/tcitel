
Ext.define
(
    'Tcitel.view.payment.OrderNorth',
    {
        extend: 'Ext.form.Panel',
        xtype: 'ordernorth',
        header:
        {
            hidden:'true'
        },
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20
        },
        items:
        [
        {
            padding: 19,
            title: 'Transaction Payment Search',
            itemId: 'orderform',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 105,
                        width:270
                    }
                },
                items:
                [
                {
                    margin: '0 8 0 0',
                    items:
                    [
                     {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                        bind: '{searchorders.log_start}'
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End',
                        format: 'Y-m-d',
                        margin: '5 5 0 0',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{searchorders.log_end}'
                    },
                     {
                        name: 'user_id',
                        fieldLabel: 'User ID',
                        margin: '5 5 0 0',
                        bind: '{searchorders.user_id}'
                    },
                     
                     
                  
                     {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '15 0 0 110',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '15 0 0 19',
                        width: 70,
                       handler: 'searchOrders'
                    }
                    ]
                },
                {  
                   margin: '0 20 0 10',
                   layout:'vbox',
                   items:
                   [
                   {
                        xtype: 'combo',
                        name: 'payment_status',
                        hiddenName: 'payment_status',
                        reference: 'payment_state',
                        fieldLabel: 'Payment Status',
                        width: 270,
                        bind: {store:'{payment_status}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        //margin: '5 0 0 0',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'combo',
                        name: 'payment_type',
                        hiddenName: 'payment_type',
                        fieldLabel: 'Payment Type',
                        reference: 'payment_tip',
                        bind: {store:'{payment_type}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'//,
                           // change: 'paymentDataSearch'
                        }
                         
                    },
                     {
                        name: 'ordertype',
                        fieldLabel: 'Payment Data',
                        reference: 'ordertype',
                         //disabled:true,
                        bind: '{searchorders.ordertype}'
                       
                    },
                     {
                        name: 'account_id',
                        fieldLabel: 'Billing ID',
                        reference: 'account_id',
                         //disabled:true,
                        bind: '{searchorders.account_id}'
                       
                    }
                   
                   ]
                }
                ]
            }
            ]
        }
      
        ]
    }
);
       