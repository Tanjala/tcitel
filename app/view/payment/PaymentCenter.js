
Ext.define
(
	'Tcitel.view.payment.PaymentCenter',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'paymentcenter',
		bind: {store:'{payment}'},
		 requires: 
		 [
		        'Tcitel.model.billing.Payment'

		 ],
		dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{payment}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
	           
		columns:
		{
            defaults:
            {
            	editor:
            	{
            		xtype:'textfield',
            		readOnly:true
            	},
                flex: 1
            },
			items:
			[
			    {
			        text: 'ID',
			        dataIndex: 'id',
			        flex: false,
			        width: 90
			    },
			     {
                    text: 'Transaction',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',

                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 60
                        },
                        items:
                        [

                            {

                                html:'details',
                                margin: '0 5 5 5',
                                handler: 'transactionDetail'
                            }
                        ]
                    }
                },
			    {
			        text: 'Billing ID',
			        dataIndex: 'account_id',
			        flex: false,
			        width: 90
			    },
			    {
                    text: 'Customer',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 60
                        },
                        items:
                        [

                            {

                                html:'details',
                                 margin: '0 5 5 5',
                                handler: 'details'
                            }
                        ]
                    }
                },
			    {
			        text: 'Meta Data',
			        dataIndex: 'meta_data',
			        editor:
	            	{
	            		xtype:'textarea',
	            		readOnly:true
	            	}
			    },
			     {
                    text: 'Payment',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    brand_access: 'Virtual SIM',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 60
                        },
                        items:
                        [

                            {

                                html:'details',
                                 margin: '0 5 5 5',
                                handler: 'paymentdetails'
                            }
                        ]
                    }
                },
			    {
			        text: 'Created',
			        dataIndex: 'created_at'
			      
			    },
			    {
			        text: 'Expires',
			        dataIndex: 'expires_at'
			    },
			    {
			        text: 'Payment Type',
			        dataIndex: 'name'
			    },
			   /* {
			        text: 'Billing Profile ID',
			        dataIndex: 'billing_profile_id'
			    },*/
			    {
			        text: 'Committed',
			        dataIndex: 'committed',
			        flex: false,
                    width: 100
			    },
			    {
			        text: 'Brand',
			        dataIndex: 'brand',
			         brand_access: 'Virtual SIM',
			         flex: false,
                     width: 100
			    }/*,
			    {
			        text: 'Multiply',
			        dataIndex: 'multiply'
			    },
			    {
			        text: 'Result',
			        dataIndex: 'result'
			    }*/
			]
		}
	}
);