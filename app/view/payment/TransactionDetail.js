Ext.define
(
    'Tcitel.view.payment.TransactionDetail',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'transactionGrid',
         alias: 'widget.transactionGrid',
        title: 'Transaction Detail',
        floating: true,
        bind: {store:'{transactiondetail}'},
       // store:'transactiondetail',
        draggable: true,
        closable: true,
        modal: true,
        autoShow: false,
        frame: true,
        
        height: 300,
        viewConfig:
        {
            enableTextSelection: true
        },
         plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },

        
        initComponent: function()
        {
             var me = this;

            me.width = 1100;
            me.columns = [

            {
                text     : 'ID',
                flex     : 1,
                dataIndex: 't_id'
            },
            {
                text     : 'Detail ID',
                flex     : 1,
                //sortable : false//,
                dataIndex: 'detail_id'
            },
            {
                text: 'Billing ID',
                dataIndex: 'account_id',
                 flex     : 1
               // sortable : true,
            },
            {
                text: 'Meta Data',
                dataIndex: 'meta_data',
                editor:
                {
                    xtype:'textarea',
                    readOnly:true
                },
                 flex     : 1,
               // width    : 80,
                sortable : true//,
               
            },
            {
                
                text: 'Created',
                dataIndex: 'created_at',
                 flex     : 1,
                 sortable : true

            },
            {
                text: 'Expires',
                dataIndex: 'expires_at',
                flex     : 1,
                sortable : true
            },
            {

                text: 'Committed',
                dataIndex: 'committed',
                flex     : 1
                
            },
             {
                text: 'Billing Type ID',
                dataIndex: 'billing_profile_id',
                 flex     : 1
            },
          
             {
                    text: 'Brand',
                    dataIndex: 'brand',
                    flex     : 1,
                    brand_access: 'Virtual SIM'
            },
             {
                    text: 'Result',
                    dataIndex: 'result',
                    width: 60
            }
            
        ];

        me.callParent();
        }
    }
);
