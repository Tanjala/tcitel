Ext.define
(
    'Tcitel.view.payment.PaymentViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.payment',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.billing.Payment',
            'Tcitel.model.orders.Orders',
            'Tcitel.model.paymentstat.PaymentStat'
          
        ],
        data:
        {
           
            total:{},

         
            searchpay:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                type: '',
                brand:''
            },
            searchorders:
            {
               user_id:'',
               log_start: Tcitel.controller.common.Common.midnight(),
               log_end: Tcitel.controller.common.Common.almostMidnight(),
               payment_status:'',
               payment_type:'',
               ordertype:'',
               account_id:''
            },
            total1:
            {
                GooglePlay:'',
                iTunes:'',
                PayPal:'',
                PayPalWeb:'',
                Bazaar:'',
                gui:'',
                totalpayment:''
            },
            searchStatPayment:
            {
                startday: Tcitel.controller.common.Common.monthBeforeDate(),
                // Ext.Date.format(Ext.Date.getFirstDateOfMonth(new Date()), 'Y-m-d'),
                endday: Ext.Date.format(new Date(), 'Y-m-d'),
                brandStat:''
                
            }
        },
        stores:
        {
            
    
            payment:
            {
                model: 'Tcitel.model.billing.Payment',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/payment.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad:true
            },

            orders:
            {
                model: 'Tcitel.model.orders.Orders',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/orders.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad:true
            },
            total:
            {
                model: 'Tcitel.model.billing.Total',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/total.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                listeners:
                {
                    load: 'paymentLoad'
                },
                remoteFilter: true,
                autoLoad: true
            },

            transactiondetail:
              {
                model: 'Tcitel.model.billing.Transaction',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/billing/transactiondetail.php',
                   
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                /*listeners:
                {
                    load: 'paymentLoad'
                },*/
                remoteFilter: true,
                autoLoad: false
            },
             paymentstat:
            {
                model: 'Tcitel.model.paymentstat.PaymentStat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/paymentstat/read_payment_chart.php',
                   /* extraParams:
                    {
                        
                        expired: 'FALSE'
                    },*/
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
            paymentstat_daily:
            {
                model: 'Tcitel.model.paymentstat.PaymentStat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/paymentstat/read_payment_chart_daily.php',
                   /* extraParams:
                    {
                        
                        expired: 'FALSE'
                    },*/
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },

             payment_type:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_orders',
                        valueField: 'payment_type',
                        displayField: 'payment_type',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             type:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'GooglePlay', value:'6'},
                    {display: 'iTunes', value:'7'},
                    {display: 'PayPal', value:'8'},
                    {display: 'PayPalWeb', value:'9'},
                    {display: 'Bazaar', value:'19'},
                    {display: 'TopUp', value:'23'}
                ]
            },
          
             brandPay:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
              payment_status:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_orders',
                        valueField: 'payment_status',
                        displayField: 'payment_status',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
         
    }
);