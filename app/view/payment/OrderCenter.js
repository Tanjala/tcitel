
Ext.define
(
	'Tcitel.view.payment.OrderCenter',
	{
		extend: 'Ext.grid.Panel',
		xtype: 'ordercenter',
		bind: {store:'{orders}'},
		 requires: 
		 [
		       'Tcitel.model.orders.Orders'

		 ],
		dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{orders}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
	           
		columns:
		{
            defaults:
            {
            	editor:
            	{
            		xtype:'textfield',
            		readOnly:true
            	},
                flex: 1
            },
			items:
			[
			    {
			        text: 'User ID',
			        dataIndex: 'user_id',
			        flex: false,
			        width: 90
			    },
			     {
                    text: 'User ',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',

                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 60
                        },
                        items:
                        [

                            {

                                html:'details',
                                margin: '0 5 5 5',
                                handler: 'detailsUser'
                            }
                        ]
                    }
                },
			    {
			        text: 'Payment Type',
			        dataIndex: 'payment_type'//,
			       /* flex: false,
			        width: 90*/
			    },
			   
			    {
			        text: 'Payment Data',
			        dataIndex: 'payment_data',
			        editor:
	            	{
	            		xtype:'textarea',
	            		readOnly:true
	            	}
			    },
			    {
			        text: 'Transaction Started',
			        dataIndex: 'transaction_started'
			      
			    },
			    {
			        text: 'Transaction Update',
			        dataIndex: 'transaction_update'
			    },
			    {
			        text: 'Payment Status',
			        dataIndex: 'payment_status'
			    }
			  
			]
		}
	}
);