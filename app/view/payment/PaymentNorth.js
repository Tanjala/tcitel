
Ext.define
(
    'Tcitel.view.payment.PaymentNorth',
    {
        extend: 'Ext.form.Panel',
        xtype: 'paymentnorth',
        header:
        {
            hidden:'true'
        },
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20
        },
        items:
        [
        {
            padding: 19,
            title: 'Transaction Type Search',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 90,
                        width:270
                    }
                },
                items:
                [
                {
                    margin: '0 8 0 0',
                    items:
                    [
                     {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                        bind: '{searchpay.log_start}'
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End',
                        format: 'Y-m-d',
                        margin: '5 5 0 0',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{searchpay.log_end}'
                    },
                    {
                        xtype: 'combo',
                        name: 'type',
                        hiddenName: 'type',
                        fieldLabel: 'Payment Type',
                        bind: {store:'{type}'},
                        displayField: 'display',
                        valueField: 'value',
                        margin: '5 5 0 0',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                         brand_access: 'Virtual SIM',
                        bind: {store:'{brandPay}'},
                        displayField: 'display',
                        valueField: 'value',
                        margin: '5 5 0 0',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelectPay'
                        }
                    },
                     {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '15 18 0 95',
                        width: 70,
                        handler: 'reset1'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '15 0 0 19',
                        width: 70,
                       handler: 'nestosearch'
                    }
                    ]
                },
                {  
                   margin: '0 20 0 0',
                   layout:'vbox',
                   items:
                   [
                   {
                        xtype: 'button',
                        text: 'Tranfer Credit',
                        width: 100,
                        handler: 'transfer'
                    },
                    {
                        xtype: 'button',
                        text: 'Gui Payment',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'gui'
                    },
                    {
                        xtype: 'button',
                        text: 'Registration',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'registration'
                    }
                   ]
                },
                {
                    margin: '0 20 0 0',
                   layout:'vbox',
                   items:
                   [
                    {
                        xtype: 'button',
                        text: 'Messaging',
                        width: 100,
                        handler: 'messaging'
                    },
                     {
                        xtype: 'button',
                        text: 'Calls',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'voice'
                    },
                    {
                        xtype: 'button',
                        text: 'DIDWW',
                        margin: '10 0 0 0',
                        width: 100,
                        handler: 'DIDWW'
                    }
                   
                   ]

                }
                ]
            }
            ]
        },
        {
            padding:10,
            title: 'Total',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        readOnly:true,
                        labelWidth: 120

                    }
                },
                items:
                [
                {
                    margin: '0 20 0 10',
                    
                    items:
                    [
                     {
                        name: 'GooglePlay',
                        fieldLabel: 'GooglePlay Total',
                        bind: '{total.GooglePlay}'
                    },
                    {
                        name: 'iTunes',
                        fieldLabel: 'iTunes Total',
                        bind: '{total.iTunes}'
                    },
                    {
                        name: 'PayPal',
                        fieldLabel: 'PayPal Total',
                        bind: '{total.PayPal}'
                    },
                    {
                        name: 'PayPalWeb',
                        fieldLabel: 'PayPalWeb Total',
                        bind: '{total.PayPalWeb}'
                    },
                     {
                        name: 'Bazaar',
                        fieldLabel: 'Bazaar Total',
                        bind: '{total.Bazaar}'
                    },
                     {
                            name: 'totalpayment',
                            margin: '15 20 0 0',
                            fieldLabel: 'TOTAL PAYMENT',
                            labelCls: 'biggertext',
                            bind: '{total.totalpayment}'
                        }
                    ]
                },
                {  
                   margin: '0 20 0 0',
                   layout:'vbox',

                   items:
                   [
                    
                    {
                        name: 'gui',
                        fieldLabel: 'Gui Total',
                        bind: '{total.gui}'
                    },
                    {
                        name: 'registration',
                        fieldLabel: 'Registration Total',
                        bind: '{total.registration}'
                    },
                    {
                        name: 'messaging',
                        fieldLabel: 'Messaging Total',
                        bind: '{total.messaging}'
                    },
                     {
                        name: 'voice',
                        fieldLabel: 'Calls Total',
                        bind: '{total.voice}'
                    },
                     {
                        name: 'DIDWW',
                        fieldLabel: 'DIDWW Total',
                        bind: '{total.DIDWW}'
                    }

                   ]
               }
              
                ]
            }
            ] 
        }
        ]
    }
);
       