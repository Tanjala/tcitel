Ext.define
(
    'Tcitel.view.payment.PaymentController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.payment',

         comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('searchorders')[name] = combo.value;
            //console.log(combo.value);
        },
         comboSelectStat: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('searchStatPayment')[name] = combo.value;
            //console.log(combo.value);
        },


        comboSelectPay: function(combo, record, eOpts)
        {
            this.getViewModel().get('searchpay')[combo.name] = combo.value;
        },
        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },
        searchOrders: function(button, e)
        {
           
            var search = this.getViewModel().get('searchorders'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('orders'), filter);
            this.total();
        },
    
        search: function(button, e)
        {
           
            var search = this.getViewModel().get('searchpay'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('payment'), filter);
            this.total();
        },
    
        specialKey: function(field, e)
        {
            // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
            // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
            if (e.getKey() == e.ENTER)
            {
                this.seach();
            }
        },
           resetOrder: function()
        {
            //this.getViewModel().set('searchorders', {});
            this.getViewModel().set
            (
                'searchorders',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    payment_status:'',
                    payment_type:'',
                    payment_data:'',
                    ordertype:'',
                    user_id: ''//,
                    //status: '',
                    //product: ''
                }
            );
            
          
            this.clearStores();

        },
          clearStores: function(store, operation, eOpts)
        {
            this.getStore('orders').loadData([],false);
          
        },
        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'searchorders',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    payment_status:'',
                    payment_type:'',
                    payment_data:'',
                    ordertype:'',
                    user_id: ''//,
                    //status: '',
                    //product: ''
                }
            );
            button.up('form').getForm().reset();
            this.searchOrders();
        },
    
        reset1: function(button)
        {
            //console.log(button.up('form').items);
            button.up('form').down('datefield').reset();
            button.up('form').down('combo').reset();
            button.up('form').down('combo[name=brand]').reset();
            this.getViewModel().set('searchpay', 
            {  
                   type: '',
                   brand:'',
                   log_start: Tcitel.controller.common.Common.midnight(),
                   log_end: Tcitel.controller.common.Common.almostMidnight()//,
                  // created_at: Ext.Date.format(new Date(), 'Y-m-d')
            });
            this.search();
        },
         groupSelect: function(combo, record, eOpts)
        {
            var filter = [{property: 'name', value: combo.value}];
            //console.log(filter);
            this.filterStore(this.getStore(combo.name), filter);
        },

        paymentDataSearch: function(combo, newValue, oldValue, eOpts)
        
        {
              combo= this.getViewModel().get('searchorders')[name] = combo.value;
              var field=this.lookupReference('ordertype');
           // console.log(field);
            if (newValue == '' )
                {
                    field.setValue('');
                    field.disable();
                }
            else
                {
                    if (combo!='') 
                    {
                        field.enable();
                        //combo.setValue('Virtual SIM');
                    }
                }
        },
    
        paymentLoad: function(store, records, successful, eOpts)
        {
             if(successful && records.length)
             {
                 var payment = store.first();
             //debugger;
                 this.getViewModel().set('total', payment);
                
                 //this.getStore('orderlog').load({params:{'ids[]': customer.data.ids}});
             }
        },
      
        change: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('searchpay')['created_at'] = field.getRawValue();
           
        },
        transfer: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 10;
            this.search();
        },
        gui: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 4;
            this.search();
        },
        registration: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 11;
            this.search();
        },
       messaging: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 12;
            this.search();
        },
        voice: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 3;
            this.search();
        },
         DIDWW: function (button, e)
        {
            this.getViewModel().get('searchpay')['type'] = 14;
            this.search();
        },
        total: function(button, e)
        {
               
          var filter = 
          [ {property: 'log_start', value: this.getViewModel().get('searchpay').log_start},
            {property: 'log_end', value: this.getViewModel().get('searchpay').log_end},
            {property: 'brand', value: this.getViewModel().get('searchpay').brand}
          ];
            this.filterStore(this.getStore('total'), filter);
          
        },
        details: function(button, e)
        {
           var record = button.up('container').getWidgetRecord(),
               billing_id=record.data.account_id,
               searchbilling_id = Ext.ComponentQuery.query('billingnorth textfield[name=billing_id]')[0];

               //console.log(Ext.ComponentQuery.query('billingnorth button')[0]);

                var billingView = Ext.ComponentQuery.query('billing')[0];

               var resetBtn = Ext.ComponentQuery.query('billingnorth button')[0];
               var searchBtn = Ext.ComponentQuery.query('billingnorth button')[1];

              Ext.ComponentQuery.query('pages')[0].setActiveItem(0);
              
               billingView.getController().reset(resetBtn);

               billingView.getController().getViewModel().get('search')['billing_id'] = billing_id;
              searchbilling_id.setValue(billing_id);
               
               billingView.getController().seach();


         
          
        },
        paymentdetails: function(button, e)
        {
           
           var record = button.up('container').getWidgetRecord(),
               account_id=record.data.account_id;
               log_start=record.data.created_at,
               combo=this.lookupReference('payment_state');
                comboType=this.lookupReference('payment_tip');
               //console.log (record);
               //console.log(log_start);

            //console.log(combo);
            this.resetOrder();
            combo.reset(); 
            comboType.reset();     
            this.getViewModel().get('searchorders')['account_id'] = account_id;
            this.getViewModel().get('searchorders')['log_start'] = log_start;
            this.searchOrders();    
            button.up('paymentmain').setActiveItem(2);
          
          
        },
          detailsUser: function(button, e)
        {
           var record = button.up('container').getWidgetRecord(),
               user_id=record.data.user_id,
               searchuser_id = Ext.ComponentQuery.query('billingnorth textfield[name=user_id]')[0];

               //console.log(Ext.ComponentQuery.query('billingnorth button')[0]);

                var billingView = Ext.ComponentQuery.query('billing')[0];

               var resetBtn = Ext.ComponentQuery.query('billingnorth button')[0];
               var searchBtn = Ext.ComponentQuery.query('billingnorth button')[1];

               Ext.ComponentQuery.query('pages')[0].setActiveItem(0);
               billingView.getController().reset(resetBtn);

               billingView.getController().getViewModel().get('search')['user_id'] = user_id;
              searchuser_id.setValue(user_id);
               
               billingView.getController().seach();
          
        },

        transactionDetail: function(button)
        {
            var record = button.up('container').getWidgetRecord();
            var transactionGrid = Ext.widget('transactionGrid');
            this.getView().add(transactionGrid);
            this.getStore('transactiondetail').load({params: {id:record.id}});
            transactionGrid.show();
        },
      
        nestosearch: function (button, e)
        {
           var form=button.up('form'),
           values=form.getValues(),
           type=values['type'];
           this.getViewModel().get('searchpay')['type'] = type;
           this.search();
        },

         changeDate: function(field, newValue, oldValue, eOpts)
        {
            this.getViewModel().get('searchStatPayment')[field.name] = field.getRawValue();
        },

        searchTab: function(button, e)
        {
          var tabPanel = Ext.ComponentQuery.query('paymentstat_tab')[0];
          var activeTab = tabPanel.getActiveTab();
          //console.log(activeTab);
          if (activeTab.xtype == 'chartPayment')
           {
               this.searchStatHourly();
           }
           else if(activeTab.xtype == 'chartPaymentDaily')
           {
               this.searchStatDaily();
           }
           

        },
         resetTab: function(button, e)
        {
          var //combo=this.lookupReference('expired'),
             // comboType=this.lookupReference('type'),
              tabPanel = Ext.ComponentQuery.query('paymentstat_tab')[0]
              activeTab = tabPanel.getActiveTab();

          if (activeTab.xtype == 'chartPayment')
           {
               button.up('form').reset();
               this.resetStatHourly();
           }
           else if(activeTab.xtype == 'chartPaymentDaily')
           {
              button.up('form').reset();
              this.resetStatDaily();
              combo.reset();
             // comboType.reset();
              //this.gridcomboSelect(combo);
               
           }
          
        },
          resetStatHourly: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStatPayment',
                {
                    startday:Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brandStat:''
                   
                }
            );
          
            this.searchStatHourly();

        },
          searchStatHourly: function(button, e)
        {
           
            var search = this.getViewModel().get('searchStatPayment'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('paymentstat'), filter);
            //this.total();
        },
         resetStatDaily: function(button, e)
        {
            this.getViewModel().set
            (
                'searchStatPayment',
                {
                    startday:Tcitel.controller.common.Common.monthBeforeDate(),
                    endday: Ext.Date.format(new Date(), 'Y-m-d'),
                    brandStat:''
                   
                }
            );
          
            this.searchStatDaily();

        },
          searchStatDaily: function(button, e)
        {
           
            var search = this.getViewModel().get('searchStatPayment'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('paymentstat_daily'), filter);
            //this.total();
        },

        activateTab: function(tab, eOpts)
        {
           var combo=this.lookupReference('end_date_stat'),
               combostart=this.lookupReference('start_date_stat');
           //console.log(combo);
           if (tab.xtype == 'chartPayment')
            {
                //console.log(combostart);
                combostart.setHidden(true);
                combo.setFieldLabel('Day');
                this.searchStatHourly();
               
            }
             if (tab.xtype == 'chartPaymentDaily')
            {
              combostart.setHidden(false);
              combo.setFieldLabel('End');
              this.searchStatDaily();
               
            }
          
        },
           edit: function(button, e)
        {
            var record = button.up('form');
            //.getWidgetRecord();
            //console.log(record);
            var partnersedit = Ext.widget('balancesearch');
            partnersedit.loadRecord(record);
            this.getView().add(partnersedit);
            partnersedit.show();
        }//,

    }
);
