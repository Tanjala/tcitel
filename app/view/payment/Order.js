Ext.define
(

    'Tcitel.view.payment.Order',
    {
    	 extend:'Ext.panel.Panel',
         xtype:'order',
         title:'Payment Transaction',
          itemId: 'order',
        layout: 'border',
        items:
        [
            {
               xtype: 'ordernorth',
                region: 'north'
            },
            {
                xtype: 'ordercenter',
                region: 'center'
            }
        ]

    }
);