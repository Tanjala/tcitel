Ext.define
(

    'Tcitel.view.payment.Payment',
    {
    	 extend:'Ext.panel.Panel',
         xtype:'payment',
         title:'Transaction',
          itemId: 'payment',
         
        layout: 'border',
        items:
        [
            {
                xtype: 'paymentnorth',
                region: 'north'
            },
            {
                xtype: 'paymentcenter',
                region: 'center'
            }
        ]

    }
);