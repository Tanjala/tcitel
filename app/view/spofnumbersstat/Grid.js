Ext.define
(
    'Tcitel.view.spofnumbersstat.Grid',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'dataStat',
        title: 'Statistic Data',
        bind: {store:'{statGrid}'},
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{statGrid}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1
            },
               
            items:
            [
                {
                    text: 'Day',
                    dataIndex: 'dan',
                    format: 'Y-m-d'
                  
                },
           
                 {
                    dataIndex :  'type',
                    id: 'hiddenheader4',
                    width: 240,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    items:
                    [
                        {
                            text      :  'Number Type',
                            dataIndex :  'type',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'combo',
                            reference: 'type',
                            flex : 1,
                            padding: 3,
                            minChars: 0,
                            name: 'type',
                            bind: {store:'{numtype}'},
                            displayField: 'display',
                            valueField: 'value',
                            allowBlank: true,
                            value: '',
                            listeners:
                            {
                               select: 'typecomboSelect'
                            }
                        }
                    ]
                },
            
                {
                    text: 'Total Numbers',
                    dataIndex: 'counter'
                },
                {
                    text: 'Brand',
                    dataIndex: 'brand'
                   
                },
                  {
                    dataIndex :  'expired',
                    name: 'expired',
                    id: 'hiddenheader5',
                    width: 240,
                    flex: false,
                    layout:
                    {
                        type: 'hbox'
                    },
                    renderer: function (value)
                    {
                        return value == 't' ? 'EXPIRED' : 'ACTIVE';
                    },
                    items:
                    [
                        {
                            text      :  'Expired',
                            dataIndex :  'expired',
                            sortable: false,
                            flex: 1,
                            height: 30
                        },
                        {
                            xtype: 'combo',
                            reference: 'expired',
                            flex : 1,
                            padding: 3,
                            minChars: 0,
                            name: 'expired',
                            bind: {store:'{expiredGrid}'},
                            displayField: 'display',
                            valueField: 'value',
                            allowBlank: true,
                            value: '',
                            listeners:
                            {
                                
                                select: 'gridcomboSelect'
                            }
                        }
                    ]
                }
               
            ]
        }
    }
);
