

Ext.define('Tcitel.view.spofnumbersstat.Donut', {
    extend: 'Ext.Panel',
    xtype: 'pie-donut',
     layout:
        {
            type: 'fit',
            align: 'stretch'
        },
     header:
        {
            hidden:'true'
        },
   border:false,
   frame:false,
   bodyBorder:false,
   width: 550,

    initComponent: function() {
        var me = this;

       /* me.myDataStore = Ext.create('Ext.data.JsonStore', {
            fields: ['type', 'counter' ],
            data: [
                { type: 'special', counter: 68888 },
                { type: 'trial', counter: 1345 },
                { type: 'real', counter: 17.9 },
                { type: 'virtual', counter: 10.2 },
                { type: 'procescom', counter: 1.9 }
            ]
        });*/

        me.items = 
        [
         {
            xtype: 'polar',
            width: '100%',
            border:false,
           //bodyBorder:false,
            borderColor: 'white',
            frame:false,
            height: 378,
            store: this.myDataStore,
            insetPadding: 20,
            innerPadding: 20,
            legend: 
            {
                border:false,
                hidden:true/*,
                docked: 'bottom',
                labelFont: '6px Comic Sans MS'*/
             
            },
            bind : {store: '{donut}'},
            interactions: ['rotate', 'itemhighlight'],
            sprites: 
            [
                {
                    type: 'text',
                    text: 'Daily Number Type Statistic',
                    docked: 'bottom',
                    fontSize: 14,
                    width: 100,
                    height: 40,
                    x: 40, // the sprite x position
                    y: 80  // the sprite y position
                }
            ],
            series: 
            [
               {
                    type: 'pie',
                    angleField: 'counter',
                    donut: 60,
                    label:
                    {
                        field: 'type',
                        display: 'outside'
                    },
                highlight: true,
                tooltip:
                 {
                    trackMouse: true,
                    style: 'background: #fff',
                    renderer: function(storeItem, item) 

                    {
                        this.setHtml(storeItem.get('type') + ': ' + storeItem.get('counter'));
                    }
                }
            }]
        }];

        this.callParent();
    }
});
