Ext.define
(
    'Tcitel.view.spofnumbersstat.Search',
    {
        extend: 'Ext.form.Panel',
        xtype: 'search',
        //padding: '50 0 0 0',
         header:
        {
            hidden:'true'
        },
        monitorValid: true,
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 60,
                    allowBlank: false,
                    width: 190
                },
                items:
                [
                     {
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        margin: '10 0 5 5',
                        editable: false,
                        //value:  'Y-m-d',
                        value: Tcitel.controller.common.Common.monthBeforeDate(),
                        name: 'startday',
                        fieldLabel: 'Start',
                        listeners:
                        {
                            change: 'change'
                        }
                   },
                     {
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        margin: '0 0 5 5',
                        editable: false,
                        value:  Ext.Date.format(new Date(), 'Y-m-d'),
                        name: 'endday',
                        fieldLabel: 'End',
                        listeners:
                        {
                            change: 'change'
                        }
                     },
                     {
                        xtype: 'combo',
                        name: 'brandStat',
                        hiddenName: 'brandStat',
                        margin: '0 0 5 5',
                        fieldLabel: 'Brand',
                        bind: {store:'{brandli}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelectStat'
                        }
                    },
                   
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 0 0 70',
                        width: 60,
                        handler: 'resetTab'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 5',
                        width: 60,
                        handler:'searchTab'
                       
                    }
                ]
            }
        ]
    }
);
