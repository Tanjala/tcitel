Ext.define
(
    'Tcitel.view.spofnumbersstat.ChartExpired',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'chart_expired',
        margin: 5,
        frame:true,
        bodyBorder:false,
       // style: 'backgroundColor: white',
        layout:
        {
            type: 'fit',
            align: 'stretch'
        },
        initComponent: function()
        {
            var me = this;
            me.items =
            [
                {
                    xtype: 'cartesian',
                    legend:
                    {
                        docked: 'right'
                    },
                    bind : {store: '{spofstatExpired}'},
                    insetPadding:
                    {
                        top: 100,
                        left: 40,
                        right: 40,
                        bottom: 40
                    },
                    sprites:
                    [
                        {
                            type: 'text',
                            text: 'Expired Special Offer Numbers',
                            fontSize: 16,
                            width: 100,
                            height: 20,
                            x: 40, // the sprite x position
                            y: 20  // the sprite y position
                         }
                    ],
                    axes:
                    [
                        {
                            type: 'numeric',
                            position: 'left',
                            grid: true,
                            minimum: 0,
                            title:'Total Numbers Expired'
                        },
                        {
                            type: 'category',
                            position: 'bottom',
                            grid: true,
                            fields: ['dan'],
                            title:'Days',
                            label:
                            {
                                rotate: { degrees: -60 }
                            }
                        }
                    ],
                    series:
                    [
                        {
                            type: 'bar',
                            axis: 'left',
                            title: [ 'SPECIAL', 'TRIAL' ],
                            xField: 'dan',
                            yField: [ 'SPECIAL', 'TRIAL' ],
                            colors : ['#A7BD2F', '#00A0B0'],
                            stacked: true,
                            style:
                            {
                                opacity: 0.80
                            },
                            highlight:
                            {
                                fillStyle: 'yellow'
                            },
                            tooltip:
                            {
                                style: 'background: #fff',
                                renderer: function(storeItem, item)
                                {
                                    var network = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                                    this.setHtml(network + ' for ' + storeItem.get('dan') + ': ' + storeItem.get(item.field));
                                }
                            }
                        }
                    ]
                }
            ];
            this.callParent();
        }
    }
);