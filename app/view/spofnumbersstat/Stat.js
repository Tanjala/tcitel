Ext.define
(
    'Tcitel.view.spofnumbersstat.Stat',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'stat_numbers',
        title: 'Statistic',
        requires:
        [
            'Tcitel.view.spofnumbersstat.Charts',
             'Tcitel.view.spofnumbersstat.Search',
             'Tcitel.view.spofnumbersstat.Grid',
             'Tcitel.view.spofnumbersstat.Donut',
             'Tcitel.view.spofnumbersstat.NumbersTab'
          
        ],
    

     layout:
        {
            type: 'hbox',
            align: 'stretch'
        },

        defaults:
        {
            xtype: 'panel',
            bodyStyle: 'background:white',
            border:false
        },
        items:
        [
            {
                width: 250,

                layout: 'border',
 
                items:
                [
                    {
                        title:'Search',
                        region: 'north',
                         xtype: 'search'
                      
                    },
                    {
                        xtype: 'pie-donut',
                        region:'center',
                        border:false
                    }
                ]
            },
            {
                flex: 1,
                  border: false,
                  margin: 0,
                 layout:
                {
                    type: 'fit'
                },
               
                items:
                [
                    {
                        xtype: 'spofnumberstabstat'
                    }
                  
                ]
            }
        ]


     
    }
);