Ext.define
(
    'Tcitel.view.spofnumbersstat.ChartsVirtual',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartNumbersVirtual',
        title: 'Virtual Numbers <i class="fa fa-bar-chart"></i>',
        requires:
        [
            'Tcitel.view.spofnumbersstat.ChartVirtual'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
           
          
             {
                 xtype: 'chart_virtual',
                   region: 'center'
            }
        ]
    }
);