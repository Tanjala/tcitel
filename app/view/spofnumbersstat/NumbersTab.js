Ext.define
(
    'Tcitel.view.spofnumbersstat.NumbersTab',
    {
    	extend: 'Ext.tab.Panel',
    	xtype: 'spofnumberstabstat',
        itemId:'spofnumberstabstat',
        defaults: 
        {
            listeners: 
            {
                activate:'activateTab'       
             }
        },
        border:false,
        requires:
        [
            'Tcitel.view.spofnumbersstat.Charts',
            'Tcitel.view.spofnumbersstat.ChartsExpired',
            'Tcitel.view.spofnumbersstat.ChartsVirtual',
            'Tcitel.view.spofnumbersstat.Grid',
            'Tcitel.view.spofnumbersstat.ChartsRealVirtual'
        ],
    	items:
        [
            {
                xtype: 'chartNumbers'
            },
            {
                xtype: 'chartNumbersExpired'
            },
            {
                xtype: 'chartNumbersVirtual'
            },
            {
                xtype: 'chartNumbersReal'
            },
            {
                xtype: 'dataStat'
            }
        ]
    }
);