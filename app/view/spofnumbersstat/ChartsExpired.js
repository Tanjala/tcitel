Ext.define
(
    'Tcitel.view.spofnumbersstat.ChartsExpired',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartNumbersExpired',
        title: 'Expired Special Offer Numbers <i class="fa fa-bar-chart"></i>',
        requires:
        [
            'Tcitel.view.spofnumbersstat.ChartExpired'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
          
             {
                 xtype: 'chart_expired',
                   region: 'center'
            }
        ]
    }
);