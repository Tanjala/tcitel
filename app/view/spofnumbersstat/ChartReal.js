Ext.define( 'Tcitel.view.spofnumbersstat.ChartReal', {
    extend: 'Ext.Panel',
    xtype: 'chart_real',
     margin: 5,
   // width: 650,
   // height: 500,
    layout:
        {
            type: 'fit',
            align: 'stretch'
        },

    initComponent: function () {
        var me = this;

        me.items = {
            xtype: 'cartesian',
             bind : {store: '{real}'},
            
            insetPadding: 
            {
                top: 100,
                bottom: 40,
                left: 20,
                right: 40
            },
            interactions: 'itemhighlight',
            axes: 
            [{
                type: 'numeric',
                position: 'left',
                minimum: 40,
                titleMargin: 20,
                title: 
                {
                    text: 'Total Real No.'
                }
              
            }, {
                type: 'category',
                position: 'bottom'
            }],
            animation: Ext.isIE8 ? false : {
                easing: 'backOut',
                duration: 500
            },
            series: 
            {
                type: 'bar',
                xField: 'dan',
                yField: 'counter',
                style: 
                {
                    minGapWidth: 5
                },
                highlight: 
                {
                    strokeStyle: 'black',
                    fillStyle: 'gold',
                    lineDash: [5, 3]
                },
                label: 
                {
                    field: 'counter',
                    display: 'insideEnd',
                    renderer: function (value) 
                    {
                        return value;
                    }
                }
            },
            sprites: 
            {
                type: 'text',
                text: 'Real Numbers',
                fontSize: 14,
                width: 100,
                height: 20,
                x: 40, // the sprite x position
                y: 20  // the sprite y position
            }
        };

        this.callParent();
    }
});