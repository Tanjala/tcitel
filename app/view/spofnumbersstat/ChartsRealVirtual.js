Ext.define
(
    'Tcitel.view.spofnumbersstat.ChartsRealVirtual',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartNumbersReal',
        title: 'Real Numbers <i class="fa fa-bar-chart"></i>',
        requires:
        [
           'Tcitel.view.spofnumbersstat.ChartReal'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
           
            {
                xtype: 'chart_real',
                //width:'50%',
                 region: 'center'
            }
        ]
    }
);