Ext.define
(
    'Tcitel.view.spofnumbersstat.Charts',
    {
    	extend: 'Ext.panel.Panel',
    	xtype: 'chartNumbers',
        title: 'Active Special Offer Numbers <i class="fa fa-bar-chart"></i>',
         //border: false,
        requires:
        [
           'Tcitel.view.spofnumbersstat.ChartActive'
            
          
        ],
        border: false,
        margin: 0,
        padding: '20 10 50 10',
         header:
        {
            hidden:'true'
        },
        layout: 'border',
        bodyStyle: 'background:white',

       
        items:
        [
           
            {
                xtype: 'chart_active',
                //width:'50%',
                 region: 'center'
            }
        ]
    }
);