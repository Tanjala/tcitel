Ext.define
(
    'Tcitel.view.messagelog.MessageLog',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'messagelog',
        title: 'Message Log',
        itemId: 'messagelog',
        controller: 'messagelog',
        viewModel:
        {
            type: 'messagelog'
        },
        requires:
        [
    
            'Tcitel.view.messagelog.North',
            'Tcitel.view.messagelog.Center',
            'Tcitel.view.messagelog.MessageLogController',
             'Tcitel.view.messagelog.MessageLogViewModel'
        ],
        layout: 'border',
        
        
            defaults:
            {
                xtype: 'label'
            },
            
         
        items:
        [
            {
                xtype: 'messagelognorth',
                region: 'north'
            },
            {
                xtype: 'messagelogcenter',
                region: 'center'
            }
        ]
      
    }
);
