Ext.define
(
    'Tcitel.view.messagelog.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'messagelognorth',
         header:
        {
            hidden:'true'
        },
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20,
            padding: 20
        },
        items:
        [
        {
            title: 'Message Log Search',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 40
                    }
                },
                items:
                [
                {
                    margin: '0 20 0 0',
                    items:
                    [
                    {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                        bind: '{search.log_start}'
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{search.log_end}'
                    },
                     {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '5 30 0 45',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '5 0 0 0',
                        width: 70,
                       handler: 'search'
                    }
                    ]
                }/*,
                {  
                   margin: '0 20 0 0',
                   items:
                   [
                   {
                        xtype: 'combo',
                        labelWidth: 70,
                       // pading: '5 10 10 0',
                        name: 'app_state',
                        hiddenName: 'app_state',
                        fieldLabel: 'App State',
                       // bind: {store:'{brand}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '5 30 0 75',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '5 0 0 0',
                        width: 70,
                       handler: 'search'
                    }
                    ]
                }*/
                ]
            }
            ]
        }
        ]
    }
);
