Ext.define
(
    'Tcitel.view.messagelog.Center',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'messagelogcenter',
        bind: {store:'{message}'},
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{message}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false,
                editor:
                {
                    xtype: 'textarea',
                    readOnly: true
                }
            },
            items:
            [
               
                {
                    text: 'Received At',
                    dataIndex: 'received_at',
                    sortable: true
                },
                {
                    text: 'Notified At',
                    dataIndex: 'notified_at'
                   
                },
                {
                    text: 'To',
                    dataIndex: 'to'
                   
                },
                {
                    text: 'Text',
                    dataIndex: 'text'
                },
                {
                    text: 'App State',
                    dataIndex: 'app_state'
                },
                {
                    text: 'Lock Value',
                    dataIndex: 'lock_value'
                }
            ]
        }
    }
);
