Ext.define
(
    'Tcitel.view.messagelog.MessageLogViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.messagelog',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.messagelog.MessageLog'
        ],
        data:
        {
            search:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                app_state: ''
            }
        },
       stores:
        {
            message:
            {
                model: 'Tcitel.model.messagelog.MessageLog',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/messagelog/read.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
               
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            }//,
      /*
            brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_cdr',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }*/
        }
    }
);
