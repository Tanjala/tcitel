Ext.define
(
    'Tcitel.view.messagelog.MessageLogController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.messagelog',

        comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

        search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [
                      {property: 'log_start', value: search.log_start},
                      {property: 'log_end', value: search.log_end},
                      {property: 'app_state', value: search.app_state}]
            // this.getStore('message').sorters.clear();
            this.filterStore(this.getStore('message'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    app_state: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        }
    }
);
