Ext.define
(
    'TrafficStat.view.main.Main',
    {
        extend: 'Ext.container.Container',
        plugins: 'viewport',
        xtype: 'first_main',

        requires:
        [
            'TrafficStat.view.main.Main'
           
        ],


        items:
        [
            {
                xtype: 'app-main'
            }
        ]
    }
);