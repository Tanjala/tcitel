Ext.define
(
    'Tcitel.view.main.RefreshAbortStore', 
    {
        extend: 'Ext.data.Store',
        xtype: 'refreshAbort',
        refreshTimeout: 0,
        refreshCond: false,
        refresh: function()
        {
            var me = this;
            if (!me.refreshTimeout) return; 
            clearTimeout(me.timeoutID);
            me.timeoutID = setTimeout
            (
                function()
                {
                    if (me.refreshCond)
                    {
                        me.abort();
                        me.reload();
                    }
                    else
                    {
                        me.refresh();
                    }
                },
                me.refreshTimeout
            );
        },
        constructor: function(config)
        {
            var me = this;
            me.callParent([config]);
            me.on
            (
                {
                    beforeload: function(store, operation)
                    {
                        store.abort();
                        clearTimeout(me.timeoutID);
                        store.lastOperation = operation;
                    },
                    load: me.refresh
                }
            );
        },
        abort: function()
        {
            var me = this;
            if (me.loading && me.lastOperation)
            {
                var requests = Ext.Ajax.requests;
                for (id in requests)
                {
                    if (requests.hasOwnProperty(id) && requests[id].options == me.lastOperation.request)
                    {
                        Ext.Ajax.abort(requests[id]);
                        delete requests[id];
                        break;
                    }
                }
            }
        }
    }
);
