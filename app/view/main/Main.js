/**
* This class is the main view for the application. It is specified in `app.js` as the
* "autoCreateViewport" property. That setting automatically applies the "viewport"
* plugin to promote that instance of this class to the body element.
*
* TODO - Replace this content of this view to suite the needs of your application.
*/
Ext.define
(
    'Tcitel.view.main.Main',
    {
        extend: 'Ext.container.Container',
        plugins: 'viewport',

        requires:
        [
            'Tcitel.view.main.MainController',
            'Tcitel.view.main.MainModel',
            'Tcitel.view.header.Header',
            'Tcitel.view.pages.Pages'
        ],

        xtype: 'main',

        controller: 'main',
        viewModel:
        {
            type: 'main'
        },

        layout:
        {
           type: 'border'
        },

        items:
        [
            {
                region: 'north',
                xtype: 'app-header'
            },
            {
                region: 'center',
                xtype: 'pages'
            }
        ]
    }
);