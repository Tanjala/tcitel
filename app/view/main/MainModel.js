/**
 * This class is the view model for the Main view of the application.
 */
Ext.define
(
	'Tcitel.view.main.MainModel',
	{
    	extend: 'Ext.app.ViewModel',
	    alias: 'viewmodel.main',
	    requires:
        [
            'Tcitel.view.main.RefreshAbortStore'
        ],
	    data:
	    {
        	name: 'Tcitel'
    	},

    	 stores:
        {
            refresh: Ext.create('Tcitel.view.main.RefreshAbortStore',
            {
                xtype: 'refreshAbort',
                refreshTimeout: 1 * 60 * 1000,
                refreshCond: true,
                proxy:
                {
                    type: 'ajax',
                    url: 'php/session/session.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
               
               /* remoteFilter: true,
                remoteSort: true,*/
                autoLoad: true
            })

        }

    //TODO - add data, formulas and/or methods to support your view

	}
);
