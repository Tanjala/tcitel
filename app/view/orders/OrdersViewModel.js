Ext.define
(
    'Tcitel.view.orders.OrdersViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.orders',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.orders.Orders'
        ],
        data:
        {
            search:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                user_id: '',
                status: '',
                product: ''
            }
        },
       stores:
        {
            orders:
            {
                model: 'Tcitel.model.orders.Orders',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/orders/orders.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
            status:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/orders/status.php',
                    extraParams:
                    {
                        table: 'orders',
                        valueField: 'value',
                        displayField: 'display',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
