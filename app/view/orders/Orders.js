Ext.define
(
    'Tcitel.view.orders.Orders',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'orders',
        title: 'Orders',
        itemId: 'orders',
        controller: 'orders',
        viewModel:
        {
            type: 'orders'
        },
        requires:
        [
            'Tcitel.view.orders.North',
            'Tcitel.view.orders.Center',
            'Tcitel.view.orders.OrdersController',
            'Tcitel.view.orders.OrdersViewModel'
        ],
        layout: 'border',
        defaults:
        {
            xtype: 'label'
        },
        items:
        [
            {
                xtype: 'ordersnorth',
                region: 'north'
            },
            {
                xtype: 'orderscenter',
                region: 'center'
            }
        ]

    }
);
