Ext.define
(
    'Tcitel.view.orders.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'ordersnorth',
        title: 'Orders',
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20,
            padding: 20
        },
        items:
        [
            {
                title: 'Orders Search',
                items:
                [
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults:
                        {
                            border: false,
                            defaults:
                            {
                                xtype: 'textfield',
                                labelWidth: 70,
                                listeners:
                                {
                                    specialkey: 'specialKey'
                                }
                            }
                        },
                        items:
                        [
                            {
                                margin: '0 20 0 0',
                                items:
                                [
                                    {
                                        xtype: 'datetimefield',
                                        name: 'log_start',
                                        editable: false,
                                        fieldLabel: 'Start',
                                        format: 'Y-m-d',
                                        value: Tcitel.controller.common.Common.midnight(),
                                        bind: '{search.log_start}'
                                    },
                                    {
                                        xtype: 'datetimefield',
                                        name: 'log_end',
                                        editable: false,
                                        fieldLabel: 'End',
                                        format: 'Y-m-d',
                                        value: Tcitel.controller.common.Common.almostMidnight(),
                                        bind: '{search.log_end}'
                                    },
                                    {
                                        name: 'user_id',
                                        maskRe: /[0-9]/,
                                        fieldLabel: 'User ID',
                                        bind: '{search.user_id}'
                                    },
                                    {
                                        name: 'billing_id',
                                        maskRe: /[0-9]/,
                                        fieldLabel: 'Billing ID',
                                        bind: '{search.billing_id}'
                                    }
                                ]
                            },
                            {
                                items:
                                [
                                    {
                                        xtype: 'combo',
                                        name: 'status',
                                        hiddenName: 'status',
                                        fieldLabel: 'Status',
                                        bind: {store:'{status}'},
                                        displayField: 'display',
                                        valueField: 'value',
                                        queryMode: 'local',
                                        value: 'pending',
                                        matchFieldWidth: false,
                                        listeners:
                                        {
                                            change: 'comboSelect',
                                            select: 'comboSelect',
                                            specialkey: 'specialKey'
                                        }
                                    },
                                    {
                                        name: 'product',
                                        fieldLabel: 'Product',
                                        bind: '{search.product}'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Reset',
                                        margin: '10 30 0 75',
                                        width: 70,
                                        handler: 'reset'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Search',
                                        margin: '10 0 0 0',
                                        width: 70,
                                        handler: 'search'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
);
