Ext.define
(
    'Tcitel.view.orders.Center',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'orderscenter',
        bind: {store:'{orders}'},
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{orders}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false,
                editor:
                {
                    xtype: 'textfield',
                    readOnly: true
                }
            },
            items:
            [
                {
                    text: 'Update time',
                    dataIndex: 'update_time',
                    editor: null,
                    sortable: true
                },
                {
                    text: 'User ID',
                    dataIndex: 'user_id'
                },
                {
                    text: 'Billing ID',
                    dataIndex: 'billing_id'
                },
                {
                    text: 'Status',
                    dataIndex: 'status',
                    editor:
                    {
                        xtype: 'textarea',
                        readOnly: true
                    }
                },
                {
                    text: 'Product',
                    dataIndex: 'product'
                },
                {
                    text: 'Sandbox',
                    dataIndex: 'sandbox'
                },
                {
                    text: 'Google Order ID',
                    dataIndex: 'google_order_id'
                }
            ]
        }
    }
);
