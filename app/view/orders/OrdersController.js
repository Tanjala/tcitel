Ext.define
(
    'Tcitel.view.orders.OrdersController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.orders',

        comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

        search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('orders'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    user_id: '',
                    status: '',
                    product: ''
                }
            );
            button.up('form').getForm().reset();
            this.search();
        }
    }
);
