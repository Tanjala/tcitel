Ext.define
(
	'Tcitel.view.menucardtab.MenuCardTab',
	{
		extend: 'Ext.ux.panel.MenuCardTab',
		xtype: 'app-menucardtab',

	    title: 'MenuCardTab1',

        items:
        [
        	{
        		title: 'Page 3',
        		itemId: 'card3'
        	},
        	{
        		title: 'Page 4',
        		itemId: 'card4'
        	},
        	{
        		title: 'Page 5',
        		itemId: 'card5'
        	}
        ]
    }
);
