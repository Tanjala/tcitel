Ext.define
(
    'Tcitel.view.cdrlog.CdrLog',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'cdrlog',
        title: 'Cdr Log',
        itemId: 'cdrlog',
        controller: 'cdrlog',
        viewModel:
        {
            type: 'cdrlog'
        },
        requires:
        [
    
            'Tcitel.view.cdrlog.North',
            'Tcitel.view.cdrlog.Center',
            'Tcitel.view.cdrlog.CdrLogController',
            'Tcitel.view.cdrlog.CdrLogViewModel'
        ],
        layout: 'border',
        
        
            defaults:
            {
                xtype: 'label'
            },
            
        
        items:
        [
            {
                xtype: 'cdrlognorth',
                region: 'north'
            },
            {
                xtype: 'cdrlogcenter',
                region: 'center'
            }
        ]
      
    }
);
