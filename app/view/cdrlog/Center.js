Ext.define
(
    'Tcitel.view.cdrlog.Center',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'cdrlogcenter',
        bind: {store:'{cdr}'},
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{cdr}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false,
                editor:
                {
                    xtype: 'textfield',
                    readOnly: true
                }
            },
            items:
            [
               /* {
                    width: 200,
                    flex: 1,
                    text: 'Record',
                    dataIndex: 'record',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store)
                    {
                        if (record.data.disposition == "ANSWERED")
                          return   '<audio style="width: 190px;" controls preload="none">\
                                        <source src="'+value+'" type="audio/mpeg">\
                                    </audio>';
                    }
                },*/
                {
                    text: 'Start',
                    dataIndex: 'start',
                    sortable: true,
                    flex: false,
                    width: 145
                },
                {
                    text: 'End',
                    dataIndex: 'end',
                    flex: false,
                    width: 145
                },
                {
                    text: 'Answer',
                    dataIndex: 'answer',
                    flex: false,
                    width: 145
                },
                {
                    text: 'Duration',
                    dataIndex: 'duration'
                },
                {
                    text: 'Billsec',
                    dataIndex: 'billsec'
                },
                {
                    text: 'Source',
                    dataIndex: 'src'
                },
                {
                    text: 'Destination',
                    dataIndex: 'dst'
                },
                {
                    text: 'Caller ID',
                    dataIndex: 'clid'
                },
              /*  {
                    text: 'Acount code',
                    dataIndex: 'accountcode'
                },*/
               /* {
                    text: 'Dcontext',
                    dataIndex: 'dcontext'
                },
                 {
                    text: 'Lastapp',
                    dataIndex: 'lastapp'
                },*/
                {
                    text: 'Disposition',
                    dataIndex: 'disposition'
                },
                /*{
                    text: 'channel',
                    dataIndex: 'channel'
                },
                {
                    text: 'lastdata',
                    dataIndex: 'lastdata'
                },
                {
                    text: 'dstchannel',
                    dataIndex: 'dstchannel'
                },
                {
                    text: 'amaflags',
                    dataIndex: 'amaflags'
                },
                {
                    text: 'userfield',
                    dataIndex: 'userfield'
                },*/
              /*  {
                    text: 'Sequence',
                    dataIndex: 'sequence'
                },
                {
                    text: 'UniqueId',
                    dataIndex: 'uniqueid'
                },
                {
                    text: 'LinkedId',
                    dataIndex: 'linkedid'
                },*/
                {
                    text: 'Call Type',
                    dataIndex: 'call_type'
                },
                {
                    text: 'Brand',
                    dataIndex: 'brand',
                    brand_access: 'Virtual SIM'
                },
               /* {
                    text: 'Peer Account',
                    dataIndex: 'peeraccount'
                },*/
                {
                    text: 'Route Name',
                    dataIndex: 'name'
                }
            ]
        }
    }
);
