Ext.define
(
    'Tcitel.view.cdrlog.CdrLogViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.cdrlog',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.cdrlog.CdrLog'
        ],
        data:
        {
            search:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                src: '',
                dst: '',
                user_id: '',
                brand: '',
                route: '',
                name:'',
                call_type: '',
                disposition: ''
            },
            billsec:''
        },
       stores:
        {
            cdr:
            {
                model: 'Tcitel.model.cdrlog.CdrLog',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/cdrlog/storeCdr.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                listeners:
                {
                    load:'billsecLoad'
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
        disposition:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/cdrlog/disposition.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_cdr',
                        valueField: 'value',
                        displayField: 'display',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            call_type:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'vs_db',
                        table: 'vs_cdr',
                        valueField: 'call_type',
                        displayField: 'call_type',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            route:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        //database: 'asterisk',
                        table: 'vs_routes',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            }
        }
    }
);
