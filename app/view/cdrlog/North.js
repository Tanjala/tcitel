Ext.define
(
    'Tcitel.view.cdrlog.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'cdrlognorth',
        title: 'Cdr Log',
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20,
            padding: 20
        },
        items:
        [
        {
            title: 'Cdr Log Search',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 90
                    }
                },
                items:
                [
                {
                    margin: '0 20 0 0',
                    items:
                    [
                    {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                        bind: '{search.log_start}'
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{search.log_end}'
                    },
                     {
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        brand_access: 'Virtual SIM',
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    /*{
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'disposition',
                        hiddenName: 'disposition',
                        fieldLabel: 'Disposition',
                        bind: {store:'{disposition}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },*/
                     {
                        name: 'billsec',
                        fieldLabel: 'Total Minutes',
                        bind: '{billsec}',
                        margin: '20 20 0 0',
                        readOnly:true
                    }
                    ]
                },
                {  
                   margin: '0 20 0 0',
                   items:
                   [
                     {
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'disposition',
                        hiddenName: 'disposition',
                        fieldLabel: 'Disposition',
                        bind: {store:'{disposition}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                 /*  {
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        displayField: 'display',
                        valueField: 'value',
                        //queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        brand_access: 'Virtual SIM',
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },*/
                     {
                        xtype: 'combo',
                        pading: '5 10 10 0',
                        name: 'name',
                        hiddenName: 'name',
                        fieldLabel: 'Route',
                        bind: {store:'{route}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                   /* {
                        name: 'route',
                        fieldLabel: 'Route',
                        bind: '{search.route}'
                    },*/
                    {
                        xtype: 'combo',
                        name: 'call_type',
                        hiddenName: 'call_type',
                        fieldLabel: 'Call Type',
                        bind: {store:'{call_type}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    }
                   
                   ]

                },
                {
                    items:
                    [
                    {
                        name: 'alias',
                        fieldLabel: 'User ID',
                        bind: '{search.user_id}'
                    },
                    {
                        name: 'source',
                        fieldLabel: 'Source',
                        bind: '{search.src}'
                    },
                    {
                        name: 'destination',
                        fieldLabel: 'Destination',
                        bind: '{search.dst}'
                    },
                   /* {
                        name: 'dst',
                        fieldLabel: 'Dst',
                        bind: '{search.dst}'
                    },,
                    {
                        xtype: 'combo',
                        name: 'dcontext',
                        hiddenName: 'dcontext',
                        fieldLabel: 'Dcontext',
                        bind: {store:'{dcontext}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'combo',
                        name: 'lastapp',
                        hiddenName: 'lastapp',
                        fieldLabel: 'Lastapp',
                        bind: {store:'{lastapp}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },*/
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 30 0 95',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'search'
                    }
                    ]
                }
                ]
            }
            ]
        }
        ]
    }
);
