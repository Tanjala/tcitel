Ext.define
(
    'Tcitel.view.cdrlog.CdrLogController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.cdrlog',

        comboSelect: function(combo, record, eOpts)
        {
            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

        search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [
                      {property: 'src', value: search.src},
                      {property: 'dst', value: search.dst},
                      {property: 'user_id', value: search.user_id},
                      {property: 'log_start', value: search.log_start},
                      {property: 'log_end', value: search.log_end},
                      {property: 'brand', value: search.brand},
                      {property: 'route', value: search.route},
                      {property: 'name', value: search.name},
                      {property: 'call_type', value: search.call_type},
                      {property: 'disposition', value: search.disposition}];
            this.getStore('cdr').sorters.clear();
            this.filterStore(this.getStore('cdr'), filter);
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    src: '',
                    dst: '',
                    user_id: '',
                    brand: '',
                    route: '',
                    name: '',
                    call_type: '',
                    disposition: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },
        billsecLoad: function(store, records, successful, eOpts)
        {
            //console.log(records);
             if(successful && records.length)
             {
                 var billsec = store.first().data.billsectotal;
             //debugger;
                 this.getViewModel().set('billsec', billsec);
                
                 //this.getStore('orderlog').load({params:{'ids[]': customer.data.ids}});
             }
        }
    }
);
