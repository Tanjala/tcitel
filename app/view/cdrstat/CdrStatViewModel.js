Ext.define
(
    'Tcitel.view.cdrstat.CdrStatViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.cdrstat',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.cdrstat.CdrStat'
        ],
        data:
        {
            search:
            {
                day: Ext.Date.format(new Date(), 'Y-m-d')
            }
        },
        stores:
        {
            cdrstat:
            {
                storeId: 'cdrstat',
                model: 'Tcitel.model.cdrstat.CdrStat',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/cdrstat/stat.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            }
        }
    }
);
