Ext.define
(
    'Tcitel.view.cdrstat.West',
    {
        extend: 'Ext.form.Panel',
        xtype: 'cdrstatwest',
        layout: 'hbox',
        items:
        [
            {
                xtype: 'fieldset',
                margin: 20,
                padding: 20,
                title: 'Filter',
                items:
                [
                    {
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        width: 180,
                        editable: false,
                        value: new Date(),
                        labelWidth: 50,
                        name: 'day',
                        fieldLabel: 'Date',
                        listeners:
                        {
                            change: 'change'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Apply',
                        handler: 'search',
                        margin: '10 0 0 110',
                        width: 70
                    }
                ]
            }
        ]
    }
);
