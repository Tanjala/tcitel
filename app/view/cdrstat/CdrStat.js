Ext.define
(
    'Tcitel.view.cdrstat.CdrStat',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'cdrstat',
        //title: 'Hourly Stat',
        border: false,
        itemId: 'cdrstat',
        controller: 'cdrstat',
        header:
        {
            //title: 'Hourly Stat',
            border: false
        },
        viewModel:
        {
            type: 'cdrstat'
        },
        requires:
        [

            'Tcitel.view.cdrstat.West',
            'Tcitel.view.cdrstat.Center',
            'Tcitel.view.cdrstat.CdrStatController',
            'Tcitel.view.cdrstat.CdrStatViewModel'
        ],
        layout: 'border',

        defaults:
        {
            xtype: 'label'
        },

        viewModel:
        {
            type: 'cdrstat'
        },
        items:
        [
            {
                border: false,
                xtype: 'cdrstatwest',
                region: 'west'
            },
            {
                border: false,
                xtype: 'cdrstatcenter',
                region: 'center'
            }
        ]
    }
);
