Ext.define
(
    'Tcitel.view.cdrstat.Center',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'cdrstatcenter',
        //title: '&nbsp;',

        layout: 'fit',

        border: false,

        items:
        [
            {
                xtype: 'cartesian',
                border: false,
                //store: Ext.getStore('cdrstat'),
                bind: {store: 'cdrstat'},
                /*
                SELECT s.`hour`, IFNULL(a.counter,0) 'ANSWERED', IFNULL(b.counter,0) 'BUSY', IFNULL(n.counter,0) 'NO ANSWER', IFNULL(f.counter,0) 'FAILED'
                FROM
                stat s
                LEFT JOIN stat a ON s.hour = a.hour AND s.day = a.day AND a.disposition = 'ANSWERED'
                LEFT JOIN stat b ON s.hour = b.hour AND s.day = b.day AND b.disposition = 'BUSY'
                LEFT JOIN stat n ON s.hour = n.hour AND s.day = n.day AND n.disposition = 'NO ANSWER'
                LEFT JOIN stat f ON s.hour = f.hour AND s.day = f.day AND f.disposition = 'FAILED'
                WHERE s.day = '2015-04-28' GROUP BY  s.`hour`
                */
               /* store:
                {
                    fields: ['hour', 'ANSWERED', 'BUSY', 'NO ANSWER', 'FAILED'],
                    data: [
                        {"hour": 1,"ANSWERED": 5,"BUSY": 2,"NO ANSWER": 8,"FAILED": 0},
                        {"hour": 2,"ANSWERED": 5,"BUSY": 0,"NO ANSWER": 4,"FAILED": 1},
                        {"hour": 3,"ANSWERED": 4,"BUSY": 7,"NO ANSWER": 60,"FAILED": 1},
                        {"hour": 4,"ANSWERED": 6,"BUSY": 0,"NO ANSWER": 18,"FAILED": 0},
                        {"hour": 5,"ANSWERED": 3,"BUSY": 5,"NO ANSWER": 14,"FAILED": 0},
                        {"hour": 6,"ANSWERED": 0,"BUSY": 1,"NO ANSWER": 6,"FAILED": 0},
                        {"hour": 7,"ANSWERED": 2,"BUSY": 2,"NO ANSWER": 5,"FAILED": 0},
                        {"hour": 8,"ANSWERED": 2,"BUSY": 0,"NO ANSWER": 16,"FAILED": 0},
                        {"hour": 9,"ANSWERED": 2,"BUSY": 1,"NO ANSWER": 6,"FAILED": 0},
                        {"hour": 10,"ANSWERED": 4,"BUSY": 1,"NO ANSWER": 48,"FAILED": 0},
                        {"hour": 11,"ANSWERED": 12,"BUSY": 3,"NO ANSWER": 31,"FAILED": 1},
                        {"hour": 12,"ANSWERED": 9,"BUSY": 0,"NO ANSWER": 20,"FAILED": 2},
                        {"hour": 13,"ANSWERED": 3,"BUSY": 6,"NO ANSWER": 31,"FAILED": 0},
                        {"hour": 14,"ANSWERED": 3,"BUSY": 7,"NO ANSWER": 40,"FAILED": 1},
                        {"hour": 15,"ANSWERED": 5,"BUSY": 4,"NO ANSWER": 20,"FAILED": 1},
                        {"hour": 16,"ANSWERED": 7,"BUSY": 3,"NO ANSWER": 34,"FAILED": 3},
                        {"hour": 17,"ANSWERED": 4,"BUSY": 6,"NO ANSWER": 13,"FAILED": 1},
                        {"hour": 18,"ANSWERED": 2,"BUSY": 0,"NO ANSWER": 67,"FAILED": 0},
                        {"hour": 19,"ANSWERED": 8,"BUSY": 1,"NO ANSWER": 35,"FAILED": 0},
                        {"hour": 20,"ANSWERED": 8,"BUSY": 1,"NO ANSWER": 12,"FAILED": 5},
                        {"hour": 21,"ANSWERED": 23,"BUSY": 14,"NO ANSWER": 51,"FAILED": 4},
                        {"hour": 22,"ANSWERED": 19,"BUSY": 0,"NO ANSWER": 30,"FAILED": 0},
                        {"hour": 23,"ANSWERED": 4,"BUSY": 4,"NO ANSWER": 13,"FAILED": 1},
                        {"hour": 24,"ANSWERED": 1,"BUSY": 5,"NO ANSWER": 30,"FAILED": 0}
                    ]
                }, */

                insetPadding: 50,

                interactions: ['panzoom'],

                legend:
                {
                    docked: 'right',
                    border: false
                },

                //define x and y axis.
                axes:
                [
                    {
                        type: 'numeric',
                        position: 'left',
                        grid: true,
                        minimum: -5
                    },
                    {
                        type: 'category',
                        //visibleRange: [0, 1],
                        position: 'bottom',
                        grid: true
                    }
                ],

                //define the actual series
                series:
                [
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'ANSWERED',
                        title: 'ANSWERED',
                        marker:
                        {
                            type: 'cross',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: {
                            //fill: "#115fa6",
                            //stroke: "#115fa6",
                            //fillOpacity: 0.6,
                            //miterLimit: 3,
                            //lineCap: 'miter',
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'BUSY',
                        title: 'BUSY',
                        marker:
                        {
                            type: 'triangle',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: {
                            //smooth: true,
                            //stroke: "#94ae0a",
                            //fillOpacity: 0.6,
                            //miterLimit: 3,
                            //lineCap: 'miter',
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'NO ANSWER',
                        title: 'NO ANSWER',
                        marker:
                        {
                            type: 'square',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: {
                            // smooth: true,
                            // stroke: "#94ae0a",
                            // fillOpacity: 0.6,
                            // miterLimit: 3,
                            // lineCap: 'miter',
                            lineWidth: 1.5
                        }
                    },
                    {
                        type: 'line',
                        xField: 'hour',
                        yField: 'FAILED',
                        title: 'FAILED',
                        marker:
                        {
                            type: 'arrow',
                            fx: {
                                duration: 200,
                                easing: 'backOut'
                            }
                        },
                        highlightCfg:
                        {
                            scaling: 2
                        },
                        tooltip:
                        {
                            trackMouse: true,
                            width: 100,
                            //style: 'background: #fff',
                            renderer: function(storeItem, item)
                            {
                                var title = item.series.getTitle();
                                this.setHtml(title + ' for ' + storeItem.get('hour') + ': ' + storeItem.get(item.series.getYField()));
                            }
                        },
                        style: {
                            // smooth: true,
                            // stroke: "#94ae0a",
                            // fillOpacity: 0.6,
                            // miterLimit: 3,
                            // lineCap: 'miter',
                            lineWidth: 1.5
                        }
                    }
                ]
            }
        ]


    }
);