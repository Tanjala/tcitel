Ext.define
(
    'Tcitel.view.header.Header',
    {
        extend: 'Ext.container.Container',
        xtype: 'app-header',
        padding: 10,
        height: 40,
        border:false,
        html: 'Tcitel',
        style: {fontSize: 'large', color: 'white', backgroundColor: '#3892d3'},
        listeners:
        {
            afterRender: function ()
            {
                 this.setHtml (Tcitel.LoggedInUser.data.brand);
            }
        }
        //backgroundColor: '#3892d3' 00aeff
    }
);
