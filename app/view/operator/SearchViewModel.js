Ext.define
(
    'Tcitel.view.operator.SearchViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.operator',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.operator.Operator'
        ],
        data:
        {
            search:
            {
                
                id: '',
                username: '',
                firstname: '',
                lastname: '',
                role: '',
                email: ''
                
            }
        },
       stores:
        {
            operator:
            {
                model: 'Tcitel.model.operator.Operator',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/operator/read.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
              all_brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_users',
                        valueField: 'brand',
                        displayField: 'brand',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name'//,
                       // all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            role:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'ADMIN', value:'ADMIN'},
                    {display: 'USER', value:'USER'}
                ]
            },
            roleForce:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                
                    {display: 'ADMIN', value:'ADMIN'},
                    {display: 'USER', value:'USER'}
                ]
            }
          
        }
    }
);
