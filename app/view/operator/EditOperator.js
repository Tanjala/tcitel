Ext.define
(
    'Tcitel.view.operator.EditOperator',
    {
        extend: 'Ext.form.Panel',
        xtype: 'operatoredit',
        title: 'Edit Operator',
        floating: true,
        draggable: true,
        closable: false,
        modal: true,
        autoShow: false,
        bodyPadding: 20,
        frame: true,
        monitorValid: true,
        defaults:
        {
            xtype: 'textfield',
            allowBlank: false
        },
        initComponent: function()
        {
            this.items =
            [
                {
                    xtype: 'hidden',
                    name: 'id'
                },
                {
                    fieldLabel: 'Username',
                    readOnly: true,
                    name: 'username'
                },
                {
                    fieldLabel: 'Password',
                    inputType: 'password',
                    name: 'password',
                    allowBlank:true
                },
                {
                    fieldLabel: 'First Name',
                    name: 'firstname'
                },
                 {
                    fieldLabel: 'Last Name',
                    name: 'lastname'
                },
                 {
                        xtype: 'combo',
                        name: 'role',
                        hiddenName: 'role',
                        fieldLabel: 'Role',
                        bind: {store:'{roleForce}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                {
                    fieldLabel: 'Email',
                    name: 'email'

                }
            ];
            this.buttons=
            [
                {
                    text: 'Cancel',
                    handler: 'cancel',
                    scope: this,
                    handler: this.close
                },
                {
                    text: 'Save',
                    handler: 'modify',
                    formBind: true
                }
            ];
            this.callParent(arguments);
        }
    }
);
