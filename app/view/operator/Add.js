Ext.define
(
    'Tcitel.view.operator.Add',
    {
        extend: 'Ext.form.Panel',
        xtype: 'operatoradd',
        monitorValid: true,
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 80,
                    allowBlank: false
                },
                items:
                [
                    {
                        name: 'username',
                        fieldLabel: 'Username'
                    },
                    {
                        name: 'password',
                        inputType: 'password',
                        fieldLabel: 'Password'
                    },
                    {
                        xtype: 'combo',
                        name: 'role',
                        hiddenName: 'role',
                        fieldLabel: 'Role',
                        bind: {store:'{roleForce}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false
                    },
                     {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        name: 'firstname',
                        fieldLabel: 'First Name'
                    },
                    {
                        name: 'lastname',
                        fieldLabel: 'Last Name'
                    },
                    {
                        name: 'email',
                        fieldLabel: 'Email',
                        allowBlank: true
                    },
                    {
                        name: 'phone',
                        fieldLabel: 'Phone',
                        allowBlank: true
                    },
                    {
                        xtype: 'button',
                        text: 'Clear',
                        margin: '10 30 0 85',
                        width: 70,
                        handler: 'clear'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'add',
                        formBind: true
                    }
                ]
            }
        ]
    }
);
