Ext.define
(
    'Tcitel.view.operator.OperatorTabs',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'operator-tabs',
        controller: 'operator',
        plain: true,
        items:
        [
            {
                title: '<i class="fa fa-search"></i>',
                tabConfig:
                {
                    tooltip: 'Search'
                },
                xtype: 'operatorsearch'
            },
            {
                title: '<i class="fa fa-user-plus"></i>',
                tabConfig:
                {
                    tooltip: 'Create'
                },
                xtype: 'operatoradd'
            }
        ]
    }
);