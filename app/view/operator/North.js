Ext.define
(
    'Tcitel.view.operator.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'operatorsearch',
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 80,
                    listeners:
                    {
                        specialkey: 'specialKey'
                    }
                },
                items:
                [
                    {
                        name: 'id',
                        fieldLabel: 'ID',
                        bind: '{search.id}'
                    },
                    {
                        name: 'username',
                        fieldLabel: 'Username',
                        bind: '{search.username}'
                    },
                    {
                        name: 'firstname',
                        fieldLabel: 'First Name',
                        bind: '{search.firstname}'
                    },
                    {
                        name: 'lastname',
                        fieldLabel: 'Last Name',
                        bind: '{search.lastname}'
                    },
                    {
                        name: 'email',
                        fieldLabel: 'Email',
                        bind: '{search.email}'
                    },
                    {
                        xtype: 'combo',
                        name: 'role',
                        hiddenName: 'role',
                        fieldLabel: 'Role',
                        bind: {store:'{role}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                     {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{all_brand}'},
                        brand_access: 'Virtual SIM',
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 30 0 85',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'search'
                    }
                ]
            }
        ]
    }
);
