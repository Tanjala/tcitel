Ext.define
(
    'Tcitel.view.operator.GridOperator',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'gridoperator',
        bind: {store:'{operator}'},
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{operator}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        columns:
        {
            defaults:
            {
                flex: 1
            },
            items:
            [
                {
                    text: 'Id',
                    dataIndex: 'id'
                },
                {
                    text: 'Username',
                    dataIndex: 'username'
                },
                {
                    text: 'First Name',
                    dataIndex: 'firstname'
                },
                {
                    text: 'Last Name',
                    dataIndex: 'lastname'
                },
                {
                    text: 'Role',
                    dataIndex: 'role'
                },
                {
                    text: 'Email',
                    dataIndex: 'email'
                },
                {
                    text: 'Phone',
                    dataIndex: 'phone'
                },
                 {
                    text: 'Brand',
                    brand_access: 'Virtual SIM',
                    dataIndex: 'brand'
                },
                  {
                    text: 'Actions',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 25
                        },
                        items:
                        [

                            {
                                html:'<i class="fa fa-pencil"></i>',
                                tooltip: 'Edit',
                                handler: 'edit'
                            },
                            {

                                html:'<i class="fa fa-trash-o"></i>',
                                tooltip: 'Delete',
                                handler: 'delete'
                            }
                        ]
                    }
                }
            ]
        }
    }
);