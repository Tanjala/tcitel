Ext.define('Tcitel.view.operator.SearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.operator',

     comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

         search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('operator'), filter);
        },

        clear: function(button, e)
        {
            button.up('form').getForm().reset();
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    id: '',
                    username: '',
                    firstname: '',
                    lastname: '',
                    email: '',
                    role: ''
                }
            );
            button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },
        add: function(button, e)
        {
            var form = button.up('form'),
            values = form.getValues(),
            store = this.getStore('operator'),
            partner = Ext.create
            (
                'Tcitel.model.operator.Operator',
                values
            );
            delete partner.id;
            delete partner.data.id;
            partner.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                           form.reset();
                           Ext.Msg.alert('Success!', 'Operator is created.');
                           store.reload();
                        }
                        else
                        {
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );

        },



         save: function(button, e, eOpts)
        {
            var form = button.up('form'),
            store = this.getStore('operator'),
            partner = form.getRecord();
            form.updateRecord();
            form.setLoading(true);
            partner.save
            (
                {
                    params :
                    {
                        id: partner.data.id
                    },
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                            form.setLoading(false);
                            form.close();
                            Ext.Msg.alert('Success!', 'Operator data saved.');
                            store.reload();
                        }
                        else
                        {
                            form.setLoading(false);
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );
        },
         edit: function(button, e)
        {
            var record = button.up('container').getWidgetRecord();
            var partnersedit = Ext.widget('operatoredit');
            partnersedit.loadRecord(record);
            this.getView().add(partnersedit);
            partnersedit.show();
        },
          modify: function(button, e, eOpts)
        {
            var form = button.up('form'),
            provider = form.getRecord(),
            store = this.getStore('operator');
            form.updateRecord(provider);
            provider.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                           form.close();
                           store.reload();
                           Ext.Msg.alert('Success!', 'Operator is changed.');
                        }
                        else
                        {
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );
        },
          delete: function(button)
        {
            var partner = button.up('container').getWidgetRecord(),
            store = this.getStore('operator');
            Ext.Msg.confirm
            (
                'Delete Operator?',
                partner.data.username,
                function(btn)
                {
                    if (btn === 'yes')
                    {
                        Ext.getBody().mask();
                        partner.erase
                        (
                            {
                                callback : function(records, operation, success)
                                {
                                    Ext.getBody().unmask();
                                    if (success)
                                    {
                                        store.reload();
                                        Ext.Msg.alert('Success!', 'Operator is deleted.');
                                    }
                                    else
                                    {
                                        var response = Ext.decode(operation._response.responseText);
                                        Ext.Msg.alert('Failure!', response.message);
                                    }
                                }
                            }
                        );
                    }
                }
            )
        }


});