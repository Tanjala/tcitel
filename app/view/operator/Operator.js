Ext.define
(
    'Tcitel.view.operator.Operator',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'operator',
        title:'<i class="fa fa-user"></i>',
        itemId: 'operator',
        controller: 'operator',
        viewModel:
        {
            type: 'operator'
        },
        requires:
        [
            'Tcitel.view.operator.North',
            'Tcitel.view.operator.GridOperator',
            'Tcitel.view.operator.Add',
            'Tcitel.view.operator.OperatorTabs',
            'Tcitel.view.operator.SearchController',
            'Tcitel.view.operator.SearchViewModel',
            'Tcitel.view.operator.EditOperator'
        ],
        layout:
        {
            type: 'hbox',
            align: 'stretch'
        },
        defaults:
        {
            xtype: 'panel',
            defaults:
            {
                border: true,
                margin: 3
            }
        },
        items:
        [
            {
                width: 310,
                items:
                [
                    {
                        xtype: 'operator-tabs',
                        title:'Operator'
                    }
                ]
            },
            {
                flex: 1,
                layout:
                {
                    type: 'fit'
                },
                items:
                [
                    {
                        title : 'Data',
                        xtype: 'gridoperator'
                    }
                ]
            }
        ]
    }
);
