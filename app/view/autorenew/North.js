Ext.define
(
    'Tcitel.view.autorenew.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'autorenewnorth',
        title: 'Autorenew',
        layout: 'hbox',
        defaults:
        {
            xtype: 'fieldset',
            margin: 20,
            padding: 20
        },
        items:
        [
        {
            title: 'Cdr Log Search',
            items:
            [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults:
                {
                    border: false,
                    defaults:
                    {
                        xtype: 'textfield',
                        labelWidth: 70
                    }
                },
                items:
                [
                {
                    margin: '0 20 0 0',
                    items:
                    [
                    {
                        xtype: 'datetimefield',
                        name: 'log_start',
                        editable: false,
                        fieldLabel: 'Start',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.midnight(),
                        bind: '{search.log_start}'
                    },
                    {
                        xtype: 'datetimefield',
                        name: 'log_end',
                        editable: false,
                        fieldLabel: 'End',
                        format: 'Y-m-d',
                        value: Tcitel.controller.common.Common.almostMidnight(),
                        bind: '{search.log_end}'
                    },
                    {
                        name: 'user_id',
                        fieldLabel: 'User ID',
                        bind: '{search.user_id}'
                    },
                    {
                        name: 'billing_id',
                        fieldLabel: 'Billing ID',
                        bind: '{search.billing_id}'
                    }
                    
                    ]
                },
                {
                    items:
                    [
                    {
                        name: 'number',
                        fieldLabel: 'Number',
                        bind: '{search.number}'
                    },
                    {
                        name: 'status',
                        fieldLabel: 'Status',
                        bind: '{search.status}'
                    },
                    {
                        xtype: 'combo',
                        name: 'provider',
                        hiddenName: 'provider',
                        fieldLabel: 'Provider',
                        bind: {store:'{provider}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 30 0 75',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'search'
                    }
                    ]
                }
                ]
            }
            ]
        }
        ]
    }
);
