Ext.define
(
    'Tcitel.view.autorenew.AutorenewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.autorenew',

        comboSelect: function(combo, record, eOpts)
        {

            var name = combo.name;
            this.getViewModel().get('search')[name] = combo.value;
        },

        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },
        search: function(button, e)
        {
            console.log(button);
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('autorenew'), filter);
        },

       /* search: function(button, e)
        {
            var search = this.getViewModel().get('search');
            console.log(search);
            filter = [{property: 'provider', value: search.provider},{property: 'log_start', value: search.log_start},{property: 'log_end', value: search.log_end},{property: 'user_id', value: search.user_id},{property: 'billing_id', value: search.billing_id},{property: 'number', value: search.number},{property: 'status', value: search.status}];
            this.filterStore(this.getStore('autorenew'), filter);
        },*/

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    log_start: Tcitel.controller.common.Common.midnight(),
                    log_end: Tcitel.controller.common.Common.almostMidnight(),
                    provider: '',
                    user_id: '',
                    billing_id: '',
                    number: '',
                    status: ''
                }
            );
            //button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },


     modify: function(button, e, eOpts)
        {
            var form = button.up('form'),
            autorenew = form.getRecord(),
            store = this.getStore('autorenew');
            form.updateRecord(autorenew);
            autorenew.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                           form.close();
                           store.reload();
                           Ext.Msg.alert('Success!', 'Status changed.');
                        }
                        else
                        {
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );
        },

        edit: function(button)
        {
            var record = button.up('container').getWidgetRecord();
            if (record.data.provider == 'didww' && record.data.status == 0)
            {
                var form = Ext.widget('autorenewmodify');
                form.loadRecord(record);
            }
            else
            {
                Ext.Msg.alert('Notification', 'Only status 0 can be changed!');
            }
            //form.down('checkbox').setValue();
        }

    }
);
