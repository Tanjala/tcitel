Ext.define
(
    'Tcitel.view.autorenew.Center',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'autorenewcenter',
        bind: {store:'{autorenew}'},
        border: true,
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{autorenew}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        plugins:
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        columns:
        {
            defaults:
            {
                flex: 1,
                sortable: false
            },
            items:
            [

                {
                    text: 'Transaction time',
                    dataIndex: 'transaction_time',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Number',
                    dataIndex: 'number',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Provider',
                    dataIndex: 'provider',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Status',
                    dataIndex: 'status',
                    editor:
                    {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'User ID',
                    dataIndex: 'user_id'
                },
                 {
                    text: 'Billing ID',
                    dataIndex: 'billing_id'
                },
                {
                    text: 'Amount',
                    dataIndex: 'amount'
                },
                {
                    text: 'Type',
                    dataIndex: 'type'
                },
                {
                    text: 'Actions',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    minWidth: 140,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 120
                        },
                        items:
                        [
                            {
                                text: 'Change status',
                                handler: 'edit'
                            }
                        ]
                    }
                }
            ]
        }
    }
);
