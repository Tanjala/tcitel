Ext.define
(
    'Tcitel.view.autorenew.AutorenewModify',
    {
        extend: 'Ext.form.Panel',
        xtype: 'autorenewmodify',
        title: 'Modify Status',
        controller: 'autorenew',
        viewModel:
        {
            type: 'autorenew'
        },
        floating: true,
        draggable: true,
        closable: false,
        modal: true,
        autoShow: true,
        bodyPadding: 20,
        frame: true,
        monitorValid: true,
        defaults:
        {
            xtype: 'textfield',
            allowBlank: false
        },
        /*items:
        [

        ],*/
        initComponent: function()
        {
            this.items =
            [
                    {
                        xtype: 'combo',
                        name: 'status',
                        hiddenName: 'status',
                        fieldLabel: 'Status',
                        bind: {store:'{status}'},
                        displayField: 'display',
                        valueField: 'value',
                        queryMode: 'local',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    }
            ];
            this.buttons=
            [
                {
                    text: 'Save',
                    handler: 'modify',
                    formBind: true
                },
                {
                    text: 'Cancel',
                    handler: 'cancel',
                    scope: this,
                    handler: this.close
                }
            ];
            this.callParent(arguments);
        }
    }
);
