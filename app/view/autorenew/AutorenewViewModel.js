Ext.define
(
    'Tcitel.view.autorenew.AutorenewViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.autorenew',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.autorenew.Autorenew'
        ],
        data:
        {
            search:
            {
                log_start: Tcitel.controller.common.Common.midnight(),
                log_end: Tcitel.controller.common.Common.almostMidnight(),
                provider: '',
                user_id: '',
                billing_id: '',
                number: '',
                status: ''
            }
        },
       stores:
        {
            autorenew:
            {
                storeId: 'autorenewStore',
                model: 'Tcitel.model.autorenew.Autorenew',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/autorenew/read.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                remoteFilter: true,
                autoLoad: true
            },
            provider:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        database: 'Tcitel',
                        table: 'autorenewlog',
                        valueField: 'provider',
                        displayField: 'provider',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            status:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: '1', value:1},
                    {display: '-1', value:-1}
                ]
            }
        }
    }
);
