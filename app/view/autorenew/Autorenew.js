Ext.define
(
    'Tcitel.view.autorenew.Autorenew',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'autorenew',
        title: 'Autorenew',
        itemId: 'autorenew',
        controller: 'autorenew',
        viewModel:
        {
            type: 'autorenew'
        },
        requires:
        [

            'Tcitel.view.autorenew.North',
            'Tcitel.view.autorenew.Center',
            'Tcitel.view.autorenew.AutorenewController',
            'Tcitel.view.autorenew.AutorenewViewModel',
            'Tcitel.view.autorenew.AutorenewModify'
        ],
        layout: 'border',


            defaults:
            {
                xtype: 'label'
            },


        viewModel:
        {
            type: 'autorenew'
        },
        items:
        [
            {
                xtype: 'autorenewnorth',
                region: 'north'
            },
            {
                xtype: 'autorenewcenter',
                region: 'center'
            }
        ]

    }
);
