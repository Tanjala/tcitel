Ext.define
(
    'Tcitel.view.gt_routes.AddRoute',
    {
        extend: 'Ext.form.Panel',
        xtype: 'gt_routes_add',
        monitorValid: true,
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 100,
                    width:240//,
                    //allowBlank: false
                },
                items:
                [
                    {
                        name: 'gt_name',
                        fieldLabel: 'GT Name'
                    },
                    {
                        xtype: 'combo',
                        name: 'type',
                        hiddenName: 'type',
                        fieldLabel: 'Type',
                        bind: {store:'{type}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        allowBlank: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelectType'
                        }
                    },
                    {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        disabled:true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        name: 'source',
                        fieldLabel: 'Source',
                        disabled:true
                    },
                     {
                        name: 'destination',
                        disabled:true,
                        fieldLabel: 'Destination'
                    },
                     {
                        xtype: 'combo',
                        name: 'provider_id',
                        hiddenName: 'provider',
                        fieldLabel: 'Provider',
                        bind: {store:'{provider}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        disabled:true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },

                     {
                        xtype: 'combo',
                        name: 'charge_provider_id',
                        hiddenName: 'charged_provider',
                        fieldLabel: 'Charge Provider',
                        bind: {store:'{provider}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        name: 'comment',
                        fieldLabel: 'Comment',
                        xtype: 'textarea'//,
                        //allowBlank: true
                    },
                   
                    {
                        xtype: 'button',
                        text: 'Clear',
                        margin: '10 5 0 95',
                        width: 70,
                        handler: 'clear'
                    },
                    {
                        xtype: 'button',
                        text: 'Add',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'add',
                        formBind: true
                    }
                ]
            }
        ]
    }
);
