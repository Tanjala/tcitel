
Ext.define
(
    'Tcitel.view.gt_routes.North',
    {
        extend: 'Ext.form.Panel',
        xtype: 'gt_routes_search',
        items:
        [
            {
                xtype: 'fieldset',
                padding: 20,
                border: false,
                defaults:
                {
                    xtype: 'textfield',
                    labelWidth: 90,
                    width:240,
                    listeners:
                    {
                        specialkey: 'specialKey'
                    }
                },
                items:
                [
                    {
                        name: 'gt_name',
                        fieldLabel: 'GT Route Name',
                        labelWidth: 120,
                        bind: '{search.gt_name}'

                    },
                     {
                        xtype: 'combo',
                        name: 'type',
                        hiddenName: 'type',
                        fieldLabel: 'Type',
                        bind: {store:'{type_all}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    {
                        name: 'source',
                        fieldLabel: 'Source',
                        bind: '{search.source}'
                    },
                    {
                        name: 'destination',
                        fieldLabel: 'Destination',
                        bind: '{search.destination}'
                    },
                    {
                        xtype: 'combo',
                        name: 'brand',
                        hiddenName: 'brand',
                        fieldLabel: 'Brand',
                        bind: {store:'{brand_all}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },
                    
                  /*  {
                        xtype: 'combo',
                        name: 'active',
                        hiddenName: 'active',
                        fieldLabel: 'Active',
                        bind: {store:'{active}'},
                        displayField: 'display',
                        valueField: 'value',
                        forceSelection: true,
                        editable: false,
                        value: '',
                        listeners:
                        {
                            select: 'comboSelect'
                        }
                    },*/
                    {
                        xtype: 'button',
                        text: 'Reset',
                        margin: '10 5 0 95',
                        width: 70,
                        handler: 'reset'
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        margin: '10 0 0 0',
                        width: 70,
                        handler: 'search'
                    }
                ]
            }
        ]
    }
);






