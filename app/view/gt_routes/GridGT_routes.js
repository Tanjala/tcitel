
Ext.define
(
    'Tcitel.view.gt_routes.GridGT_routes',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'grid_gt_routes',
        bind: {store:'{route_gt}'},
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{route_gt}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        columns:
        {
            defaults:
            {
                flex: 1
            },
            items:
            [
               {
                    text: 'GT Route ID',
                    dataIndex: 'id',
                    sortable: true,
                    flex: false,
                    width: 90
                },
               {
                    text: 'GT Route Name',
                    dataIndex: 'gt_name',
                    sortable: true
                },
                {
                    text: 'Type',
                    dataIndex: 'type',
                    flex: false,
                    width: 80
                   
                },
                {
                    text: 'Source',
                    dataIndex: 'source'
                   
                },
                {
                    text: 'Destination',
                    dataIndex: 'destination'
                   
                },
                {
                    text: 'Provider',
                    dataIndex: 'provider'
                   
                },
                {
                    text: 'Brand',
                    dataIndex: 'brand_name'
                },
                {
                    text: 'Charge Provider',
                    dataIndex: 'charge_provider'
                   
                },
                {
                    text: 'Comment',
                    dataIndex: 'comment',
                    flex: false,
                    width: 200
                 
                   
                },
               
                 {
                    text: 'Actions',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 25
                        },
                        items:
                        [

                            {
                                html:'<i class="fa fa-pencil"></i>',
                                tooltip: 'Edit',
                                handler: 'edit'
                            },
                            {

                                html:'<i class="fa fa-trash-o"></i>',
                                tooltip: 'Delete',
                                handler: 'delete'
                            }
                        ]
                    }
                }
            ]
        }
    }
);
