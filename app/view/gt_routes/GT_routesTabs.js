Ext.define
(
    'Tcitel.view.gt_routes.GT_routesTabs',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'gt_routes-tabs',
        //controller: 'gt_routes',
        plain: true,
        items:
        [
            {
                title: 'GT Route Search',
                tabConfig:
                {
                    tooltip: 'Search'
                },
                xtype: 'gt_routes_search'
            },
            {
                title: 'Add GT Route',
                tabConfig:
                {
                    tooltip: 'Create'
                },
                xtype: 'gt_routes_add'
            }
        ]
    }
);