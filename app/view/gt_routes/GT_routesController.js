Ext.define('Tcitel.view.gt_routes.GT_routesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.gt_routes',

     comboSelect: function(combo, record, eOpts)
        {
            this.getViewModel().get('search')[combo.name] = combo.value;
        },
        comboSelectType: function(combo, record, eOpts)
        {
            var value =record.data.value;
            this.getViewModel().get('search')[combo.name] = combo.value;
            source=combo.up('gt_routes_add').down('field[name=source]');
            destination=combo.up('gt_routes_add').down('field[name=destination]');
            provider=combo.up('gt_routes_add').down('combo[name=provider_id]');
            brand=combo.up('gt_routes_add').down('combo[name=brand]');
            console.log(source);
            //fieldNumber= field.up('billingnorth').down('field[name=source]'),
            if (value==1)
            {
               source.enable();
               destination.disable();
               provider.disable();
               brand.disable();

            }else if(value==2)
            {
               source.enable();
               destination.enable();
               provider.disable();
               brand.disable();
            }
            else if(value==3)
            {
               provider.enable();
               destination.enable();
               source.disable();
               brand.disable();
            }
             else if(value==4)
            {
               destination.enable();
               source.disable();
               provider.disable();
               brand.disable();
            }
            else if(value==5)
            {
               brand.enable();
               destination.disable();
               provider.disable();
               source.disable();
            }
            else if(value==99)
            {
               brand.disable();
               destination.disable();
               provider.disable();
               source.disable();
            }


        },
        filterStore: function(store, filter)
        {
            store.clearFilter(true);
            store.addFilter(filter);
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                this.search();
            }
        },

         search: function(button, e)
        {
            var search = this.getViewModel().get('search'),
            filter = [];
            Ext.Object.each
            (
                search,
                function(key, value, myself)
                {
                    filter.push({property: key, value: value});
                }
            );
            this.filterStore(this.getStore('route_gt'), filter);
        },

        clear: function(button, e)
        {
            button.up('form').getForm().reset();
            button.up('gt_routes_add').down('field[name=source]').disable();
            button.up('gt_routes_add').down('field[name=destination]').disable();
            button.up('gt_routes_add').down('combo[name=provider_id]').disable();
            button.up('gt_routes_add').down('combo[name=brand]').disable();
        },

        reset: function(button, e)
        {
            this.getViewModel().set
            (
                'search',
                {
                    gt_name: '',
                    source: '',
                    destination: '',
                    brand: '',
                    type:''   
                }
            );
            button.up('fieldset').down('combo').reset();
            button.up('form').getForm().reset();
            this.search();
        },
          edit: function(button, e)
        {
            var record = button.up('container').getWidgetRecord();
            var routeedit = Ext.widget('gt_route_edit');
            routeedit.loadRecord(record);
            this.getView().add(routeedit);
            routeedit.show();
        }
        ,
        add: function(button, e)
        {
            var form = button.up('form'),
            values = form.getValues(),
            store = this.getStore('route_gt'),
            route = Ext.create
            (
                'Tcitel.model.gt_routes.GT_routes',
                values
            );
            //delete route.id;
           // delete route.data.id;
            route.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        var response = Ext.decode(operation._response.responseText);
                        if (success)
                        {
                           if (response.message)
                           {
                                Ext.Msg.alert('Success with error!', response.message);
                           }
                           else
                           {
                                Ext.Msg.alert('Success!', 'Route is added.');
                           }
                           form.reset();
                           store.reload();
                        }
                        else
                        {
                            //var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );

        },



        save: function(button, e, eOpts)
        {
            var form = button.up('form'),
            store = this.getStore('route'),
            route = form.getRecord();
            form.updateRecord();
            form.setLoading(true);
            route.save
            (
                {
                    params :
                    {
                        route: route.data.route
                    },
                    callback : function(records, operation, success)
                    {
                        if (success)
                        {
                            form.setLoading(false);
                            form.close();
                            Ext.Msg.alert('Success!', 'Route data is saved.');
                            store.reload();
                        }
                        else
                        {
                            form.setLoading(false);
                            var response = Ext.decode(operation._response.responseText);
                            Ext.Msg.alert('Failure!', response.message);
                        }
                    }
                }
            );
        },
        
        modify: function(button, e, eOpts)
        {
            var form = button.up('form'),
            route = form.getRecord(),
            store = this.getStore('route_gt')
           // route.data.ip = ' ' + route.data.ip + ' ';
            form.updateRecord(route);
            route.save
            (
                {
                    callback : function(records, operation, success)
                    {
                        var response = Ext.decode(operation._response.responseText);

                         if (success)
                        {
                           if (response.message)
                           {
                                Ext.Msg.alert('Success with error!', response.message);
                           }
                           else
                           {
                                Ext.Msg.alert('Success!', 'Route is changed.');
                           }
                           form.close();
                           store.reload();
                        }
                        else
                        {
                            Ext.Msg.alert('Failure!', response.message);
                            store.reload();
                        }
                    }
                }
            );
        },
         delete: function(button)
        {
            var route_gt = button.up('container').getWidgetRecord(),
            store = this.getStore('route_gt');
            Ext.Msg.confirm
            (
                'Delete Route?',
                route_gt.data.gt_name,
                function(btn)
                {
                    if (btn === 'yes')
                    {
                        Ext.getBody().mask();
                        route_gt.erase
                        (
                            {
                                callback : function(records, operation, success)
                                {
                                    Ext.getBody().unmask();
                                    if (success)
                                    {
                                        store.reload();
                                        Ext.Msg.alert('Success!', 'Route is deleted.');
                                    }
                                    else
                                    {
                                        var response = Ext.decode(operation._response.responseText);
                                        Ext.Msg.alert('Failure!', response.message);
                                    }
                                }
                            }
                        );
                    }
                }
            )
        }


});