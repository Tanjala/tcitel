Ext.define
(
    'Tcitel.view.gt_routes.GT_routesViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.gt_routes',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.gt_routes.GT_routes'
        ],
        data:
        {
            search:
            {
                
                gt_name: '',
                source: '',
                destination: '',
                brand: '',
                type: ''        
                
            }
        },
       stores:
        {
            route_gt:
            {
                model: 'Tcitel.model.gt_routes.GT_routes',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/gt_routes/gt_route_read.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                /*sorters: 
                {
                    property: 'name',
                    direction: 'ASC'
                },*/
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
             brand_all:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name',
                        all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             brand:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_brand',
                        valueField: 'name',
                        displayField: 'name'//,
                       // all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             provider:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_routes',
                        valueField: 'route',
                        displayField: 'name'//,
                       // all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
             charged_provider:
            {
                model: 'Tcitel.model.Combo',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/common/distinctfield.php',
                    extraParams:
                    {
                        table: 'vs_routes',
                        valueField: 'name',
                        displayField: 'name'//,
                       // all: true
                    },
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                autoLoad: true
            },
            type:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'},
                    {name:'active'}
                ],
                data:
                [
                    {display: '1', value:'1', active: 'source' },
                    {display: '2', value:'2', active: 'source_dest'},
                    {display: '3', value:'3', active: 'provider_dest'},
                    {display: '4', value:'4', active: 'destination'},
                    {display: '5', value:'5', active: 'brand'},
                    {display: '99', value:'99', active: 'default'}
                ]
            },
             type_all:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: '1', value:'1'},
                    {display: '2', value:'2'},
                    {display: '3', value:'3'},
                    {display: '4', value:'4'},
                    {display: '5', value:'5'},
                    {display: '99', value:'99'}
                ]
            }

            /*direction:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'Inbound', value:'inbound'},
                    {display: 'Outbound', value:'outbound'}
                ]
            },
            active:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'TRUE', value:'TRUE'},
                    {display: 'FALSE', value:'FALSE'}
                ]
            }*/
          
        }
    }
);
