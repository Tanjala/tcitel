

Ext.define
(
    'Tcitel.view.gt_routes.GT_routes',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'gt_routes',
        title:'GT Routes',
        itemId: 'gt_routes',
        controller: 'gt_routes',
        viewModel:
        {
            type: 'gt_routes'
        },
        requires:
        [
            'Tcitel.view.gt_routes.North',
            'Tcitel.view.gt_routes.GridGT_routes',
            'Tcitel.view.gt_routes.AddRoute',
            'Tcitel.view.gt_routes.GT_routesTabs',
            'Tcitel.view.gt_routes.GT_routesController',
            'Tcitel.view.gt_routes.GT_routesViewModel'//,
            //'Tcitel.view.gt_routes.EditRoute'
        ],
        layout:
        {
            type: 'hbox',
            align: 'stretch'
        },
        defaults:
        {
            xtype: 'panel',
            defaults:
            {
                border: true,
                margin: 3
            }
        },
        items:
        [
            {
                width: 300,
                items:
                [
                    {
                        xtype: 'gt_routes-tabs',
                        title:'GT Routes'
                    }
                ]
            },
            {
                flex: 1,
                layout:
                {
                    type: 'fit'
                },
                items:
                [
                    {
                        title : 'Data',
                        xtype: 'grid_gt_routes'

                    }
                ]
            }
        ]
    }
);



