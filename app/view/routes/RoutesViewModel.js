Ext.define
(
    'Tcitel.view.routes.RoutesViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.routes',
        requires:
        [
            'Tcitel.model.Combo',
            'Tcitel.model.routes.Routes'
        ],
        data:
        {
            search:
            {
                
                name: '',
                host: '',
                direction: '',
                active: ''        
                
            }
        },
       stores:
        {
            route:
            {
                model: 'Tcitel.model.routes.Routes',
                proxy:
                {
                    type: 'ajax',
                    url: 'php/routes/readroutes.php',
                    reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    }
                },
                sorters: 
                {
                    property: 'name',
                    direction: 'ASC'
                },
                remoteFilter: true,
                remoteSort: true,
                autoLoad: true
            },
            direction:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'Inbound', value:'inbound'},
                    {display: 'Outbound', value:'outbound'}
                ]
            },
            active:
            {
                fields:
                [
                    {name: 'display'},
                    {name: 'value'}
                ],
                data:
                [
                    {display: 'ALL', value:''},
                    {display: 'TRUE', value:'TRUE'},
                    {display: 'FALSE', value:'FALSE'}
                ]
            }
          
        }
    }
);
