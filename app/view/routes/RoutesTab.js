Ext.define
(
    'Tcitel.view.routes.RoutesTabs',
    {
        extend: 'Ext.tab.Panel',
        xtype: 'routes-tabs',
        //controller: 'routes',
        plain: true,
        items:
        [
            {
                title: 'Routes Search',
                tabConfig:
                {
                    tooltip: 'Search'
                },
                xtype: 'routessearch'
            },
            {
                title: 'Add Route',
                tabConfig:
                {
                    tooltip: 'Create'
                },
                xtype: 'routesadd'
            }
        ]
    }
);