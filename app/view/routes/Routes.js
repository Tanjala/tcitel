

Ext.define
(
    'Tcitel.view.routes.Routes',
    {
        extend: 'Ext.panel.Panel',
        xtype: 'routes',
        title:'Routes',
        itemId: 'routes',
        controller: 'routes',
        viewModel:
        {
            type: 'routes'
        },
        requires:
        [
            'Tcitel.view.routes.North',
            'Tcitel.view.routes.GridRoutes',
            'Tcitel.view.routes.AddRoute',
            'Tcitel.view.routes.RoutesTabs',
            'Tcitel.view.routes.RoutesController',
            'Tcitel.view.routes.RoutesViewModel'//,
            //'Tcitel.view.routes.EditRoute'
        ],
        layout:
        {
            type: 'hbox',
            align: 'stretch'
        },
        defaults:
        {
            xtype: 'panel',
            defaults:
            {
                border: true,
                margin: 3
            }
        },
        items:
        [
            {
                width: 300,
                items:
                [
                    {
                        xtype: 'routes-tabs',
                        title:'Routes'
                    }
                ]
            },
            {
                flex: 1,
                layout:
                {
                    type: 'fit'
                },
                items:
                [
                    {
                        title : 'Data',
                        xtype: 'gridroutes'
                    }
                ]
            }
        ]
    }
);



