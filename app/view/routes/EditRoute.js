Ext.define
(
    'Tcitel.view.routes.EditRoute',
    {
        extend: 'Ext.form.Panel',
        xtype: 'routeedit',
        title: 'Edit Route',
        floating: true,
        draggable: true,
        closable: false,
        modal: true,
        autoShow: false,
        bodyPadding: 20,
        frame: true,
        monitorValid: true,
        defaults:
        {
            xtype: 'textfield',
            allowBlank: false
        },
        initComponent: function()
        {
            this.items =
            [
                {
                    xtype: 'hidden',
                    name: 'route'
                },
                {
                    name: 'name',
                    fieldLabel: 'Route Name'
                },
                 {
                    name: 'host',
                    fieldLabel: 'Host',
                    allowBlank: true
                },
                {
                    name: 'ip',
                    xtype:'textarea',
                    emptyText: 'line break separated',
                    fieldLabel: 'IP',
                    allowBlank: true
                },
                {
                    xtype:'checkboxfield',
                    name: 'active',
                    fieldLabel: 'Activation',
                    inputValue: 't',
                    uncheckedValue:'f',
                    labelWidth: 104,
                    boxLabel  : 'Active'
                },
                {             
                    xtype: 'checkboxgroup',
                    fieldLabel: 'Call Direction',
                    allowBlank: false,
                    items: 
                    [
                       {
                            xtype: 'checkboxfield',
                            boxLabel  : 'Inbound',
                            name: 'inbound',
                            inputValue: 't',
                            uncheckedValue:'f',
                            margin:'0 6 0 0'

                        },
                        {
                            xtype: 'checkboxfield',
                            name:'outbound',
                            inputValue: 't',
                            uncheckedValue:'f',
                            boxLabel  : 'Outbound'

                        }
                    ]

                }    
                    
            ];
            this.buttons=
            [
                {
                    text: 'Cancel',
                    handler: 'cancel',
                    scope: this,
                    handler: this.close
                },
                {
                    text: 'Save',
                    handler: 'modify',
                    formBind: true
                }
            ];
            this.callParent(arguments);
        }
    }
);
