
Ext.define
(
    'Tcitel.view.routes.GridRoutes',
    {
        extend: 'Ext.grid.Panel',
        xtype: 'gridroutes',
        bind: {store:'{route}'},
        dockedItems:
        [
            {
                xtype: 'pagingtoolbar',
                bind: {store:'{route}'},
                dock: 'bottom',
                displayInfo: true
            }
        ],
        columns:
        {
            defaults:
            {
                flex: 1
            },
            items:
            [
               {
                    text: 'Route ID',
                    dataIndex: 'route',
                    sortable: true,
                    flex: false,
                    width: 90
                },
               {
                    text: 'Route Name',
                    dataIndex: 'name',
                    sortable: true
                },
                {
                    text: 'Host',
                    dataIndex: 'host'
                   
                },
                {
                    text: 'Outbound',
                    dataIndex: 'outbound',
                    renderer: function (value)
                    {
                        return value == 't' ? 'TRUE' : 'FALSE';
                    }
                   
                },
                {
                    text: 'Inbound',
                    dataIndex: 'inbound',
                    renderer: function (value)
                    {
                       return value == 't' ? 'TRUE' : 'FALSE';
                    }
                },
                {
                    text: 'IP',
                    dataIndex: 'ip',
                    renderer: function(value)
                    {
                        return value.replace( new RegExp('\n', 'g'),"<br>");
                    }
                   
                },
                {
                    text: 'Active',
                    dataIndex: 'active',
                    renderer: function (value)
                    {
                        return value == 't' ? 'TRUE' : 'FALSE';
                    }
                },
                 {
                    text: 'Actions',
                    stopSelection: true,
                    xtype: 'widgetcolumn',
                    flex: false,
                    width: 90,
                    widget:
                    {
                        xtype: 'container',
                        defaults:
                        {
                            xtype: 'button',
                            margin: 5,
                            width: 25
                        },
                        items:
                        [

                            {
                                html:'<i class="fa fa-pencil"></i>',
                                tooltip: 'Edit',
                                handler: 'edit'
                            }
                        ]
                    }
                }
            ]
        }
    }
);
