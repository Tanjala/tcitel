Ext.define
(
    'Tcitel.view.login.Login',
    {
        extend: 'Ext.form.Panel',
        title: 'Login',
        controller: 'login',
        xtype: 'login',
        floating:true,
        modal: true,
        bodyPadding: 20,
        frame: true,
        monitorValid: true,
        requires:
        [
            'Tcitel.view.login.LoginController'
        ],
        defaults:
        {
            xtype: 'textfield',
            allowBlank: false,
            listeners:
            {
                specialKey: 'specialKey',
                afterrender: 'focusUsername'
            }
        },
        items:
        [
            {
                name: 'username',
                fieldLabel: 'Username'
            },
            {
                name: 'password',
                inputType: 'password',
                fieldLabel: 'Password'
            }
        ],
        buttons:
        [
            {
                xtype: 'button',
                text: 'Login',
                itemId: 'login',
                formBind: true,
                listeners: {
                    click: 'doLogin'
                }
            }
        ]
    }
);