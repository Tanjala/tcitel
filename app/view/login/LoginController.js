Ext.define
(
    'Tcitel.view.login.LoginController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.login',

        focusUsername: function(field)
        {
            if (field.name == 'username') field.focus();
        },

        specialKey: function(field, e)
        {
            if(e.getKey() == e.ENTER)
            {
                if (field.name != 'password')
                {
                    field.nextSibling().focus(false,true);
                }
                else
                {
                    this.doLogin( field, e) ;
                }
            }
        },

        doLogin: function( component, e )
        {
            var me = this,
            form = component.up( 'form' ),
            values = form.getValues();
            if (!form.getForm().isValid()) return;
            Ext.Ajax.request
            (
                {
                    url: 'php/login/login.php',
                    params:
                    {
                        username: values.username,
                        password: values.password
                    },
                    success: function( response, options )
                    {
                        var result = Ext.decode( response.responseText );
                        if( result.success )
                        {
                            Tcitel.LoggedInUser = Ext.create( 'Tcitel.model.login.User', result.data );
                            Ext.globalEvents.fireEvent('aftervalidateloggedin');

                            /*var mb = Ext.Msg.show
                            (
                                {
                                    title:'Welcome',
                                    msg: 'You have successfully logged in to Tcitel!',
                                    icon: Ext.Msg.INFO,
                                    buttons: Ext.MessageBox.OK,
                                    closable: false,
                                    fn:function(btn) { } // singleton
                                }
                            );

                            setTimeout
                            (
                                function() { mb.close(); },
                                2000
                            );*/

                            form.close();
                        }
                        else
                        {
                            Ext.Msg.alert( 'Loggin failed', 'Wrong username / password' );
                            form.getForm().reset();
                        }
                    },
                    failure: function( response, options )
                    {
                        Ext.Msg.alert( 'Loggin failed', 'An error occurred during your request. Please try again.');
                        form.getForm().reset();
                    }
                }
            );
        }
    }
);
