Ext.define
(

    'Tcitel.model.gt_routes.GT_routes',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'gt_name'},
            { name: 'source'},
            { name: 'id'},
            { name: 'destination'},
            { name: 'type'},
            { name: 'brand'}
         
        ],
        idProperty: 'route_gt',
        proxy:
        {
            type: 'ajax',
            api:
            {
               create: 'php/gt_routes/gt_route_add.php',
               destroy: 'php/gt_routes/destroy_gt_route.php'//,
               // update: 'php/routes/updateroute.php'
            },
            reader:
            {
                type: 'json',
                rootProperty: 'data'
            },
            writer:
            {
                type: 'json',
                rootProperty: 'record',
                encode: true
            }
        }
    }
);
