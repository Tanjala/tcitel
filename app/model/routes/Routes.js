Ext.define
(
    'Tcitel.model.routes.Routes',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'name'},
            { name: 'host'},
            { name: 'outbound'},
            { name: 'inbound'},
            { name: 'route'},
            { name: 'ip'},
            { name: 'active'}
         
        ],
        idProperty: 'route',
        proxy:
        {
            type: 'ajax',
            api:
            {
                create: 'php/routes/addroute.php',
                update: 'php/routes/updateroute.php'
            },
            reader:
            {
                type: 'json',
                rootProperty: 'data'
            },
            writer:
            {
                type: 'json',
                rootProperty: 'record',
                encode: true
            }
        }
    }
);
