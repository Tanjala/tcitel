Ext.define
(
    'Tcitel.model.autorenew.Autorenew',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'user_id'},
            { name: 'billing_id'},
            { name: 'number'},
            { name: 'type'},
            { name: 'provider'},
            { name: 'transaction_time'},
            { name: 'amount'},
            { name: 'status'}
        ],
         proxy:
        {
            type: 'ajax',
            api:
            {
                //read: 'php/providers/read.php',
                create: 'php/autorenew/create.php',
                update: 'php/autorenew/update.php'
               // destroy: 'php/autorenew/destroy.php'
            },
            reader:
            {
                type: 'json',
                rootProperty: 'data'
            },
            writer:
            {
                type: 'json',
                rootProperty: 'record',
                encode: true
            }
        }
    }
);
