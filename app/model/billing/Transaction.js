Ext.define(

'Tcitel.model.billing.Transaction',
 {
	extend: 'Ext.data.Model',

	fields:
	 [ 
		    { name: 'account_id'},
            { name: 'billing_profile_id'},
            { name: 'committed'},
            { name: 'result'},
            { name: 'brand'},
            { name: 'name'},
            { name: 'expires_at'},
            { name: 'created_at'},
            { name: 'meta_data'},
            { name: 'type'},
            { name: 'transaction_id'}
		
	 ]

}); 