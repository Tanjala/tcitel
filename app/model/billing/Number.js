Ext.define
(
    'Tcitel.model.billing.Number',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'user_id'},
            { name: 'number'},
            { name: 'status'},
            { name: 'type'},
            { name: 'expiration_date'},
            { name: 'auto_renew', type: 'bool'},
            { name: 'created'},
            { name: 'id'}
        ],
        idProperty: 'number'/*,
        proxy:
        {
            type: 'ajax',
            url: 'app/model/billing/number.php',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }*/
    }
);
