Ext.define
(
    'Tcitel.model.billing.User',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'user_id'},
            {name: 'email'},
            {name: 'created'},
            {name: 'active'},
            {name: 'user_type'},
            {name: 'brand'},
            {name: 'balances'},
            {name: 'reservations'},
            {name: 'billing_id'}
        ]/*,
        proxy:
        {
            type : 'ajax',
            url  : 'app/model/billing/read.php',
            extraParams: {
                //id: '{id}'
            },
            api:
            {
                //create  : 'ajax/swi/create.php',
                read: 'read.php'//,
                //destroy : 'ajax/swi/delete.php',
                //update: 'ajax/swi/cc_update.php'
            },
            reader: {
                type            : 'json',
                rootProperty    : 'data'//,
                //totalProperty   : 'total'
            },
            writer:
            {
                root: 'record',
                encode: true
            }
        }
        ,
        proxy:
        {
            type: 'ajax',
            api:
            {
                //create  : 'ajax/swi/create.php',
                read: 'read.php'//,
                //destroy : 'ajax/swi/delete.php',
                //update: 'ajax/swi/cc_update.php'
            },
            reader:
            {
                type: 'json',
                root: 'data'
            },
            writer:
            {
                root: 'record',
                encode: true
            }
        }*/
    }
);
