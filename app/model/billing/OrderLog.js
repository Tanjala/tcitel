Ext.define
(
    'Tcitel.model.billing.OrderLog',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'timestamp'},
            { name: 'user_id'},
            { name: 'billing_id'},
            { name: 'number'},
            { name: 'type'},
            { name: 'credit'}
        ]
    }
);
