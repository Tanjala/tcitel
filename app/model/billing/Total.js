Ext.define
(
    'Tcitel.model.billing.Total',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'GooglePlay'},
            { name: 'PayPal'},
            { name: 'PayPalWeb'},
            { name: 'Bazaar'},
            { name: 'iTunes'},
            { name: 'guitotal'},
            { name: 'totalpayment'}
        ]
        //idProperty: 'number',
        /*proxy:
        {
            type: 'ajax',
            url: 'app/model/billing/number.php',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }*/
    }
);