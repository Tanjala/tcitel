Ext.define
(
    'Tcitel.model.billing.Devices',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'user_id'},
            { name: 'platform'},
            { name: 'platform_data'},
            { name: 'app_name'},
            { name: 'app_version'},
            { name: 'nc_prefix'},
            { name: 'created'},
            { name: 'registered', type: 'bool'}
        ]
        
    }
);
