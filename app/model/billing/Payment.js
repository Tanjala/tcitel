Ext.define
(
    'Tcitel.model.billing.Payment',
    {
        extend: 'Tcitel.model.Base',
        //idProperty: 'id',
        fields:
        [
            { name: 'id'},
            { name: 'transaction_id'},
            { name: 'account_id'},
            { name: 'billing_profile_id'},
            { name: 'committed', type: 'float'},
            { name: 'result'},
            { name: 'brand'},
            { name: 'name'},
            { name: 'expires_at'},
            { name: 'created_at'},
            { name: 'meta_data'},
            { name: 'type'}
        ]//,
       /* hasMany: 
        {
            model: 'Tcitel.model.billing.Transaction', 
            name: 'detail',
            foreignKey: 'transaction_id'
        }*/
        /*,
    
      proxy: 
      {
        type: 'direct',
       directFn: 'areService.readWithTransaction'
      }*/

       
    }
);