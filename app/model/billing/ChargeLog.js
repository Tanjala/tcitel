Ext.define
(
    'Tcitel.model.billing.ChargeLog',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'transaction_id'},
            { name: 'auth_id'},
            //{ name: 'account_id'},
            { name: 'billing_profile_id'},
            { name: 'committed'},
            { name: 'brand'},
            { name: 'rate'},
            { name: 'multiply'},
            { name: 'amount'},
            { name: 'result'},
            { name: 'result_name'},
            { name: 'name'},
            { name: 'id'},
            { name: 'meta_data'},
            { name: 'created_at'},
            { name: 'expires_at'}
        ],
        idProperty: 'transaction_id'
    }
);
