Ext.define
(
    'Tcitel.model.stat.Stat',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'src'},
            { name: 'dst'},
            { name: 'clid'},
            { name: 'brand'},
            { name: 'log_start'},
            { name: 'log_end'},
            { name: 'route'},
            { name: 'call_type'}
        ]
    }
);