Ext.define
(
    'Tcitel.model.cdrlog.CdrLog',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'accountcode'},
            {name: 'src'},
            {name: 'dst'},
            {name: 'dcontext'},
            {name: 'clid'},
            {name: 'channel'},
            {name: 'dstchannel'},
            {name: 'lastapp'},
            {name: 'lastdata'},
            {name: 'start'},
            {name: 'answer'},
            {name: 'end'},
            {name: 'duration'},
            {name: 'billsec'},
            {name: 'disposition'},
            {name: 'amaflags'},
            {name: 'userfield'},
            {name: 'uniqueid'},
            {name: 'linkedid'},
            {name: 'peeraccount'},
            {name: 'sequence'},
            {name: 'route'},
            {name: 'name'},
            {name: 'call_type'},
            {name: 'brand'}
        ]
    }
);