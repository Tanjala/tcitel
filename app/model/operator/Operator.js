Ext.define
(
    'Tcitel.model.operator.Operator',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'username'},
            { name: 'password'},
            { name: 'firstname'},
            { name: 'lastname'},
            { name: 'role'},
            { name: 'email'},
            { name: 'phone'},
            { name: 'brand'}
        ],
        proxy:
        {
            type: 'ajax',
            api:
            {
                //read: 'php/providers/read.php',
                create: 'php/operator/create.php',
                update: 'php/operator/update.php',
                destroy: 'php/operator/destroy.php'
            },
            reader:
            {
                type: 'json',
                rootProperty: 'data'
            },
            writer:
            {
                type: 'json',
                rootProperty: 'record',
                encode: true
            }
        }
    }
);
