Ext.define
(
    'Tcitel.model.rates.Rates',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'destination'},
            { name: 'prefix'},
            { name: 'iso2'},
            { name: 'route'},
            { name: 'perminutecost'},
            { name: 'persmscost'},
            { name: 'brand'}
         
        ],
        proxy:
        {
            type: 'ajax',
           /* api:
            {
                create: 'php/routes/addrate.php',
                update: 'php/routes/updaterate.php'
            },*/
            reader:
            {
                type: 'json',
                rootProperty: 'data'
            },
            writer:
            {
                type: 'json',
                rootProperty: 'record',
                encode: true
            }
        }
    }
);
