Ext.define
(
    'Tcitel.model.Combo',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'value'},
            { name: 'display'}
        ]
    }
);
