Ext.define
(
    'Tcitel.model.messagelog.MessageLog',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'to'},
            {name: 'from'},
            {name: 'text'},
            {name: 'app_state'},
            {name: 'lock_value'},
            {name: 'received_at'},
            {name: 'notified_at'}
        ]
    }
);