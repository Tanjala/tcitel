Ext.define
(
    'Tcitel.model.orders.Orders',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'user_id'},
            { name: 'payment_status'},
            { name: 'payment_data'},
            { name: 'payment_type'},
            { name: 'transaction_update'},
            { name: 'transaction_started'}
        ]
    }
);