Ext.define
(
    'Tcitel.model.messagestat.Hourly',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'id'},
            {name: 'day'},
            {name: 'hour'},
            {name: 'state'},
            {name: 'brand'},
            {name: 'direction'},
            {name: 'counter'}        ]
    }
);