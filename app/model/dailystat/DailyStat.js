Ext.define
(
    'Tcitel.model.dailystat.DailyStat',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'id'},
            { name: 'call_a'},
            { name: 'call_b'},
            { name: 'alias'},
            { name: 'dst'},
            { name: 'log_start'},
            { name: 'log_end'},
            { name: 'lastapp'},
            { name: 'disposition'}
        ]
    }
);