Ext.define
(
    'Tcitel.model.spofstat.Daily',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'id'},
            {name: 'dan'},
            //{name: 'hour'},
            {name: 'expired'},
            {name: 'type'},
            {name: 'brand'},
            {name: 'counter'}        ]
    }
);