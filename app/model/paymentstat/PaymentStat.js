Ext.define
(
    'Tcitel.model.paymentstat.PaymentStat',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {name: 'id'},
            {name: 'dan'},
            {name: 'sat'},
            {name: 'transaction_counter'},
            {name: 'transaction_type_id'},
            {name: 'brand'},
            {name: 'payment_sum'}        ]
    }
);