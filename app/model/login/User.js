/**
 * {@link Ext.data.Model} for Security User
 */
Ext.define
(
    'Tcitel.model.login.User',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            {
                name: 'firstname',
                type: 'string',
                persist: false
            },
            {
                name: 'lastname',
                type: 'string',
                persist: false
            },
            {
                name: 'username',
                type: 'string',
                persist: false
            },
            {
                name: 'brand',
                type: 'string',
                persist: false
            },
            {
                name: 'roles',
                defaultValue: [],
                convert: function(data, model)
                {
                    return ( data && Ext.isArray(data) ) ? data : [data];
                },
                persist: false
            }
        ],
        inRole: function( roles )
        {
            var me = this;
            return Ext.isArray(roles) ? Ext.Array.intersect(me.get( 'roles' ), roles) : Ext.Array.contains( me.get( 'roles' ), roles );
        }
    }
);
