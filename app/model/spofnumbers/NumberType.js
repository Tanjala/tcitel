Ext.define
(
    'Tcitel.model.spofnumbers.NumberType',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'number'},
            { name: 'user_id'},
            { name: 'created_date'},
            { name: 'type'},
            { name: 'provider'},
            { name: 'expire_date'},
            { name: 'brand'},
            { name: 'auto_renew'}
        ],
        idProperty: 'number'
    }
);
