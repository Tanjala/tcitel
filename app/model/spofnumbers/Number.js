Ext.define
(
    'Tcitel.model.spofnumbers.Number',
    {
        extend: 'Tcitel.model.Base',
        fields:
        [
            { name: 'number'},
            { name: 'special_offer_regions_id'},
            { name: 'reserved'},
            { name: 'type'},
            { name: 'provider'},
            { name: 'price_id'},
            { name: 'brand'},
            { name: 'quarantine'},
            { name: 'special_offer_cities_id'}
        ],
        idProperty: 'number'
    }
);
