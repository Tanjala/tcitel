/**
 * The main application class. An instance of this class is created by `app.js` when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define
(
    'Tcitel.Application',
    {
        extend: 'Ext.app.Application',
        name: 'Tcitel',
        controllers:
        [
            'Tcitel.controller.login.Login',
            'Tcitel.controller.common.Common'
        ],
        models:
        [
            //'Tcitel.model.billing.Number'
        ],
        stores:
        [
            // TODO: add global / shared stores here
        ],
        views:
        [
        ],
        launch: function ()
        {
            Ext.globalEvents.fireEvent('beforeviewportrender');
        }
    }
);
