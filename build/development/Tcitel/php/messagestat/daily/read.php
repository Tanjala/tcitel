<?php

require_once("../../../lib/php/common.php");

class Row
{
	var $day;
	var $DELIVERED;
	var $UNDELIVERED;

	function Row($day)
	{
		$this->day = $day;
		$this->DELIVERED = 0;
		$this->UNDELIVERED = 0;

	}
}

function dateRange( $first, $last, $step = '+1 day', $format = 'm-d' ) {

	$dates = array();
	$current = strtotime( $first );
	$last = strtotime( $last );

	while( $current <= $last ) {

		$dates[] = date( $format, $current );
		$current = strtotime( $step, $current );
	}

	return $dates;
}


$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND brand = '$brand_access' " : " WHERE true ";


$startday = date('Y-m-d', strtotime("-30 days"));
$endday = date('Y-m-d');
$brand = '';
$direction = '';
$route = '';

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

//$call_type = $DB->escape($_REQUEST['call_type']);

// $call_type = 'app2app';

$where .= " AND day >= '$startday' AND day <= '$endday' ";
$where .= "AND direction NOT ILIKE 'NA%'";
if ($brand) $where .= " AND brand = '$brand' ";
if ($direction) $where .= " AND direction = '$direction' ";
if ($route) $where .= " AND route = '$route' ";

/*$query = "  SELECT 	DATE_FORMAT(day,'%m-%d') day,
					SUM(CASE WHEN disposition = 'ANSWERED' THEN counter ELSE 0 END) ANSWERED,
					SUM(CASE WHEN disposition = 'BUSY' THEN counter ELSE 0 END) BUSY,
					SUM(CASE WHEN disposition = 'NO ANSWER' THEN counter ELSE 0 END) 'NO ANSWER',
					SUM(CASE WHEN disposition = 'FAILED' THEN counter ELSE 0 END) 'FAILED'
            FROM stat
            WHERE day >= '$startday' AND day <= '$endday' GROUP BY `day`";*/

$query = "SELECT
	to_char(day, 'MM-DD') AS day,

	SUM (CASE
	WHEN state = 'DELIVERED' THEN counter
	ELSE 0
	END)
	AS \"DELIVERED\",
	SUM (CASE
	WHEN state = 'SENT' THEN counter
	ELSE 0
	END)
	AS \"SENT\"

FROM mg_stat
	$where
GROUP BY day ORDER BY day";

$DB->query($query);

$result = array();

foreach (dateRange($startday, $endday) as $date )
{
	$result[$date] = new Row($date);
};

while($obj = $DB->fetch_object())
{
	$obj->UNDELIVERED =  $obj->SENT-$obj->DELIVERED;
	$result[$obj->day] = $obj;
}

$result = array_values ($result);

$response = array('data' => $result);
$response['sql'] = preg_replace('/\s\s+/', ' ', $query);

echo json_encode($response);
