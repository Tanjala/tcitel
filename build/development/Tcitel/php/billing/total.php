<?php

require_once("../../lib/php/common.php");


$log_start = date('Y-m-d 00:00:00');
$log_end = date('Y-m-d 23:59:59');
$brand = '';

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND t.brand = '$brand_access' " : " WHERE true ";

$log_start = str_replace('T', ' ', $log_start);

$log_end = str_replace('T', ' ', $log_end);


$where .= " AND created_at >= '$log_start' AND created_at <= '$log_end' ";

if ($brand)
{
	$where .= " AND t.brand = '$brand' ";
}


$query=" SELECT SUM(t.committed) zbir, p.name, t.brand FROM b_transaction t JOIN b_transaction_detail d ON d.transaction_id = t.id JOIN b_billing_profile p ON t.billing_profile_id = p.id $where GROUP BY p.name, t.brand";



$DB->query($query);

$arr = array();

$arr['PayPal']= $arr['PayPalWeb'] = $arr['GooglePlay'] = $arr['iTunes'] = $arr['gui']= $arr['TransferCredit']= $arr['registration']=$arr['messaging']=$arr['voice']=$arr['DIDWW']=$arr['maintanance']=$arr['SPOfferOrder']=$arr['Bazaar']=0;

while($obj = $DB->fetch_object())
{
	$arr[$obj->name]+=$obj->zbir/-100000;

}

$arr['totalpayment'] = $arr['PayPal'] +  $arr['PayPalWeb'] +  $arr['GooglePlay'] +  $arr['iTunes']+  $arr['Bazaar'];
$arr['messaging'] = round($arr['messaging']*-1,3);
$arr['voice'] = round($arr['voice']*-1/2,3);
$arr['DIDWW'] = round($arr['DIDWW']*-1/2,3);


$response = array();
$response['query'] = $query;
$response['data'] = $arr;

echo json_encode($response);
