Ext.define
(
	'Ext.ux.panel.MenuCardTab',
	{
		extend: 'Ext.panel.Panel',
		xtype: 'menucardtab',

	    initComponent: function()
	    {
	    	var me =  this;
	    	this.layout = 'card';
	    	onMenuItemClick = function (item)
	    	{
	    		try
	    		{
	    			me.getLayout().setActiveItem(item.name);
	    			item.up('button').focus();
	    		}
	    		catch (ex)
	    		{
					return;
				};
	    		me.up('panel').setActiveTab(me);
	    	};
	    	onMenuMouseLeave = function(menu, e, eOpts)
			{
				menu.hide();
			};
	    	var menu =
	    	{
	    		items:[],
	    		listeners:
	    		{
	    			mouseleave: onMenuMouseLeave
	    		}
	    	};
	    	this.items = this.items || [];
	    	this.items.forEach
	    	(
	    		function(item)
	    		{
	    			menu.items.push
	    			(
		    			{
		    				text: item.title,
			                name: item.itemId,
			                handler: onMenuItemClick
		    			}
	    			);
	    		}
	    	);
	    	this.tabConfig =
	    	{
	            menu: menu,
	            listeners:
	            {
			        click: function(button, e, eOpts )
			        {
			        	if (button.menu)
			        	{
			        		button.blur();
			        		e.stopEvent();
			        	}
			        },
			        mouseover: function( button, e, eOpts )
			        {
			        	if(button.menu && !button.hasVisibleMenu())
			            {
			            	Ext.ComponentQuery.query('button menu').forEach
			            	(
			            		function(menu)
			            		{
			            			menu.hide();
			            		}
			            	);
			                button.showMenu();
			            }
			        }
			    }
	        };
	    	this.callParent();
	    }
    }
);
