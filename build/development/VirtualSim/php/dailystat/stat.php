<?php

require_once("../../lib/php/common.php");

class Row
{
	var $day;
	var $ANSWERED;
	var $BUSY;
	var $NOANSWER;
	var $FAILED;
	var $CONGESTION;

	function Row($day)
	{
		$this->day = $day;
		$this->billmin=0;
		$this->ANSWERED = 0;
		$this->BUSY = 0;
		$this->NOANSWER = 0;
		$this->FAILED = 0;
		$this->CONGESTION = 0;
	}
}

function dateRange( $first, $last, $step = '+1 day', $format = 'm-d' ) {

	$dates = array();
	$current = strtotime( $first );
	$last = strtotime( $last );

	while( $current <= $last ) {

		$dates[] = date( $format, $current );
		$current = strtotime( $step, $current );
	}

	return $dates;
}

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND brand = '$brand_access' " : " WHERE true ";

$startday = date('Y-m-d', strtotime("-30 days"));
$endday = date('Y-m-d');
$brand = '';
$route = '';

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

$call_type = $DB->escape($_REQUEST['call_type']);

// $call_type = 'app2app';

$where .= " AND day >= '$startday' AND day <= '$endday' AND call_type = '$call_type' ";

if ($brand != '') $where .= " AND brand = '$brand' ";
if ($route) $where .= " AND route = '$route' ";

/*$query = "  SELECT 	DATE_FORMAT(day,'%m-%d') day,
					SUM(CASE WHEN disposition = 'ANSWERED' THEN counter ELSE 0 END) ANSWERED,
					SUM(CASE WHEN disposition = 'BUSY' THEN counter ELSE 0 END) BUSY,
					SUM(CASE WHEN disposition = 'NO ANSWER' THEN counter ELSE 0 END) 'NO ANSWER',
					SUM(CASE WHEN disposition = 'FAILED' THEN counter ELSE 0 END) 'FAILED'
            FROM stat
            WHERE day >= '$startday' AND day <= '$endday' GROUP BY `day`";*/

$query = "SELECT
	to_char(day, 'MM-DD') AS day,
	SUM (CASE
	WHEN disposition = 'ANSWERED' THEN CAST(billsec AS FLOAT)/60
	ELSE 0
	END)
	AS billmin,
	SUM (CASE
	WHEN disposition = 'ANSWERED' THEN counter
	ELSE 0
	END)
	AS \"ANSWERED\",
	SUM (CASE
	WHEN disposition = 'BUSY' THEN counter
	ELSE 0
	END)
	AS \"BUSY\",
	SUM (CASE
	WHEN disposition = 'NO ANSWER' THEN counter
	ELSE 0
	END)
	AS \"NO_ANSWER\",
	SUM (CASE
	WHEN disposition = 'FAILED' THEN counter
	ELSE 0
	END)
	AS \"FAILED\",
	SUM (CASE
	WHEN disposition = 'CONGESTION' THEN counter
	ELSE 0
	END)
	AS \"CONGESTION\"
FROM vs_stat
	$where
GROUP BY day";

$DB->query($query);

$result = array();

foreach (dateRange($startday, $endday) as $date )
{
	$result[$date] = new Row($date);
};

while($obj = $DB->fetch_object())
{
	$obj->billmin = (int)($obj->billmin);
	$result[$obj->day] = $obj;
}

$result = array_values ($result);

$response = array('data' => $result);
$response['sql'] = preg_replace('/\s\s+/', ' ', $query);

echo json_encode($response);
