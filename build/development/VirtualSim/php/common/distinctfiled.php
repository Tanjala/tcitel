<?php

require_once("../../lib/php/common.php");

$database = $DB->escape($_REQUEST["database"]);
if ($database != '') $DB->select($database);

$table = $DB->escape($_REQUEST["table"]);
$valueField = $DB->escape($_REQUEST["valueField"]);
$displayField = stripslashes($DB->escape($_REQUEST["displayField"]));

$orderField = (isset($_REQUEST["orderField"]) && $_REQUEST["orderField"]!='')?$DB->escape($_REQUEST["orderField"]):$valueField;
//$sort = (isset($_REQUEST["sort"]) && $_REQUEST["sort"]!='')?$DB->escape($_REQUEST["sort"]):'';

/*$queryFilter = (isset($_REQUEST["queryFilter"]) && $_REQUEST["queryFilter"]!='')?$DB->escape($_REQUEST["queryFilter"]):'';

if ($queryFilter != '') $where = " WHERE $queryFilter ";
else $where = '';
*/
$query = " SELECT DISTINCT $valueField AS value, $displayField AS display FROM `$table` /*$where*/ ORDER BY $orderField /*$sort*/ ";

$DB->query($query);

$arr = array();

if ($DB->escape($_REQUEST["all"]))
{
	$arr = array(array('value'=>'', 'display' => 'ALL'));
}
else
{
	$arr = array();
}

if ($DB->num_rows())
{
	$row=$DB->fetch_row();
	$options=explode("','", preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $row[1]));
	foreach ($options as $value)
	{
		$arr[] = array
		(
		    'value' => $value,
		    'display' => str_replace('_', ' ',htmlentities($value))
		);
	}
}

$result = array('data' => $arr);

echo json_encode($result);
