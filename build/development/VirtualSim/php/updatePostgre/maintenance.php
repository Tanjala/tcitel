<?php

require_once("../../lib/php/common.php");
while (@ob_end_flush());	

$count = $DB->sfetch("SELECT COUNT(*) FROM vs_contacts");
echo 'rows: ', $count, "\n";

$limit = 10000;
$portions = ceil($count / $limit);

$portions = 100;

$counter = 0;

$insert_tmpl = "INSERT INTO vs_contacts_test ( user_id, id, contact_name, emails, msisdns) VALUES ";

for ($i=0;$i<$portions;$i++)
{
	$offset = $i*$limit;
	$sql = "select * from vs_contacts ORDER BY user_id DESC , id DESC  OFFSET $offset LIMIT $limit";


	$DB->query($sql);

	$values = array();

	while($obj = $DB->fetch_assoc())
	{
		$id=$obj['id'];
		$user_id = $obj['user_id'];
		$contact_name=$obj['contact_name'];
		$emails = $obj['emails'];
		$msisdns = array();

		if (++$counter % 10 == 0) print(++$counter . ': ' . $user_id . ' ' .$id . "\n");

		$json = json_decode($obj["msisdns"]);

		foreach ($json as $key => $value) {
			if (is_object($value) and isset($value->msisdn)) $msisdns[]=preg_replace ('/[^\d]/', '', $value->msisdn);
		}

		if (!$msisdns) 
		{
			$msisdns = $obj['msisdns'];
		}
		else
		{
			$msisdns = array_unique($msisdns);
			$msisdns = array("msisdn"=>$msisdns);
			$msisdns = json_encode($msisdns);
		}

		$values[] = " ( $user_id, $id, '$contact_name', '$emails', '$msisdns' ) ";
	}

	$values = implode(', ', $values);

	$insert = $insert_tmpl . $values;

	echo $insert;
	$DB->nquery($insert);

}
