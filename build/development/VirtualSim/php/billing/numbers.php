<?php

require_once("../../lib/php/common.php");

$user_id = $DB->escape($_REQUEST['user_id']);

if (!$user_id)
{
	$response = array('data' => array());
	echo json_encode($response);
	exit;
}

$where = ' WHERE TRUE ';

$where .= " AND user_id = '$user_id' ";

$sql = "SELECT * FROM vs_numbers $where";

$DB->query($sql);

$arr = array();

while($obj = $DB->fetch_object())
{
	$obj->auto_renew = $obj->auto_renew == 't' ? true : false;
    $arr[] = $obj;
}

//$total = $DB->sfetch(" SELECT count(*) FROM vs_numbers $where ");
//$response = array('data' => $arr, 'total' => $total);

$response = array('data' => $arr);
//$response['sql'] = $sql;

echo json_encode($response);

