<?php

require_once("../../lib/php/common.php");

$number = '';
$email = '';
$user_id = '';
$brand = '';
$billing_id='';

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND b.brand = '$brand_access' AND u.brand = '$brand_access'" : " WHERE true ";

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

if (!$number && !$email && !$user_id && !$billing_id)
{
	$response = array('data' => array());
	echo json_encode($response);
	exit;
}

if($brand_access != 'Virtual SIM')
{
	if ($number)
	{
		$where .= " AND u.id = (SELECT user_id FROM vs_numbers WHERE number = $number AND  brand = '$brand_access') ";
	}

}
elseif($brand_access == 'Virtual SIM'){

if ($number)
	{
		$where .= " AND u.id = (SELECT user_id FROM vs_numbers WHERE number = $number AND brand = '$brand' ) ";
	}
}

if ($email)
{
	$where .= " AND u.email = '$email' ";
}

if ($brand)
{
	$where .= " AND b.brand = '$brand' AND u.brand = '$brand' ";
}

if ($user_id)
{
	$where .= " AND u.id = '$user_id' ";
}

if ($billing_id)
{
	$where .= " AND u.billing_id = '$billing_id' ";
}

$sql = $query = "SELECT u.id user_id, u.email, u.created, u.active, u.user_type, b.id billing_id, u.brand, b.balances, b.reservations FROM vs_users u JOIN b_user b ON u.billing_id = b.id  $where  LIMIT 1";

$DB->query($sql);

$arr = array();

function format_value(&$val)
{
	$val = trim($val, '{}');
	$val = explode (',', $val);
	foreach ($val as $key => $value) {
		$val[$key] = $key+1 . ".\t" . $value/100000;
	}
	$val = implode("\n", $val);
};

while($obj = $DB->fetch_object())
{
	/*$user_id = $obj->id;
	$sql = "SELECT @pv:=old_user_id AS 'old_user_id'
			FROM
			(SELECT old_user_id, new_user_id FROM reuse_log ORDER BY old_user_id DESC) t
			JOIN
			(SELECT @pv:=$user_id) tmp
			WHERE new_user_id=@pv";
	$DB->query($sql);
	$old_user_ids = array($user_id);
	while($obj1 = $DB->fetch_object())
	{
		$old_user_ids[] = $obj1->old_user_id;
	}
	$obj->ids = $old_user_ids;*/
	//$obj->balances = str_replace(',',"\n",trim($obj->balances, '{}'));
	format_value($obj->balances);
	format_value($obj->reservations);
    $arr[] = $obj;
}

$response = array('data' => $arr);
//$response['sql'] = $query;

echo json_encode($response);
