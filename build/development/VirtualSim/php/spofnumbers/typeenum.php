<?php

require_once("../../lib/php/common.php");

$table = 'special_offer_numbers';
$field = 'type';

$query = "SHOW COLUMNS FROM $table LIKE '$field'";

$DB->query($query);

$arr = array();

if ($DB->num_rows())
{
	$row=$DB->fetch_row();
	$options=explode("','", preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $row[1]));
	foreach ($options as $value)
	{
		$arr[] = array
		(
		    'value' => $value,
		    'display' => str_replace('_', ' ',htmlentities($value))
		);
	}
}

$result = array('data' => $arr);

echo json_encode($result);
