<?php

require_once("../../lib/php/common.php");

foreach ($_POST as $key => $value)
{
	${$DB->escape($key)} = $DB->escape($value);
}

$password_hash = $DB->sfetch("SELECT md5('$password')");

$response = array();

$req["req_type"] = "CM";
$req["auth"] = array("user" => "gui", "password" => "gui1234");
$req["type"] = "addbalance";
$req["account_id"] = floatval($billing_id);
$req["billing_profile"] = floatval(4);
$req["rate"] = floatval(100000*$ammount);
$req["brand"] = $brand;
$req["meta_data"] = "GUI user: {$_SESSION['USERDATA']["username"]}\n$info ";

if (!$ammount || !$password || !$billing_id || $password_hash != $_SESSION['USERDATA']["password"])
{
	$response['success'] = false;
	echo json_encode($response);
	exit;
}

$req_str = "http://$BILLING_IP:9090/?jsonData=".urlencode(json_encode($req));

$res = json_decode(file_get_contents($req_str));

$result = $res->result;

if (is_int($result) && $result >= 0)
{
	$response['success'] = true;
}
else
{
	// $response['getIPs'] = getIPs();
    $response['req_str'] = $req_str;
    $response['res'] = $res;
	$response['success'] = false;
}

echo json_encode($response);
