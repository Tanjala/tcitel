<?php

require_once("../../lib/php/common.php");

$offset = ($_REQUEST["start"] == null)? 0 : $_REQUEST["start"];
$limit = ($_REQUEST["limit"] == null)? 25 : $_REQUEST["limit"];

if (isset($_REQUEST['sort']) && $_REQUEST['sort'] != '')
{
	$sort = array();
	$ar = json_decode($_REQUEST['sort']);
	foreach ($ar as $ob)
	{
		$property = $DB->escape($ob->property);
		$direction = $DB->escape($ob->direction);
		$sort[] = " $property $direction ";
	}
	$sort = implode (', ', $sort);
	$sort = " ORDER BY $sort ";
}
else
{
	$sort = '';
}

$src = '';
$dst = '';
$user_id = '';
$call_type= '' ;
$brand = '' ;
$route = '';
$name = '';
$disposition = '';

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND c.brand = '$brand_access' " : " WHERE true ";

$log_start = date('Y-m-d 00:00:00');

$log_end = date('Y-m-d 23:59:59');

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

//$where = " WHERE true ";


$log_start = str_replace('T', ' ', $log_start);


$log_end = str_replace('T', ' ', $log_end);


$where .= " AND start >= '$log_start' AND start <= '$log_end' ";

if ($src != '') $where.=" AND src LIKE '$src%' ";
if ($dst != '') $where.=" AND dst = '$dst' ";
if ($user_id != '') $where.=" AND user_id = '$user_id' ";
//if ($clid != '') $where.=" AND clid IN ('$clid', '+$clid') ";
//if ($dst != '' && $dst != 'ALL') $where.=" AND dst = '$dst' ";
if ($call_type!= '' && $call_type!= 'ALL') $where.=" AND call_type = '$call_type' ";
if ($brand!= '' && $brand!= 'ALL') $where.=" AND brand = '$brand' ";
if ($route != '' && $route != 'ALL') $where.=" AND route = '$route' ";
if ($name != '' && $name != 'ALL') $where.=" AND name = '$name' ";
if ($disposition != '' && $disposition != 'ALL') $where.=" AND disposition = '$disposition' ";

$query = " SELECT c.start, c.end, c.answer, c.duration, c.billsec, c.disposition, c.src, c.dst, c.dcontext,c.clid, c.call_type, c.brand, c.user_id, c.payed_per_min, c.billed_per_min, r.route, r.name FROM vs_cdr c join vs_routes r on c.route = r.route $where $sort OFFSET $offset LIMIT $limit";

//$query = " SELECT * FROM vs_cdr $where $sort OFFSET $offset LIMIT $limit";

//$query = " SELECT *, \"count\" (*) OVER () AS total FROM vs_cdr $where ";

$total = $DB->sfetch(" SELECT count(*) FROM vs_cdr c join vs_routes r on c.route = r.route $where ");


$billsec = $DB->sfetch(" SELECT SUM(billsec) FROM vs_cdr c join vs_routes r on c.route = r.route $where ");
/*if ($sort!="") $query .= " ORDER BY `$sort` $dir ";

$query .= " LIMIT $start, $limit ";*/

$DB->query($query);

$arr = array();
while($obj = $DB->fetch_object())
{
	//$total = $obj->total;
	//$obj->record = "record/roll.mp4";
	$obj->billsectotal= (int)($billsec/60);
	///$obj->income= (int)($billsec/60)*((int)($billed_per_min)-(int)($payed_per_min));
	$arr[] = $obj;
}

$response = array();
$response['sql'] = $query;
$response['data'] = $arr;
$response['total'] = $total;
$response['billsec'] = $billsec;

echo json_encode($response);
