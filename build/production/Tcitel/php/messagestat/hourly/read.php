<?php

require_once("../../../lib/php/common.php");

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND brand = '$brand_access' " : " WHERE true ";

$day = date('Y-m-d');
$brand = '';
$direction = '';
//$route = '';

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
};

//$direction = $DB->escape($_REQUEST['direction']);

$where .= "AND direction NOT ILIKE 'NA%'";

$where .= " AND day = '$day'";
if ($direction) $where .= " AND direction = '$direction' ";
if ($brand) $where .= " AND brand = '$brand' ";
//if ($route) $where .= " AND route = '$route' ";


$query = "SELECT
	hour,
	SUM (CASE
	WHEN state = 'DELIVERED' THEN counter
	ELSE 0
	END)
	AS \"DELIVERED\",
	SUM (CASE
	WHEN state = 'SENT' THEN counter
	ELSE 0
	END)
	AS \"SENT\"

FROM mg_stat
	$where
GROUP BY hour ORDER BY hour";

$DB->query($query);

//$total = $DB->found_rows();

$arr = array();
while($obj = $DB->fetch_object())
{
   $obj->UNDELIVERED =  $obj->SENT-$obj->DELIVERED;
	$arr[] = $obj;
}

$response = array('data' => $arr);
//$response['query'] = $query;

echo json_encode($response);
