/***********************************************
Adds extension validation at the client side
for the extjs File Upload field. You must
set the extension in an array of strings.
 **********************************************/
Ext.define
(
    'Ext.ux.restrictiveFileUpload', 
    {
        extend: 'Ext.form.field.File',
        alias: 'widget.restfileupload',
        // Array of acceptable file extensions
        // overridden when decalred with a string
        // of extensions minus the period.
        accept: [],
        case_insensitive: true,
        listeners: 
        {
            change: function(me, value)
            {
                if (value == '') 
                {
                    me.setRawValue(null);
                    return;
                }

                if (this.accept.length == 0) return; // no validation

                var indexofPeriod = value.lastIndexOf("."),
                uploadedExtension = value.substr(indexofPeriod + 1, value.length - indexofPeriod);

                if (this.case_insensitive)
                {
                    uploadedExtension = uploadedExtension.toLowerCase(); 
                    this.accept.forEach
                    (
                        function(element, index, array)
                        {
                            array[index]=element.toLowerCase()
                        }
                    );
                }

                if (!Ext.Array.contains(this.accept, uploadedExtension))
                {
                    Ext.MessageBox.show
                    (
                        {
                            title: 'File Type Error',
                            msg: 'Please upload files with an extension of: <b>' + this.accept.join(', ') + '</b> only!',
                            buttons : Ext.Msg.OK,
                            icon  : Ext.Msg.ERROR
                        }
                    );
                    me.setRawValue(null);
                }
            }
        }
    }
);
