<?php

error_reporting(E_ALL);

$dir = __DIR__;
require_once("$dir/../../config/config.php");
require_once("$dir/pgsql.class.php");

date_default_timezone_set('Europe/Belgrade');

session_name($session_name);
session_start();

$DB = new pgsql();

function isJson($string)
{
	return (is_object(json_decode($string)));
}

function checkSession()
{
	return (isset($_SESSION['USER_ID']));
}

function login($username, $password)
{
	clearSession();
	global $DB;
	$username = $DB->escape($username);
	$password = $DB->escape($password);
	$sql = "SELECT id, role, username, password, firstname, lastname, email, phone, brand
  			FROM vs_operators
			WHERE username = '$username'
			AND password = md5('$password') ";
	$row = $DB->afetcha($sql);
	if ($row['role'])
	{
		$_SESSION['USER_ID'] = $row["id"];
		$_SESSION['USERDATA']["username"] = $row["username"];
		$_SESSION['USERDATA']["password"] = $row["password"];
		$_SESSION['USERDATA']["firstname"] = $row["firstname"];
		$_SESSION['USERDATA']["lastname"] = $row["lastname"];
		$_SESSION['USERDATA']["email"] = $row["email"];
		$_SESSION['USERDATA']["phone_number"] = $row["phone"];
		$_SESSION['USERDATA']["role"] = $row["role"];
		$_SESSION['USERDATA']["brand"] = $row["brand"];
	}
	    
	return (isset($_SESSION['USER_ID']));
}

function logout()
{
	clearSession();
	setcookie(session_name(),'',0,'/');
	return true;
}

function clearSession()
{
	session_unset();
    session_destroy();
    $_SESSION = array();
    session_write_close();
    session_start();
    session_regenerate_id(true);
    return true;
}
