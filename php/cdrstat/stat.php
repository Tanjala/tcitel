<?php

require_once("../../lib/php/common.php");

$brand_access = $_SESSION['USERDATA']["brand"];

$where = $brand_access != 'Virtual SIM' ? " WHERE true AND brand = '$brand_access' " : " WHERE true ";


$day = date('Y-m-d');
$brand = '';
$route = '';



if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}

$call_type = $DB->escape($_REQUEST['call_type']);

$where .= " AND call_type = '$call_type' AND day = '$day'";

if ($brand != '') $where .= " AND brand = '$brand' ";
if ($route) $where .= " AND route = '$route' ";

/*$query = "SELECT
	hour,
	sum(billsec) billsec,
	sum(answered) \"ANSWERED\",
	sum(busy) \"BUSY\",
	sum(no_answer) \"NO ANSWER\",
	sum(failed) \"FAILED\",
	sum(congestion) \"CONGESTION\"
FROM
(SELECT
	hour,
	CASE
	WHEN disposition = 'answered' THEN billsec
	ELSE 0
	END
	AS billsec,
	CASE
	WHEN disposition = 'answered' THEN counter
	ELSE 0
	END
	AS answered,
	CASE
	WHEN disposition = 'busy' THEN counter
	ELSE 0
	END
	AS busy,
	CASE
	WHEN disposition = 'NO ANSWER' THEN counter
	ELSE 0
	END
	AS no_answer,
	CASE
	WHEN disposition = 'FAILED' THEN counter
	ELSE 0
	END
	AS failed,
	CASE
	WHEN disposition = 'CONGESTION' THEN counter
	ELSE 0
	END
	AS congestion
FROM vs_stat
	$where) AS tmp
GROUP BY hour";*/

$query = "SELECT
	hour,
	SUM (CASE
	WHEN disposition = 'ANSWERED' THEN CAST(billsec AS FLOAT)/60
	ELSE 0
	END)
	AS billmin,
	SUM (CASE
	WHEN disposition = 'ANSWERED' THEN counter
	ELSE 0
	END)
	AS \"ANSWERED\",
	SUM (CASE
	WHEN disposition = 'BUSY' THEN counter
	ELSE 0
	END)
	AS \"BUSY\",
	SUM (CASE
	WHEN disposition = 'NO ANSWER' THEN counter
	ELSE 0
	END)
	AS \"NO_ANSWER\",
	SUM (CASE
	WHEN disposition = 'FAILED' THEN counter
	ELSE 0
	END)
	AS \"FAILED\",
	SUM (CASE
	WHEN disposition = 'CONGESTION' THEN counter
	ELSE 0
	END)
	AS \"CONGESTION\"
FROM vs_stat
	$where
GROUP BY hour ORDER BY hour";

$DB->query($query);

//$total = $DB->found_rows();

$arr = array();
while($obj = $DB->fetch_object())
{
	$obj->billmin = round($obj->billmin,2);
	$arr[] = $obj;
}

$response = array('data' => $arr);
//$response['sql'] = $query;

echo json_encode($response);
