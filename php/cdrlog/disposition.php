<?php

require_once("../../lib/php/common.php");

/*$query = "SELECT DISTINCT `status` AS `value`, `status` AS `display` FROM `orders` WHERE  `status` LIKE 'IabResult%' ORDER BY `status`";

$DB->query($query);*/

$arr = array();

if ($DB->escape($_REQUEST["all"]))
{
	$arr[] = array('value'=>'', 'display' => 'ALL');
}

$arr[] = array('value'=>'ANSWERED', 'display' => 'ANSWERED');
$arr[] = array('value'=>'BUSY', 'display' => 'BUSY');
$arr[] = array('value'=>'FAILED', 'display' => 'FAILED');
$arr[] = array('value'=>'NO_ANSWER', 'display' => 'NO ANSWER');

/*while($obj = $DB->fetch_object())
{
    $arr[] = $obj;
}*/

$response = array('data' => $arr);
//$response['sql'] = $query;

echo json_encode($response);
