<?php

date_default_timezone_set('Europe/Belgrade');

function getNewAccessToken(){
​
	$url = "https://accounts.google.com/o/oauth2/token";
	$ch = curl_init($url);
	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POSTFIELDS,
	    'refresh_token=1/MGP0oF3UeTqCneMnOkncFpUTgtpvJEOLXFd5msJIH88' . '&' .
	    'client_id=666572905172-areqh16fg39c3411kunr467mqlst6f96.apps.googleusercontent.com' . '&' .
	    'client_secret=raC93QXhJxdu_EJU9ND_VoW4' . '&' .   
	    'grant_type=refresh_token'
	); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$lResponse_json = curl_exec($ch);
	$curl_errno = curl_errno($ch);
	$curl_error = curl_error($ch);
​
	curl_close($ch);
​
	if ($curl_errno > 0) {
	 	return false;
	}
​
	$resp=json_decode($lResponse_json);
​
	if (null!=$resp) {
		return $resp->access_token;
	}
	else {
		return false;
	}
​
}
​
function checkTransaction($product_id,$token){
​
	$access_token=getNewAccessToken();
	if ($access_token===false) return false;
​
	$url="https://www.googleapis.com/androidpublisher/v2/applications/com.Tcitelapp/purchases/products/".urlencode($product_id)."/tokens/".urlencode($token)."?access_token=".$access_token;
	
	$ch = curl_init();		
	curl_setopt($ch, CURLOPT_URL, $url);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$lResponse_json = curl_exec($ch);
	$curl_errno = curl_errno($ch);
	$curl_error = curl_error($ch);
	curl_close($ch);
​
	if ($curl_errno > 0) {
	 	return false;
	}
	
	$resp=json_decode($lResponse_json);
​
	if (null!=$resp) {
		return $resp;
	}
	else {
		return false;
	}
	
}
​
function checkTransactionWithToken($product_id,$token,$access_token){
	
	if ($access_token===false) return false;
​
	$url="https://www.googleapis.com/androidpublisher/v2/applications/com.Tcitelapp/purchases/products/".urlencode($product_id)."/tokens/".urlencode($token)."?access_token=".$access_token;
	echo $url."\n";
​
	$ch = curl_init();		
	curl_setopt($ch, CURLOPT_URL, $url);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$lResponse_json = curl_exec($ch);
	$curl_errno = curl_errno($ch);
	$curl_error = curl_error($ch);
	curl_close($ch);
​
	if ($curl_errno > 0) {
	 	return false;
	}
	
	$resp=json_decode($lResponse_json);
​
	if (null!=$resp) {
		return $resp;
	}
	else {
		return false;
	}
	
}
​
$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
$access_token=getNewAccessToken();
​
if (($handle = fopen("filetest.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);     
​
        $line=checkTransactionWithToken($data[2],$data[3],$access_token);
        print_r($line);
        echo "\n";      
​
		fwrite($myfile, "\n". $data[0] . " | ". $data[1]. " | ". print_r( $line, true) );
​
 
    }
    
    fclose($handle);
}
​
fclose($myfile);