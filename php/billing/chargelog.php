<?php

require_once("../../lib/php/common.php");

$billing_id = $DB->escape($_REQUEST['billing_id']);

if (!$billing_id)
{
	$response = array('data' => array());
	echo json_encode($response);
	exit;
}
$name = '';

if (isset($_REQUEST['filter']) && $_REQUEST['filter'] != '')
{
	$filter = json_decode($_REQUEST['filter']);
	foreach ($filter as $f)
	{
		$property = $DB->escape($f->property);
		$value = $DB->escape($f->value);
		$$property = $value;
	}
}



$where = ' WHERE TRUE ';

$where .= " AND t.account_id = '$billing_id' ";

if ($name != '') $where.=" AND name = '$name' ";
//if ($brand != '') $where.=" AND brand = '$brand' ";

$sql = "SELECT *, p.id, p.name, to_char(created_at, 'YYYY-MM-DD HH24:MI:SS') as created_at , to_char(expires_at, 'YYYY-MM-DD HH24:MI:SS') as expires_at, ROUND(committed/-100000.00,2) as committed, ROUND(amount/-100000.00,2) as amount FROM b_transaction t JOIN b_transaction_detail d ON d.transaction_id = t.id JOIN b_transaction_result r ON r.id = d.result JOIN b_billing_profile p ON p.id = t.billing_profile_id $where ORDER BY to_char(created_at, 'YYYY-MM-DD HH24:MI:SS') DESC";

$DB->query($sql);

$arr = array();

while($obj = $DB->fetch_object())
{
    $arr[] = $obj;
}

$response = array('data' => $arr);
//$response['sql'] = $sql;

echo json_encode($response);
